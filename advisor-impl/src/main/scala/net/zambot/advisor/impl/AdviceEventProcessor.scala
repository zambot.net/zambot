package net.zambot.advisor.impl

import java.time.{LocalDateTime, ZoneId}
import java.util.{Date, UUID}

import akka.Done
import com.datastax.driver.core.utils.UUIDs
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, EventStreamElement, ReadSideProcessor}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.jdk.CollectionConverters._


class AdviceEventProcessor(session: CassandraSession, readSide: CassandraReadSide)
                          (implicit ec: ExecutionContext) extends ReadSideProcessor[AdvisorEvent] {

  override def aggregateTags: Set[AggregateEventTag[AdvisorEvent]] = Set(AdviceEvent.Tag)

  override def buildHandler(): ReadSideProcessor.ReadSideHandler[AdvisorEvent] = {
    readSide.builder[AdvisorEvent]("eventProcessor-advice_action")
      .setGlobalPrepare(() => globalPrepare)
      .setEventHandler[Advice](processAdviceEvent)
      .setPrepare(_ => prepareAdviceSession)
      .build

  }

  private def globalPrepare: Future[Done] = {
    createAdviceEventTable
  }

  private def createAdviceEventTable: Future[Done] = {
    session.executeCreateTable("CREATE TABLE IF NOT EXISTS advisor.advice_action (agent_id uuid, " +
      "advice_id timeuuid, ts timestamp, sequence bigint, code text, value text, data frozen<map<text, text>>, " +
      "PRIMARY KEY ((agent_id), advice_id, code, value)) WITH CLUSTERING ORDER BY (advice_id desc)")
  }

  private val writeAdvicePromise = Promise[PreparedStatement] // initialized in prepare
  private def writeEmittedSignalEvent: Future[PreparedStatement] = writeAdvicePromise.future

  private def prepareAdviceSession: Future[Done] = {
    val f = session.prepare("INSERT INTO advisor.advice_action (agent_id, advice_id, ts, sequence, code, value, " +
      "data) VALUES(?, ?, ?, ?, ?, ?, ?)")

    writeAdvicePromise.completeWith(f)
    f.map(_ => Done)

  }

  private def processAdviceEvent(ev: EventStreamElement[AdvisorEvent]): Future[List[BoundStatement]] = {
    ev.event match {
      case advice: Advice =>
        writeEmittedSignalEvent.map { ps =>
          advice.actionList.map(action => {
            ps.bind
              .setUUID("agent_id", java.util.UUID.fromString(advice.agentId))
              .setUUID("advice_id", UUID.fromString(advice.adviceId))
              .setTimestamp("ts", Date.from(LocalDateTime.parse(advice.timestamp).atZone(ZoneId.systemDefault).toInstant))
              .setLong("sequence", advice.sequence)
              .setString("code", action.code)
              .setString("value", action.value)
              .setMap("data", action.data.asJava)
          }).toList
        }
      case _ => Future {
        Nil
      }

    }

  }

}