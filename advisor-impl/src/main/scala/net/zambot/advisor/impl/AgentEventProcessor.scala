package net.zambot.advisor.impl

import java.time.{LocalDateTime, ZoneId}
import java.util.Date

import akka.Done
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, EventStreamElement, ReadSideProcessor}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.jdk.CollectionConverters._

class AgentEventProcessor(session: CassandraSession, readSide: CassandraReadSide)
                         (implicit ec: ExecutionContext) extends ReadSideProcessor[AdvisorEvent] {

  override def aggregateTags: Set[AggregateEventTag[AdvisorEvent]] = Set(AdvisorEvent.Tag)

  override def buildHandler(): ReadSideProcessor.ReadSideHandler[AdvisorEvent] = {
    readSide.builder[AdvisorEvent]("eventProcessor-agent_events")
      .setGlobalPrepare(() => createAgentTable)
      .setEventHandler[AgentInitialized](processAdvisorEvent)
      .setEventHandler[AgentStatusUpdated](processAdvisorEvent)
      .setPrepare(_ => prepareAgentSession)
      .build

  }

  private def createAgentTable: Future[Done] = {
    session.executeCreateTable("CREATE TABLE IF NOT EXISTS advisor.agent_list (agent_id UUID," +
      " inbound_channel_id UUID, outbound_channel_id UUID, created timestamp, updated timestamp, status text, " +
      "parameters frozen<map<text, text>>, version bigint, PRIMARY KEY(agent_id))")
  }

  private val writeAgentPromise = Promise[PreparedStatement] // initialized in prepare
  private def writeAgent: Future[PreparedStatement] = writeAgentPromise.future


  private def prepareAgentSession: Future[Done] = {
    val f = session.prepare("INSERT INTO advisor.agent_list (agent_id, inbound_channel_id, outbound_channel_id," +
      " status, created, updated,  parameters, version) VALUES(?, ?, ?, ?, ?, ?, ?, ?)")

    writeAgentPromise.completeWith(f)
    f.map(_ => Done)

  }

  private def processAdvisorEvent(ev: EventStreamElement[AdvisorEvent]): Future[List[BoundStatement]] = ev.event match {
    case e: AgentInitialized =>
      writeAgent.map {
        ps =>
          List({
            val bind = ps.bind()
              .setUUID("agent_id", java.util.UUID.fromString(e.id))
              .setString("status", e.status)
              .setTimestamp("created", Date.from(LocalDateTime.parse(e.created).atZone(ZoneId.systemDefault()).toInstant))
              .setTimestamp("updated", Date.from(LocalDateTime.parse(e.timestamp).atZone(ZoneId.systemDefault()).toInstant))
              .setMap("parameters", e.parameters.asJava)
              .setLong("version", e.version)
            e.incomingChannelId match {
              case Some(s) => bind.setUUID("inbound_channel_id", java.util.UUID.fromString(s))
              case None => bind.setToNull("inbound_channel_id")
            }
            e.outgoingChannelId match {
              case Some(s) => bind.setUUID("outbound_channel_id", java.util.UUID.fromString(s))
              case None => bind.setToNull("outbound_channel_id")
            }

            bind
          })
      }

    case e: AgentStatusUpdated =>
      writeAgent.map {
        ps =>
          List(ps.bind()
            .setUUID("agent_id", e.trackId)
            .setString("status", e.status)
            .setTimestamp("created", Date.from(LocalDateTime.parse(e.created).atZone(ZoneId.systemDefault()).toInstant))
            .setTimestamp("updated", Date.from(LocalDateTime.parse(e.timestamp).atZone(ZoneId.systemDefault()).toInstant))
            .setMap("parameters", e.parameters.asJava)
            .setLong("version", e.version)
          )
      }
    case _ => Future {
      Nil
    }

  }

}