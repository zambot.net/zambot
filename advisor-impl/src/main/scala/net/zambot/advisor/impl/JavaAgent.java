package net.zambot.advisor.impl;

import java.util.Map;

public class JavaAgent {

    public final Map<String, String> profile;
    public final Map<String, String> parameters;

    public JavaAgent(Map<String, String> profile, Map<String, String> parameters) {
        this.profile = profile;
        this.parameters = parameters;
    }
}
