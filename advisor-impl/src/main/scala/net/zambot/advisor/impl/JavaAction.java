package net.zambot.advisor.impl;

import java.util.Map;

public class JavaAction {

    public final String code;
    public final String value;
    public final Map<String, String> data;

    private JavaAction(String code, String value, Map<String, String> data) {
        this.code = code;
        this.value = value;
        this.data = data;

    }

    public static JavaAction internal(String value, Map<String, String> data) {
        return new JavaAction("internal", value, data);

    }

    public static JavaAction external(String code, String value, Map<String, String> data) {
        if ("internal".equals(code)) throw new RuntimeException("\"internal\" is a reserved action code!");
        return new JavaAction(code, value, data);

    }

}

