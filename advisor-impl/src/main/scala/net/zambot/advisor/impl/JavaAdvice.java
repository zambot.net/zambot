package net.zambot.advisor.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


public class JavaAdvice {

    public final Map<String, String> newProfile;
    public final List<JavaAction> actionList;
    public final LocalDateTime timestamp;

    public JavaAdvice(Map<String, String> newProfile, List<JavaAction> actionList) {
        this.actionList = actionList;
        this.newProfile = newProfile;
        this.timestamp = LocalDateTime.now();

    }

}
