package net.zambot.advisor.impl

import java.io.InputStreamReader
import java.time.{LocalDateTime, ZoneId}
import java.util.{Date, UUID}

import akka.actor.typed.{ActorRef, Behavior}
import akka.cluster.sharding.typed.scaladsl._
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior, ReplyEffect}
import com.datastax.driver.core.utils.UUIDs
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventTag, AkkaTaggerAdapter}
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import org.slf4j.LoggerFactory
import play.api.libs.json.{Format, Json, _}

import scala.collection.immutable.{ListMap, Seq}
import scala.jdk.CollectionConverters._

object AgentBehavior {

  val jsInvocable: javax.script.Invocable = {
    val engine = new javax.script.ScriptEngineManager().getEngineByName("JavaScript")
    engine.eval(new InputStreamReader(this.getClass.getClassLoader.getResourceAsStream("agent-rules.js")))
    engine.asInstanceOf[javax.script.Invocable]
  }

  def create(entityContext: EntityContext[AgentCommand]): Behavior[AgentCommand] = {
    create(
      PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId)
    ).withTagger(
      AkkaTaggerAdapter.fromLagom(entityContext, AdvisorEvent.Tag)
    ).withTagger(
      AkkaTaggerAdapter.fromLagom(entityContext, AdviceEvent.Tag)
    )
  }

  /*
   * This method is extracted to write unit tests that are completely independendant to Akka Cluster.
   */
  private[impl] def create(persistenceId: PersistenceId) =
    EventSourcedBehavior
      .withEnforcedReplies[AgentCommand, AdvisorEvent, AgentState](
        persistenceId = persistenceId,
        emptyState = AgentState.initial,
        commandHandler = (advisorAggregate, cmd) => advisorAggregate.applyCommand(cmd),
        eventHandler = (advisorAggregate, evt) => advisorAggregate.applyEvent(evt)
      ).snapshotWhen {
      case (state, event, sequence) => sequence % 100 == 0
    }
}

/**
  * The current state of the Aggregate.
  */
case class AgentState(id: Option[String], inboundChannelId: Option[String], outboundChannelId: Option[String],
                      created: String, status: String, timestamp: String, sequence: Long, profile: Map[String, String],
                      parameters: Map[String, String], decisionState: String, version: Long) {

  private val log = LoggerFactory.getLogger("AdvisorState")

  def applyCommand(cmd: AgentCommand): ReplyEffect[AdvisorEvent, AgentState] = status match {
    case "notInitialized" => cmd match {
      case c: InitializeAgent => onInitializeAgent(c)
      case c: GetAgentInfo => Effect.reply(c.replyTo)(Rejected(s"Agent not initialized"))
      case c: UpdateAgentStatus => Effect.reply(c.replyTo)(Rejected(s"Agent not initialized"))
      case c: Advise => Effect.reply(c.replyTo)(Rejected(s"Agent not initialized"))
    }
    case "paused" => cmd match {
      case c: InitializeAgent => onInitializeAgent(c)
      case c: GetAgentInfo => onGetAgentInfo(c)
      case c: UpdateAgentStatus => onUpdateAgentStatus(c)
      case c: Advise => onAdvise(c)
    }
    case "running" => cmd match {
      case c: InitializeAgent => Effect.reply(c.replyTo)(Rejected(s"Agent already initialized"))
      case c: GetAgentInfo => onGetAgentInfo(c)
      case c: UpdateAgentStatus => onUpdateAgentStatus(c)
      case c: Advise => onAdvise(c)
    }

  }

  def applyEvent(evt: AdvisorEvent): AgentState =
    evt match {
      case e: AgentInitialized => initializeAgent(e)
      case e: AgentStatusUpdated => updateAgentStatus(e)
      case e: Advice => advise(e)
      case e: ChangedDecisionState => seDecisionState(e)
      case e: UpdatedProfile => updateProfile(e)

    }

  def supportedAgentStatus = List("notInitialized", "running", "paused")

  def validateTrackStatus(taskStatus: String): Seq[String] =
    if (supportedAgentStatus.contains(taskStatus)) Nil else Seq(s"Status not supported. Supported Status are: $supportedAgentStatus")

  def validateUpdateAgentStatus(cmd: UpdateAgentStatus): Seq[String] = validateTrackStatus(cmd.status)

  private def onUpdateAgentStatus(cmd: UpdateAgentStatus): ReplyEffect[AdvisorEvent, AgentState] = {
    val errorList = validateUpdateAgentStatus(cmd)
    errorList match {
      case Nil => Effect
        .persist(
          AgentStatusUpdated(
            trackId = java.util.UUID.fromString(id.get),
            timestamp = LocalDateTime.now.toString,
            status = cmd.status,
            version = version + 1,
            created = created,
            parameters = parameters
          )
        ).thenReply(cmd.replyTo) { _ =>
        Accepted
      }
      case errors: Seq[String] => Effect.reply(cmd.replyTo)(
        Rejected(s"Error setting status: ${errors.fold("")((acc, s) => acc + ", " + s)}")
      )
    }
  }

  private def onInitializeAgent(cmd: InitializeAgent): ReplyEffect[AdvisorEvent, AgentState] =
    status match {
      case "notInitialized" => Effect
        .persist({
          val now = LocalDateTime.now
          AgentInitialized(id = cmd.id,
            status = "running",
            incomingChannelId = cmd.incomingChannelId,
            outgoingChannelId = cmd.outgoingChannelId,
            created = now.toString,
            timestamp = now.toString,
            parameters = cmd.parameters,
            version = 1
          )
        }).thenReply(cmd.replyTo) { _ =>
        AdvisorAgentId(cmd.id)
      }
      case _ => Effect.reply(cmd.replyTo)(Rejected("Already initialized."))

    }

  private def onGetAgentInfo(cmd: GetAgentInfo): ReplyEffect[AdvisorEvent, AgentState] =
    Effect.reply(cmd.replyTo)(AgentInfoSummary(
      agentId = java.util.UUID.fromString(id.get),
      inboundChannelId = inboundChannelId.fold(None: Option[UUID])(e => Some(java.util.UUID.fromString(e))),
      outboundChannelId = outboundChannelId.fold(None: Option[UUID])(e => Some(java.util.UUID.fromString(e))),
      created = created, status = status, timestamp = timestamp, parameters = parameters, version = version))


  private def onAdvise(cmd: Advise): ReplyEffect[AdvisorEvent, AgentState] = {

    case class AdviceResult(newProfile: Map[String, String], spec: String, data: Map[String, String])

    def applyRules(spec: String, facts: Map[String, String]): (Seq[AdvisorEvent], GeneratedAdvice) = {
      AgentBehavior.jsInvocable.invokeFunction(s"${spec}_${decisionState}",
        new JavaAgent(profile.asJava, parameters.asJava),
        new JavaFacts(spec, facts.asJava)
      ) match {
        case advice: JavaAdvice => {
          val adviceTimestamp = advice.timestamp
          val adviceId = UUIDs.startOf(
            Date.from(adviceTimestamp.atZone(ZoneId.systemDefault()).toInstant).getTime
          ).toString

          val (internalActionEvents, generatedActions) = advice.actionList.asScala.toSeq.foldLeft((Nil: Seq[AdvisorEvent], Nil: Seq[GeneratedAction])) {
            case ((events, actions), action) => {

              def convertActionToGeneratedAction(action: JavaAction): GeneratedAction = GeneratedAction(
                code = action.code, value = action.value, data = action.data.asScala.toMap
              )

              val internalActionEvents = action.code match {

                /**
                  * Internal actions.
                  * The code "internal" is a reserved word thus raises an exception if set.
                  * For creating internal actions, use the internal() factory method.
                  */
                case "internal" => action.value match {
                  /**
                    * Changes the decision State
                    */
                  case "change_decision_state" => events :+ ChangedDecisionState(agentId = id.get,
                    decisionId = adviceId,
                    timestamp = advice.timestamp.toString,
                    newState = action.data.get("newState"))

                  /** Unknown actions should have a special treatment */
                  case _ => events
                }

                /**
                  * External Actions.
                  * All the other codes are considered external actions.
                  * External actions are created with the external() factory method.
                  */
                case _ => events
              }

              (internalActionEvents, actions :+ convertActionToGeneratedAction(action))

            }

          }

          /**
            * This is the advice generated by the rule engine.
            */
          val generatedAdvice = GeneratedAdvice(
            adviceId = adviceId,
            timestamp = adviceTimestamp.toString,
            sequence = sequence + 1,
            inboundChannelId = inboundChannelId.fold(None: Option[UUID])(id => Some(UUID.fromString(id))),
            outboundChannelId = outboundChannelId.fold(None: Option[UUID])(id => Some(UUID.fromString(id))),
            actions = generatedActions
          )

          /**
            * All decisions updates the profile, thus the profile parameter is mandatory.
            */
          val updateProfileEvent = UpdatedProfile(agentId = id.get,
            adviceId = adviceId,
            timestamp = advice.timestamp.toString,
            newProfile = advice.newProfile.asScala.toMap)

          /**
            * All decisions also generates an advice event.
            */
          val adviceEvent = Advice(
            agentId = id.get,
            adviceId = adviceId,
            timestamp = adviceTimestamp.toString,
            sequence = sequence + 1,
            actionList = generatedAdvice.actions.map(action => AdviceAction(
              action.code,
              action.value,
              action.data)
            )
          )

          val allEvents = updateProfileEvent +: internalActionEvents :+ adviceEvent
          (allEvents, generatedAdvice)

        }
        case o => throw new RuntimeException(s"Unrecognized object: $o")
      }
    }

    val (events: Seq[AdvisorEvent], reply: GeneratedAdvice) = applyRules(spec = cmd.spec, facts = cmd.data)
    Effect
      .persist(events)
      .thenReply(cmd.replyTo) { _ => reply }

  }

  private def initializeAgent(event: AgentInitialized) = {
    copy(id = Some(event.id),
      inboundChannelId = event.incomingChannelId,
      outboundChannelId = event.outgoingChannelId,
      status = event.status,
      timestamp = LocalDateTime.now.toString,
      version = event.version,
      parameters = event.parameters
    )
  }

  private def updateAgentStatus(event: AgentStatusUpdated): AgentState = copy(
    status = event.status,
    timestamp = event.timestamp,
    version = event.version
  )

  private def advise(decision: Advice): AgentState = {
    copy(sequence = decision.sequence, timestamp = LocalDateTime.now.toString)
  }

  private def seDecisionState(e: ChangedDecisionState): AgentState = {
    copy(decisionState = e.newState)
  }

  private def updateProfile(e: UpdatedProfile): AgentState = {
    copy(profile = e.newProfile)
  }

}


object AgentState {

  /**
    * The initial state. This is used if there is no snapshotted state to be found.
    */
  def initial: AgentState = AgentState(id = None: Option[String], inboundChannelId = None,
    outboundChannelId = None, created = LocalDateTime.now.toString, timestamp = LocalDateTime.now.toString,
    status = "notInitialized", sequence = 0, parameters = ListMap.empty, profile = ListMap.empty,
    decisionState = "initial_state", version = 0)

  /**
    * The [EventSourcedBehavior] instances (aka Aggregates) run on sharded actors inside the Akka Cluster.
    * When sharding actors and distributing them across the cluster, each aggregate is
    * namespaced under a typekey that specifies a name and also the type of the commands
    * that sharded actor can receive.
    */
  val typeKey = EntityTypeKey[AgentCommand]("AgentAggregate")

  /**
    * Format for the Agent state.
    *
    * Persisted entities get snapshotted every configured number of events. This
    * means the state gets stored to the database, so that when the aggregate gets
    * loaded, you don't need to replay all the events, just the ones since the
    * snapshot. Hence, a JSON format needs to be declared so that it can be
    * serialized and deserialized when storing to and from the database.
    */

  implicit val agentStateFormat: Format[AgentState] = Json.format
}


/**
  * This interface defines all the events that the SchedulerAggregate supports.
  */
sealed trait AdvisorEvent extends AggregateEvent[AdvisorEvent]

object AdvisorEvent {
  val Tag: AggregateEventTag[AdvisorEvent] = AggregateEventTag[AdvisorEvent]
}

object AdviceEvent {
  val Tag: AggregateEventTag[AdvisorEvent] = AggregateEventTag[AdvisorEvent]
}

/**
  * Events
  */
case class AgentInitialized(id: String, incomingChannelId: Option[String], outgoingChannelId: Option[String],
                            status: String, timestamp: String, created: String, parameters: Map[String, String],
                            version: Long) extends AdvisorEvent {
  def aggregateTag: AggregateEventTag[AdvisorEvent] = AdvisorEvent.Tag
}

object AgentInitialized {
  implicit val format: Format[AgentInitialized] = Json.format
}

case class AgentStatusUpdated(trackId: UUID, status: String, created: String, timestamp: String,
                              parameters: Map[String, String], version: Long) extends AdvisorEvent {
  def aggregateTag: AggregateEventTag[AdvisorEvent] = AdvisorEvent.Tag
}

object AgentStatusUpdated {
  implicit val format: Format[AgentStatusUpdated] = Json.format
}

case class AdviceAction(code: String, value: String, data: Map[String, String])

object AdviceAction {
  implicit val format: Format[AdviceAction] = Json.format
}

case class Advice(agentId: String, adviceId: String, timestamp: String, sequence: Long,
                  actionList: Seq[AdviceAction]
                 ) extends AdvisorEvent {
  def aggregateTag: AggregateEventTag[AdvisorEvent] = AdviceEvent.Tag
}

object Advice {
  implicit val format: Format[Advice] = Json.format
}

case class ChangedDecisionState(agentId: String, decisionId: String, timestamp: String, newState: String) extends AdvisorEvent {
  def aggregateTag: AggregateEventTag[AdvisorEvent] = AdvisorEvent.Tag

}

object ChangedDecisionState {
  implicit val format: Format[ChangedDecisionState] = Json.format

}


case class UpdatedProfile(agentId: String, adviceId: String, timestamp: String, newProfile: Map[String, String]) extends AdvisorEvent {
  def aggregateTag: AggregateEventTag[AdvisorEvent] = AdvisorEvent.Tag

}

object UpdatedProfile {
  implicit val format: Format[UpdatedProfile] = Json.format

}

/**
  * Commands
  */
trait AdvisorCommandSerializable

sealed trait AgentCommand extends AdvisorCommandSerializable

case class InitializeAgent(id: String, incomingChannelId: Option[String], outgoingChannelId: Option[String],
                           parameters: Map[String, String], replyTo: ActorRef[Confirmation]) extends AgentCommand

case class GetAgentInfo(replyTo: ActorRef[Confirmation]) extends AgentCommand

case class UpdateAgentStatus(status: String, replyTo: ActorRef[Confirmation]) extends AgentCommand

case class Advise(timestamp: LocalDateTime, spec: String, data: Map[String, String],
                  replyTo: ActorRef[Confirmation]) extends AgentCommand

/**
  * Replies
  */
sealed trait Confirmation

case object Confirmation {
  implicit val format: Format[Confirmation] = new Format[Confirmation] {
    override def reads(json: JsValue): JsResult[Confirmation] = {
      if ((json \ "reason").isDefined) Json.fromJson[Rejected](json)
      else Json.fromJson[Accepted](json)
    }

    override def writes(o: Confirmation): JsValue = {
      o match {
        case acc: Accepted => Json.toJson(acc)
        case advisorInfoSummary: AgentInfoSummary => Json.toJson(advisorInfoSummary)
        case advisorId: AdvisorAgentId => Json.toJson(advisorId)
        case rej: Rejected => Json.toJson(rej)
        case adv: GeneratedAdvice => Json.toJson(adv)
        case action: GeneratedAction => Json.toJson(action)
      }
    }

  }

}

sealed trait Accepted extends Confirmation

case object Accepted extends Accepted {
  implicit val format: Format[Accepted] =
    Format(Reads(_ => JsSuccess(Accepted)), Writes(_ => Json.obj()))
}

case class Rejected(reason: String) extends Confirmation

object Rejected {
  implicit val format: Format[Rejected] = Json.format
}

case class AgentInfoSummary(agentId: UUID, inboundChannelId: Option[UUID], outboundChannelId: Option[UUID],
                            created: String, status: String, timestamp: String, parameters: Map[String, String],
                            version: Long) extends Confirmation


object AgentInfoSummary {
  implicit val format: Format[AgentInfoSummary] = Json.format
}


case class TaskInfoResult(success: Boolean, message: String, values: Map[String, String])

object TaskInfoResult {
  implicit val format: Format[TaskInfoResult] = Json.format
}

case class AdvisorAgentId(value: String) extends Confirmation

object AdvisorAgentId {
  implicit val format: Format[AdvisorAgentId] = Json.format
}

case class GeneratedAction(code: String, value: String, data: Map[String, String]) extends Confirmation


object GeneratedAction {
  implicit val format: Format[GeneratedAction] = Json.format
}

case class GeneratedAdvice(adviceId: String, inboundChannelId: Option[UUID], outboundChannelId: Option[UUID],
                           timestamp: String, sequence: Long, actions: Seq[GeneratedAction]) extends Confirmation

object GeneratedAdvice {
  implicit val format: Format[GeneratedAdvice] = Json.format
}

/**
  * Akka serialization, used by both persistence and remoting, needs to have
  * serializers registered for every type serialized or deserialized. While it's
  * possible to use any serializer you want for Akka messages, out of the box
  * Lagom provides support for JSON, via this registry abstraction.
  *
  * The serializers are registered here, and then provided to Lagom in the
  * application loader.
  */
object AdvisorSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[AgentState],
    JsonSerializer[AgentInitialized],
    JsonSerializer[AgentStatusUpdated],
    JsonSerializer[AgentInfoSummary],
    JsonSerializer[Confirmation],
    JsonSerializer[ChangedDecisionState],
    JsonSerializer[UpdatedProfile],
    JsonSerializer[Advice],
    JsonSerializer[Accepted],
    JsonSerializer[Rejected]
  )
}