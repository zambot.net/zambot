package net.zambot.advisor.impl

import java.time.{LocalDateTime, ZoneId}
import java.util.{Date, UUID}

import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, EntityRef}
import akka.persistence.query.Offset
import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Sink}
import akka.util.Timeout
import akka.{Done, NotUsed}
import com.datastax.driver.core.SimpleStatement
import com.datastax.driver.core.utils.UUIDs
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.transport.{BadRequest, NotFound}
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry, ReadSide}
import com.softwaremill.macwire.wire
import net.zambot.advisor.api.{AdviceActionEvent, AdviceEvent, AdvisorService}
import net.zambot.advisor.model
import net.zambot.advisor.model._

import scala.annotation.tailrec
import scala.collection.immutable
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.jdk.CollectionConverters._


/**
  * Implementation of the SchedulerService.
  */
class AdvisorServiceImpl(clusterSharding: ClusterSharding, persistentEntityRegistry: PersistentEntityRegistry,
                         cassandraSession: CassandraSession, cassandraReadSide: CassandraReadSide,
                         readSide: ReadSide)(implicit ec: ExecutionContext, materializer: Materializer) extends AdvisorService {

  readSide.register[AdvisorEvent](wire[AgentEventProcessor])
  readSide.register[AdvisorEvent](wire[AdviceEventProcessor])

  /**
    * Looks up the entity for the given ID.
    */
  private def entityRef(id: String): EntityRef[AgentCommand] =
    clusterSharding.entityRefFor(AgentState.typeKey, id)

  implicit val timeout: Timeout = Timeout(5.seconds)

  /**
    * Presents a fact to the agent
    * Generate advice from fact
    *
    * @param agentId The agent identifier
    * @return EntityId Body Parameter  Generates an advice from fact
    */
  override def advise(agentId: UUID): ServiceCall[Fact, GeneratedAdviceInfo] = { request => {
    val ref = entityRef(agentId.toString)
    ref.ask[Confirmation](replyTo =>
      net.zambot.advisor.impl.Advise(
        timestamp = request.timestamp,
        spec = request.code,
        data = request.data,
        replyTo = replyTo)
    ).map {
      case adv: GeneratedAdvice => GeneratedAdviceInfo(
        adviceId = UUID.fromString(adv.adviceId),
        timestamp = LocalDateTime.parse(adv.timestamp),
        sequence = adv.sequence,
        inboundChannelId = adv.inboundChannelId,
        outboundChannelId = adv.outboundChannelId,
        actions = adv.actions.map(action =>
          GeneratedAdviceActionInfo(code = action.code,
            value = action.value,
            data = action.data
          )
        )
      )
      case Rejected(reason) => throw BadRequest(reason)
      case _ => throw BadRequest("Invalid request.")
    }
  }

  }

  /**
    * Shows agent advices
    *
    * @param agentId   The Agent identifier
    * @param startDate (optional)
    * @param endDate   (optional)
    * @param pageSize  (optional)
    * @return AdviceInfoList
    */
  override def findAdviceAction(agentId: UUID, startDate: Option[String], endDate: Option[String], code: Option[String], value: Option[String], pageSize: Option[Int]): ServiceCall[NotUsed, AdviceInfoList] = { _ =>

    def generateWhereClause(after: Option[LocalDateTime], before: Option[LocalDateTime], code: Option[String], value: Option[String]): String = {

      @tailrec
      def whereClause(item: Option[String], list: List[Option[String]], counter: Int, currentClause: String = ""): String = item match {
        case None => if (list.isEmpty) currentClause else whereClause(list.head, list.tail, counter, currentClause)
        case Some(s) => if (counter == 0)
          if (list.isEmpty) "where " + s else whereClause(list.head, list.tail, counter + 1, "where " + s)
        else if (list.isEmpty) currentClause + " and " + s
        else whereClause(list.head, list.tail, counter + 1, currentClause + " and " + s)

      }

      val dateFilter: Option[String] = after.fold(
        before.fold(None: Option[String])(_ => Some("advice_id < ?"))
      )(_ => Some("advice_id > ?"))

      val filterList = List(Some("TOKEN(agent_id) = TOKEN(?)"), dateFilter, code.fold(None: Option[String])(c => Some("code = ?")), value.fold(None: Option[String])(v => Some("value = ?")))
      if (filterList.isEmpty) "" else whereClause(filterList.head, filterList.tail, 0)

    }

    def generateParams(pageSize: Int, after: Option[LocalDateTime], before: Option[LocalDateTime], c: Option[String], v: Option[String]): Array[Object] = {
      val date: Option[UUID] = after.fold(before.fold(None: Option[UUID])(after =>
        Some(UUIDs.startOf(Date.from(after.atZone(ZoneId.systemDefault()).toInstant).getTime))
      ))(before => Some(UUIDs.startOf(Date.from(before.atZone(ZoneId.systemDefault).toInstant).getTime)))

      val parameters: Seq[Object] = Seq(agentId.asInstanceOf[Object]) ++ (date match {
        case Some(o) => Seq(o.asInstanceOf[Object]);
        case _ => Nil
      }) ++ (c match {
        case Some(o) => Seq(o.asInstanceOf[Object]);
        case _ => Nil
      }) ++ (v match {
        case Some(o) => Seq(o.asInstanceOf[Object]);
        case _ => Nil
      }) ++ Seq(pageSize.asInstanceOf[Object])
      parameters.toArray

    }

    entityRef(agentId.toString).ask[Confirmation](replyTo =>
      GetAgentInfo(replyTo = replyTo)
    ).map {
      case agentInfo: AgentInfoSummary => {
        val ps = pageSize.getOrElse(10)

        val after: Option[LocalDateTime] = startDate.fold(None: Option[LocalDateTime])(s => Some(java.time.LocalDateTime.parse(s)))
        val before: Option[LocalDateTime] = endDate.fold(None: Option[LocalDateTime])(s => Some(java.time.LocalDateTime.parse(s)))

        val statement = new SimpleStatement(
          s"SELECT agent_id, advice_id, ts, sequence, code, value, data " +
            s" FROM advisor.advice_action " +
            generateWhereClause(after, before, code, value) + " LIMIT ? ALLOW FILTERING", generateParams(ps, after, before, code, value): _*)


        statement.setFetchSize(ps)

        val source = cassandraSession.select(statement).map(row => AdviceInfo(
          agentId = agentId,
          adviceId = row.getUUID("advice_id"),
          timestamp = java.time.LocalDateTime.ofInstant(row.getTimestamp("ts").toInstant, ZoneId.systemDefault()),
          sequence = row.getLong("sequence"),
          code = row.getString("code"),
          value = row.getString("value"),
          inboundChannelId = agentInfo.inboundChannelId,
          outboundChannelId = agentInfo.outboundChannelId,
          data = row.getMap("data", classOf[String], classOf[String]).asScala.toMap
        ))

        source.runWith(Sink.seq[AdviceInfo])
          .map(e => AdviceInfoList(e))
      }

      case _ => throw NotFound(s"Agent ${agentId.toString} was not found.")

    }.flatMap(list => list)

  }

  /**
    * List advisor agents
    * List Advisor Agents
    *
    * @param nextPageToken     (optional)
    * @param previousPageToken (optional)
    * @param pageSize          (optional)
    * @return AgentInfoList
    */
  override def findAgent(nextPageToken: Option[UUID], previousPageToken: Option[UUID], pageSize: Option[Int],
                         inboundChannelId: Option[UUID], outboundChannelId: Option[UUID]): ServiceCall[NotUsed, AgentInfoList] = { _ =>

    def generateWhereClause(previousPageToken: Option[UUID], nextPageToken: Option[UUID],
                            incomingChannelIdId: Option[UUID]): String = {

      @tailrec
      def whereClause(item: Option[String], list: List[Option[String]], counter: Int, currentClause: String = ""): String = {
        item match {
          case None => if (list.isEmpty) currentClause else whereClause(list.head, list.tail, counter, currentClause)
          case Some(s) => if (counter == 0)
            if (list.isEmpty) "where " + s else whereClause(list.head, list.tail, counter + 1, "where " + s)
          else if (list.isEmpty) currentClause + " and " + s
          else whereClause(list.head, list.tail, counter + 1, currentClause + " and " + s)

        }
      }

      val pageTokenFilter: Option[String] = previousPageToken.fold(
        nextPageToken.fold(None: Option[String])(_ => Some("token(agent_id) > token(?)")))(_ => Some("token(agent_id) < token(?)"))

      val filterList: List[Option[String]] = List(pageTokenFilter, incomingChannelIdId.fold(None: Option[String])(e => Some("inbound_channel_id=?")), outboundChannelId.fold(None: Option[String])(e => Some("outbound_channel_id=?")))
      if (filterList.isEmpty) "" else whereClause(filterList.head, filterList.tail, 0)

    }

    def generateParams(pageSize: Int, previousPageToken: Option[UUID], nextPageToken: Option[UUID], incomingChannelId: Option[UUID]): Array[Object] = {
      val pageToken = previousPageToken.fold(nextPageToken.fold(None: Option[UUID])(nextPageToken => Some(nextPageToken)))(previousPageToken => Some(previousPageToken))
        .fold(Seq(pageSize.asInstanceOf[Object]))(token => Seq(token.asInstanceOf[Object], pageSize.asInstanceOf[Object]))
      val parameters: Seq[Object] = incomingChannelId.fold(Nil: Seq[Object])(e => Seq(e.asInstanceOf[Object])) ++ outboundChannelId.fold(Nil: Seq[Object])(e => Seq(e.asInstanceOf[Object])) ++ pageToken
      parameters.toArray
    }

    val ps = pageSize.getOrElse(10)

    val statement = new SimpleStatement(
      "SELECT agent_id, inbound_channel_id, outbound_channel_id, created, updated, status, " +
        s"parameters, version FROM advisor.agent_list ${generateWhereClause(previousPageToken, nextPageToken, inboundChannelId)} LIMIT ? ALLOW FILTERING",
      generateParams(ps, previousPageToken, nextPageToken, inboundChannelId): _*)

    statement.setFetchSize(ps)

    cassandraSession
      .select(statement)
      .map(row => {
        AgentInfo(
          agentId = row.getUUID("agent_id"),
          incomingChannelId = if (row.isNull("inbound_channel_id")) None: Option[UUID] else Some(row.getUUID("inbound_channel_id")),
          outgoingChannelId = if (row.isNull("outbound_channel_id")) None: Option[UUID] else Some(row.getUUID("outbound_channel_id")),
          created = java.time.LocalDateTime.ofInstant(row.getTimestamp("created").toInstant, ZoneId.systemDefault()),
          timestamp = java.time.LocalDateTime.ofInstant(row.getTimestamp("updated").toInstant, ZoneId.systemDefault()),
          status = row.getString("status"),
          version = 1, //row.getLong("version"),
          parameters = AgentParameters(values = row.getMap("parameters", classOf[String], classOf[String]).asScala.toMap)
        )
      }
      )
      .runWith(Sink.fold[Seq[AgentInfo], AgentInfo](Seq())((seq, elem) => seq :+ elem))
      .map(e => AgentInfoList(e))

  }

  /**
    * Retrieves agent information
    *
    * @param agentId Agent identifier
    * @return AgentInfo
    */
  override def getAgentInfo(agentId: UUID): ServiceCall[NotUsed, AgentInfo] = { _ =>
    entityRef(agentId.toString)
      .ask[Confirmation](replyTo => GetAgentInfo(replyTo = replyTo))
      .map {
        case agentInfo: AgentInfoSummary => AgentInfo(
          agentId = agentId,
          incomingChannelId = agentInfo.inboundChannelId.fold(None: Option[UUID])(e => Some(e)),
          outgoingChannelId = agentInfo.outboundChannelId.fold(None: Option[UUID])(e => Some(e)),
          created = LocalDateTime.parse(agentInfo.created),
          status = agentInfo.status,
          timestamp = LocalDateTime.parse(agentInfo.timestamp),
          version = agentInfo.version,
          parameters = AgentParameters(values = agentInfo.parameters)
        )
        case _ => throw NotFound(s"Agent ${agentId.toString} was not found.")
      }
  }

  /**
    * Initializes an advisor agent
    * Initializes an Advisor Agent
    *
    * @return EntityId Body Parameter  Advisor Agent Initialization Info
    */
  override def initializeAgent(): ServiceCall[CreateAgent, AgentId] = { request =>
    val newAgentId = java.util.UUID.randomUUID()
    entityRef(newAgentId.toString)
      .ask[Confirmation](replyTo =>
        InitializeAgent(
          id = newAgentId.toString,
          parameters = request.parameters.values,
          incomingChannelId = request.inboundChannelId.fold(None: Option[String])(e => Some(e.toString)),
          outgoingChannelId = request.outboundChannelId.fold(None: Option[String])(e => Some(e.toString)),
          replyTo = replyTo)
      ).map {
      case AdvisorAgentId(id) => AgentId(java.util.UUID.fromString(id.toString))
      case Rejected(reason) => throw BadRequest(reason)
      case _ => throw BadRequest("Invalid request.")
    }

  }

  /**
    * Updates agent status
    * Updates agent status
    *
    * @param agentId Advisor Agent identifier
    * @return void Body Parameter  Agent Status
    */
  override def updateAgentStatus(agentId: UUID): ServiceCall[model.UpdateAgentStatus, Done] = { request =>
    entityRef(agentId.toString)
      .ask[Confirmation](replyTo => net.zambot.advisor.impl.UpdateAgentStatus(status = request.status, replyTo = replyTo)
      ).map {
      case Accepted => Done
      case Rejected(reason) => throw BadRequest(reason)
      case _ => throw BadRequest("Invalid request")
    }

  }

  override def agentAdviceEventsTopic(): Topic[AdviceEvent] = TopicProducer.singleStreamWithOffset {
    fromOffset =>
      persistentEntityRegistry
        .eventStream(net.zambot.advisor.impl.AdviceEvent.Tag, fromOffset)
        .mapConcat(filterSignalEvents)
  }

  private def filterSignalEvents(ev: EventStreamElement[AdvisorEvent]): scala.collection.immutable.Iterable[(AdviceEvent, Offset)] = ev match {
    case ev@EventStreamElement(_, e: net.zambot.advisor.impl.Advice, offset) => scala.collection.immutable.Iterable((convertAdviceToAdviceEvent(e), offset))
    case _ => Nil
  }

  private def convertAdviceToAdviceEvent(advice: net.zambot.advisor.impl.Advice): AdviceEvent = AdviceEvent(
    agentId = advice.agentId,
    adviceId = advice.adviceId,
    timestamp = advice.timestamp,
    sequence = advice.sequence,
    actions = advice.actionList
      .map(action =>
        AdviceActionEvent(code = action.code,
          value = action.value,
          data = action.data)
      )
  )

}