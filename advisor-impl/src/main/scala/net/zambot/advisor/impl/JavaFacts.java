package net.zambot.advisor.impl;

import java.util.Map;

public class JavaFacts {

    public final String spec;
    public final Map<String, String> data;

    public JavaFacts(String spec, Map<String, String> data) {
        this.spec = spec;
        this.data = data;

    }

}
