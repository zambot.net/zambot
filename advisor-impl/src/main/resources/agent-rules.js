function getText(map, key, def) {
    var o = map.get(key);
    if (o == null) return def;
    else return o;
}

function getNumber(map, key, def) {
    var o = Number(map.get(key));
    if (o == NaN) return def;
    else return o;
}

/**
 * Zambot DSL can be extended by creating JavaScript methods
 */
var JavaAdvice = Java.type("net.zambot.advisor.impl.JavaAdvice");
var JavaAction = Java.type("net.zambot.advisor.impl.JavaAction");

function getMarketSymbol(agent) {
    return getText(agent.parameters, "marketSymbol", "DEFAULT")
}

function advice(newProfile, actions) {
    return new JavaAdvice(newProfile, actions);
}

function internalAction(value, data) {
    return JavaAction.internal(value, data);
}

function externalAction(code, value, data) {
    return JavaAction.external(code, value, data);
}

function createOrderAction(quantity, limit, marketSymbol, direction, type, timeInForce) {
    return externalAction("place_order", direction, {
        "quantity": quantity.toString(),
        "limit": limit.toString(),
        "type": type,
        "timeInForce": timeInForce,
        "marketSymbol": marketSymbol
    });
}

function createBuyOrderAction(marketSymbol, quantity, limit) {
    return createOrderAction(quantity, limit, marketSymbol, "BUY", "LIMIT", "GOOD_TIL_CANCELLED");
}

function createSellOrderAction(marketSymbol, quantity, limit) {
    return createOrderAction(quantity, limit, marketSymbol, "SELL", "LIMIT", "GOOD_TIL_CANCELLED");
}

function createCancelOrderAction(orderId) {
    return externalAction("cancel_order", orderId, {});
}

function changeDecisionStateAction(newState) {
    return internalAction("change_decision_state", {"newState": newState});
}

function o() {
    return externalAction("X", "Y", {});
}

function example1(agent, facts) {
    var count = getNumber(agent.profile, "count", 0) + 1;
    if (count % 10000 == 0) {
        var action = changeDecisionStateAction("second_state");
        var action2 = createBuyOrderAction(getMarketSymbol(agent), 1, 0.00000000001);
        return advice({"count": count.toString()}, [action, action2]);

    } else {
        return advice({"count": count.toString()}, []);
    }

}

function example2(agent, facts) {
    var count = getNumber(agent.profile, "count", 0) + 1;
    if (count % 10000 == 0) {
        var action = changeDecisionStateAction("initial_state");
        var action2 = createSellOrderAction(getMarketSymbol(agent), 1, 10000);
        return advice({"count": count.toString()}, [action, action2]);
    } else {
        return advice({"count": count.toString()}, []);
    }

}

/**
 * Here goes all the methods to be invoked when a Fact arrives. Method name convention:
 *
 *      [FACTS_SPEC]_[DECISION_STATE](agent, facts)
 *
 *      where:
 *          [FACTS_SPEC] - is the spec value for the Fact
 *          [DECISION_STATE] - is the current decision state (defaults to initial_state)
 *          agent - agent information, including the profile
 *          facts - facts data
 *
 *  For instance, given a "tick" facts spec arriving and "initial_state" as the state,
 *  the method signature to be invoked is:
 *      tick_initial_state(agent, facts)
 *
 *  - For state transitions, use changeDecisionStateAction(newState);
 *  - For creating sell orders, use createSellOrderAction(quantity, limit);
 *  - For creating buy Orders, use createBuyOrderAction(quantity, limit);
 *  - For canceling orders, use createCancelOrderAction(quantity, limit);
 *
 */

/**
 * Invoked when a 'tick' fact spec is advised during 'initial' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'tick' spec
 */
function tick_initial_state(agent, facts) {
    return example1(agent, facts);

}


function tick_second_state(agent, facts) {
    return example2(agent, facts);

}

/**
 * Invoked when a 'tick' fact spec is advised during 'waiting_buy_request' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'tick' spec
 */
function tick_waiting_buy_request(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'tick' fact spec is advised during 'waiting_buy_confirmation' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'tick' spec
 */
function tick_waiting_buy_confirmation(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'tick' fact spec is advised during 'waiting_cancel_buy_confirmation' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'tick' spec
 */
function tick_waiting_cancel_buy_confirmation(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'tick' fact spec is advised during 'waiting_sell_request' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'tick' spec
 */
function tick_waiting_sell_request(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'tick' fact spec is advised during 'waiting_sell_confirmation' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'tick' spec
 */
function tick_waiting_sell_confirmation(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'tick' fact spec is advised during 'waiting_cancel_sell_confirmation' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'tick' spec
 */
function tick_waiting_cancel_sell_confirmation(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'buy_confirmed' fact spec is advised during 'waiting_buy_confirmation' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'buy_confirmed' spec
 */
function buy_confirmed_waiting_buy_confirmation(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'buy_confirmed' fact spec is advised during 'waiting_cancel_buy_confirmation' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'signal' spec
 */
function buy_confirmed_waiting_cancel_buy_confirmation(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'buy_cancelled' fact spec is advised during 'waiting_cancel_buy_confirmation' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'signal' spec
 */
function buy_cancelled_waiting_cancel_buy_confirmation(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'sell_confirmed' fact spec is advised during 'waiting_sell_confirmation' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'signal' spec
 */
function sell_confirmed_waiting_sell_confirmation(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'sell_confirmed' fact spec is advised during 'waiting_cancel_sell_confirmation' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'signal' spec
 */
function sell_confirmed_waiting_cancel_sell_confirmation(agent, facts) {
    return example1(agent, facts);
}

/**
 * Invoked when a 'sell_cancelled' fact spec is advised during 'waiting_cancel_sell_confirmation' state
 * @agent The object that holds agent profile and parameters
 * @facts The facts values according with the 'signal' spec
 */
function sell_cancelled_waiting_cancel_sell_confirmation(agent, facts) {
    return example1(agent, facts);
}



