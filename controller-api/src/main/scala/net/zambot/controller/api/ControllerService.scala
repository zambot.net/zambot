package net.zambot.controller.api

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.broker.kafka.{KafkaProperties, PartitionKeyStrategy}
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import play.api.libs.json.{Format, Json}

object ControllerService {
  val CONTROLLER_EVENTS_TOPIC_NAME = "controller_events"
}

/**
  * The scheduler service interface.
  * <p>
  * This describes everything that Lagom needs to know about how to serve and
  * consume the SchedulerService.
  */
trait ControllerService extends Service {

  final override def descriptor: Descriptor = {
    import Service._
    named("ControllerApi")
      .withCalls(
        restCall(Method.GET, "/controller/setup", setup _),
        restCall(Method.GET, "/controller/zstatus", zstatus _)
      )
      .withAutoAcl(true)
  }

  def setup: ServiceCall[NotUsed, String]

  def zstatus: ServiceCall[NotUsed, String]

}