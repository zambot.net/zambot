package net.zambot.scheduler.api

import java.time.LocalDateTime
import java.util.UUID

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.broker.kafka.{KafkaProperties, PartitionKeyStrategy}
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import net.zambot.scheduler.model._
import play.api.libs.json.{Format, Json}

object SchedulerService {
  val SCHEDULER_TOPIC_NAME = "scheduler_events"
  val TASK_TOPIC_NAME = "task_events"
  val COMPLETED_TASK_TOPIC_NAME = "completed_task_events"
}

/**
  * The scheduler service interface.
  * <p>
  * This describes everything that Lagom needs to know about how to serve and
  * consume the SchedulerService.
  */
trait SchedulerService extends Service {

  /**
    * Assigns a task to a runner
    * Assigns a task to a runner
    *
    * @param schedulerId The Scheduler identifier
    * @return TaskInfo Body Parameter  Assigns a task to a task runner
    */
  def assignTask(schedulerId: UUID): ServiceCall[TaskRunner, AssignTaskToRunnerResult]

  /**
    * Creates a scheduler
    * Creates a scheduler
    *
    * @return EntityId Body Parameter  Creates a scheduler
    */
  def createScheduler(): ServiceCall[CreateScheduler, EntityId]

  /**
    * Schedules a task
    * Schedules a task to be managed by the scheduler identified by {schedulerId}
    *
    * @param schedulerId The Scheduler identifier
    * @return EntityId Body Parameter  Schedules a task to be managed by the scheduler identified by {schedulerId}
    */
  def createTask(schedulerId: UUID): ServiceCall[CreateTask, EntityId]

  /**
    * List Schedulers
    * List Markets
    *
    * @param nextPageToken     (optional)
    * @param previousPageToken (optional)
    * @param pageSize          (optional)
    * @return PaginatedSchedulerInfo
    */
  def findScheduler(nextPageToken: Option[UUID] = None,
                    previousPageToken: Option[UUID] = None,
                    pageSize: Option[Int] = None,
                    inboundChannelId: Option[UUID] = None,
                    outboundChannelId: Option[UUID] = None): ServiceCall[NotUsed, SchedulerInfoList]

  /**
    * Retrieves Scheduler Information
    *
    * @param schedulerId Scheduler Identifier
    * @return SchedulerInfo
    */
  def getSchedulerInfo(schedulerId: UUID): ServiceCall[NotUsed, SchedulerInfo]

  /**
    * Lists tasks for a scheduler
    *
    * @param schedulerId       The Scheduler identifier
    * @param nextPageToken     (optional)
    * @param previousPageToken (optional)
    * @param pageSize          (optional)
    * @return PaginatedTaskInfo
    */
  def findTask(schedulerId: UUID,
               nextPageToken: Option[UUID] = None,
               previousPageToken: Option[UUID] = None,
               pageSize: Option[Int] = None,
               taskType: Option[String] = None,
               taskStatus: Option[String] = None,
               externalId: Option[UUID] = None): ServiceCall[NotUsed, TaskInfoSummaryList]

  /**
    * Lists results for a task
    *
    * @param schedulerId The Scheduler identifier
    * @param taskId      The Task identifier
    * @param pageSize    (optional)
    * @return PaginatedTaskInfo
    */
  def findTaskResult(schedulerId: UUID, taskId: UUID, pageSize: Option[Int] = None, startDate: Option[String],
                     endDate: Option[String], success: Option[Boolean]): ServiceCall[NotUsed, TaskResultList]

  /**
    * Retrieves task information
    *
    * @param schedulerId The scheduler identifier
    * @param taskId      The task identifier
    * @return TaskInfo
    */
  def getTaskInfo(schedulerId: UUID, taskId: UUID): ServiceCall[NotUsed, TaskInfo]

  /**
    * Updates a scheduler
    * Updates a scheduler
    *
    * @param schedulerId Scheduler identifier
    * @return void Body Parameter  Updates a given scheduler
    */
  def updateScheduler(schedulerId: UUID): ServiceCall[UpdateSchedulerInfo, Done]

  /**
    * Updates a task
    * Updates a task
    *
    * @param schedulerId The scheduler identifier
    * @param taskId      The task identifier
    * @return void Body Parameter
    */
  def completeTask(schedulerId: UUID, taskId: UUID): ServiceCall[CompleteTaskInfo, CompletedTaskInfo]

  def schedulerEventsTopic(): Topic[SchedulerEvent]

  def taskEventsTopic(): Topic[TaskEvent]

  def completedTaskEventsTopic(): Topic[CompletedTaskEvent]

  final override def descriptor = {
    import Service._
    named("SchedulerApi").withCalls(
      restCall(Method.POST, "/scheduler/:schedulerId/task/runner", assignTask _),
      restCall(Method.POST, "/scheduler", createScheduler _),
      restCall(Method.POST, "/scheduler/:schedulerId/task", createTask _),
      restCall(Method.GET, "/scheduler/?nextPageToken&previousPageToken&pageSize&inboundChannelId&outboundChannelId", findScheduler _),
      restCall(Method.GET, "/scheduler/:schedulerId", getSchedulerInfo _),
      restCall(Method.GET, "/scheduler/:schedulerId/task/?nextPageToken&previousPageToken&pageSize&taskType&taskStatus&externalId", findTask _),
      restCall(Method.GET, "/scheduler/:schedulerId/task/:taskId", getTaskInfo _),
      restCall(Method.GET, "/scheduler/:schedulerId/task/:taskId/result/?pageSize&startDate&endDate&success", findTaskResult _),
      restCall(Method.PATCH, "/scheduler/:schedulerId", updateScheduler _),
      restCall(Method.PATCH, "/scheduler/:schedulerId/task/:taskId", completeTask _)
    ).withTopics(
      topic(SchedulerService.SCHEDULER_TOPIC_NAME, schedulerEventsTopic())
        .addProperty(
          KafkaProperties.partitionKeyStrategy,
          PartitionKeyStrategy[SchedulerEvent](e => s"${e.schedulerId}")
        ),
      topic(SchedulerService.TASK_TOPIC_NAME, taskEventsTopic())
        .addProperty(
          KafkaProperties.partitionKeyStrategy,
          PartitionKeyStrategy[TaskEvent](e => s"${e.taskId}")
        ),
      topic(SchedulerService.COMPLETED_TASK_TOPIC_NAME, completedTaskEventsTopic())
        .addProperty(
          KafkaProperties.partitionKeyStrategy,
          PartitionKeyStrategy[CompletedTaskEvent](e => s"${e.taskId}")
        )

    ).withAutoAcl(true)
  }

}

case class SchedulerEvent(schedulerId: String, created: String, status: String, timestamp: String,
                          maxOneTimeTaskExecutions: Int, maxRecurrentTaskExecutions: Int, interval: String, version: Long)

object SchedulerEvent {
  implicit val format: Format[SchedulerEvent] = Json.format[SchedulerEvent]
}

case class TaskEvent(taskId: UUID, created: LocalDateTime, externalId: Option[UUID],
                     parameters: Map[String, String], result: Map[String, String], scheduler_policy: String,
                     status: String, timestamp: LocalDateTime, message: Option[String], success: Option[Boolean])

object TaskEvent {
  implicit val format: Format[TaskEvent] = Json.format[TaskEvent]

}

case class TaskEventResult(values: Map[String, String], message: String, success: Boolean)

object TaskEventResult {
  implicit val format: Format[TaskEventResult] = Json.format[TaskEventResult]
}

case class CompletedTaskEvent(schedulerId: UUID, taskId: UUID, externalId: Option[UUID], created: LocalDateTime,
                              timestamp: LocalDateTime, completedTimestamp: LocalDateTime, message: String,
                              success: Boolean, result: Map[String, String])

object CompletedTaskEvent {
  implicit val format: Format[CompletedTaskEvent] = Json.format[CompletedTaskEvent]
}