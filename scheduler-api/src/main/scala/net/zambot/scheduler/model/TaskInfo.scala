/**
  * Zambot
  * Zambot | Unlimited Trading | [https://zambot.net](https://zambot.net).
  *
  * The version of the OpenAPI document: 1.0.0
  * Contact: gzambon@gmail.com
  *
  * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
  * https://openapi-generator.tech
  * Do not edit the class manually.
  */

package net.zambot.scheduler.model

import java.time.LocalDateTime

import play.api.libs.json._
import java.util.UUID

case class TaskInfo(
                     taskId: UUID,
                     schedulerId: UUID,
                     externalId: Option[UUID],
                     outboundChannelId: Option[UUID],
                     inboundChannelId: Option[UUID],
                     created: LocalDateTime,
                     timestamp: LocalDateTime,
                     status: String,
                     taskType: String,
                     schedulePolicy: String,
                     executionCount: Long,
                     parameters: TaskParameters
                   )

object TaskInfo {
  implicit val formatParameters: Format[TaskParameters] = Json.format
  implicit val formatResult: Format[TaskResult] = Json.format
  implicit val format: Format[TaskInfo] = Json.format
}

