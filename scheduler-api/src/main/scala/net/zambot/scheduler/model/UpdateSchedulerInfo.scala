/**
  * Zambot
  * Zambot | Unlimited Trading | [https://zambot.net](https://zambot.net).
  *
  * The version of the OpenAPI document: 1.0.0
  * Contact: gzambon@gmail.com
  *
  * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
  * https://openapi-generator.tech
  * Do not edit the class manually.
  */

package net.zambot.scheduler.model

import play.api.libs.json._

case class UpdateSchedulerInfo(status: Option[String],
                               maxRecurrentTaskExecutions: Option[Int], maxOneTimeTaskExecutions: Option[Int])

object UpdateSchedulerInfo {
  implicit val format: Format[UpdateSchedulerInfo] = Json.format
}

