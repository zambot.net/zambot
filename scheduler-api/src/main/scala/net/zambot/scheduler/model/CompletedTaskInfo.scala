package net.zambot.scheduler.model

import java.time.LocalDateTime

import play.api.libs.json.{Format, Json}

case class CompletedTaskInfo(id: String, inboundChannelId: Option[String], outboundChannelId: Option[String],
                             created: LocalDateTime, timestamp: LocalDateTime, taskStatus: String, taskType: String,
                             schedulePolicy: String, schedulerId: String, executionCount: Long,
                             parameters: Map[String, String])

object CompletedTaskInfo {
  implicit val format: Format[CompletedTaskInfo] = Json.format
}
