package net.zambot.scheduler.model

import play.api.libs.json._

case class AssignTaskToRunnerResult(success: String,
                                    message: String,
                                    result: Option[TaskInfo])

object AssignTaskToRunnerResult {

  implicit val formatTaskInfo: Format[TaskInfo] = Json.format
  implicit val format: Format[AssignTaskToRunnerResult] = Json.format

}