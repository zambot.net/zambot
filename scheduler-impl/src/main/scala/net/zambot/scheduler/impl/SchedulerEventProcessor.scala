package net.zambot.scheduler.impl

import java.time.{LocalDateTime, ZoneId}
import java.util.{Date, UUID}

import akka.Done
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, EventStreamElement, ReadSideProcessor}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.jdk.CollectionConverters._

class SchedulerEventProcessor(session: CassandraSession, readSide: CassandraReadSide)
                             (implicit ec: ExecutionContext) extends ReadSideProcessor[SchedulerEvent] {

  override def aggregateTags: Set[AggregateEventTag[SchedulerEvent]] = Set(SchedulerEvent.Tag)

  override def buildHandler(): ReadSideProcessor.ReadSideHandler[SchedulerEvent] = {
    readSide.builder[SchedulerEvent]("eventProcessor-scheduler_events")
      .setGlobalPrepare(() => createTickerTable)
      .setEventHandler[SchedulerInitialized](processSchedulerEvent)
      .setEventHandler[SchedulerUpdated](processSchedulerEvent)
      .setPrepare(ev => prepareSchedulerSession)
      .build

  }

  private def createTickerTable: Future[Done] = {
    session.executeCreateTable("CREATE TABLE IF NOT EXISTS scheduler.scheduler_list " +
      "(scheduler_id UUID, inbound_channel_id UUID, outbound_channel_id UUID, updated timestamp," +
      " parameters frozen<map<text,text>>, status text, created timestamp," +
      " interval text, max_onetime_task_executions int, max_recurrent_task_executions int, version bigint," +
      "PRIMARY KEY(scheduler_id))")
  }

  private val writeSchedulerPromise = Promise[PreparedStatement] // initialized in prepare
  private def writeScheduler: Future[PreparedStatement] = writeSchedulerPromise.future


  private def prepareSchedulerSession: Future[Done] = {
    val f = session.prepare("INSERT INTO scheduler.scheduler_list " +
      "(scheduler_id, inbound_channel_id, outbound_channel_id, " +
      "parameters, status, created, updated, interval," +
      " max_onetime_task_executions, max_recurrent_task_executions, version)" +
      " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

    writeSchedulerPromise.completeWith(f)
    f.map(_ => Done)

  }

  private def processSchedulerEvent(ev: EventStreamElement[SchedulerEvent]): Future[List[BoundStatement]] = ev.event match {
    case schedulerInitialized: SchedulerInitialized => {
      writeScheduler.map {
        ps =>
          val binds = ps.bind()
            .setUUID("scheduler_id", java.util.UUID.fromString(schedulerInitialized.id))
            .setMap("parameters", schedulerInitialized.parameters.asJava)
            .setString("status", schedulerInitialized.status)
            .setTimestamp("created", Date.from(LocalDateTime.parse(schedulerInitialized.created).atZone(ZoneId.systemDefault()).toInstant()))
            .setTimestamp("updated", Date.from(LocalDateTime.parse(schedulerInitialized.timestamp).atZone(ZoneId.systemDefault()).toInstant()))
            .setString("interval", schedulerInitialized.timeWindowDuration)
            .setInt("max_onetime_task_executions", schedulerInitialized.maxOneTimeTaskExecutions)
            .setInt("max_recurrent_task_executions", schedulerInitialized.maxRecurrentTaskExecutions)
            .setLong("version", schedulerInitialized.version)

          if (schedulerInitialized.inboundChannelId.isEmpty) {
            binds.setToNull("inbound_channel_id")
          } else {
            binds.setUUID("inbound_channel_id", UUID.fromString(schedulerInitialized.inboundChannelId.get))
          }

          if (schedulerInitialized.outboundChannelId.isEmpty) {
            binds.setToNull("outbound_channel_id")
          } else {
            binds.setUUID("outbound_channel_id", UUID.fromString(schedulerInitialized.outboundChannelId.get))
          }

          List(binds)

      }

    }

    case schedulerUpdated: SchedulerUpdated => {
      writeScheduler.map {
        ps =>
          val binds = ps.bind()
            .setUUID("scheduler_id", schedulerUpdated.schedulerId)
            .setMap("parameters", schedulerUpdated.parameters.asJava)
            .setString("status", schedulerUpdated.status)
            .setTimestamp("created", Date.from(LocalDateTime.parse(schedulerUpdated.created).atZone(ZoneId.systemDefault()).toInstant()))
            .setTimestamp("updated", Date.from(LocalDateTime.parse(schedulerUpdated.timestamp).atZone(ZoneId.systemDefault()).toInstant()))
            .setString("interval", schedulerUpdated.timeWindowDuration)
            .setInt("max_onetime_task_executions", schedulerUpdated.maxOneTimeTaskExecutions)
            .setInt("max_recurrent_task_executions", schedulerUpdated.maxRecurrentTaskExecutions)
            .setLong("version", schedulerUpdated.version)
          List(binds)
      }
    }

    case _ => Future {
      Nil
    }

  }

}