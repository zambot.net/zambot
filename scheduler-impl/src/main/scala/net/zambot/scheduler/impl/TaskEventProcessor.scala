package net.zambot.scheduler.impl

import java.time.{LocalDateTime, ZoneId}
import java.util.Date

import akka.Done
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, EventStreamElement, ReadSideProcessor}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.jdk.CollectionConverters._


class TaskEventProcessor(session: CassandraSession, readSide: CassandraReadSide)
                        (implicit ec: ExecutionContext) extends ReadSideProcessor[SchedulerEvent] {

  override def aggregateTags: Set[AggregateEventTag[SchedulerEvent]] = Set(TaskEvent.Tag)

  override def buildHandler(): ReadSideProcessor.ReadSideHandler[SchedulerEvent] = {
    readSide.builder[SchedulerEvent]("eventProcessor-scheduler_task_events")
      .setGlobalPrepare(() => globalPrepare)
      .setEventHandler[TaskScheduled](processTaskEvent)
      .setEventHandler[TaskAssigned](processTaskEvent)
      .setEventHandler[CompletedTask](processTaskEvent)
      .setPrepare(ev => prepareTaskSession)
      .build

  }

  private def globalPrepare: Future[Done] = {
    //    createTaskType.flatMap(_ => createTaskEventTable)
    createTaskEventTable
  }

  private def createTaskEventTable: Future[Done] = {
    session.executeCreateTable("CREATE TABLE IF NOT EXISTS scheduler.scheduler_task_list (scheduler_id UUID," +
      " task_id UUID, updated timestamp, external_id UUID, created timestamp, status text," +
      "task_type text, schedule_policy text, parameters frozen<map<text,text>>, primary key(scheduler_id, task_id))")

  }

  private val writeTaskPromise = Promise[PreparedStatement] // initialized in prepare
  private def writeTaskEvent: Future[PreparedStatement] = writeTaskPromise.future

  private def prepareTaskSession: Future[Done] = {
    val f = session.prepare("INSERT INTO scheduler.scheduler_task_list (scheduler_id, task_id, updated, external_id, created," +
      "status, task_type, schedule_policy, parameters) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)")

    writeTaskPromise.completeWith(f)
    f.map(_ => Done)

  }

  def createTaskBind(ps: PreparedStatement, schedulerId: String, taskId: String, updated: String,
                     externalId: Option[String], created: String, parameters: Map[String, String],
                     schedulePolicy: String, status: String, taskType: String): BoundStatement = {
    val bind = ps.bind()
      .setUUID("scheduler_id", java.util.UUID.fromString(schedulerId))
      .setUUID("task_id", java.util.UUID.fromString(taskId))
      .setTimestamp("updated", Date.from(LocalDateTime.parse(updated).atZone(ZoneId.systemDefault()).toInstant()))
      .setTimestamp("created", Date.from(LocalDateTime.parse(created).atZone(ZoneId.systemDefault()).toInstant()))
      .setMap("parameters", parameters.asJava)
      .setString("schedule_policy", schedulePolicy)
      .setString("status", status)
      .setString("task_type", taskType)

    externalId match {
      case Some(s) => bind.setUUID("external_id", java.util.UUID.fromString(s))
      case None => bind.setToNull("external_id")

    }

    bind

  }

  private def processTaskEvent(ev: EventStreamElement[SchedulerEvent]): Future[List[BoundStatement]] = ev.event match {
    case taskScheduledEvent: TaskScheduled => {
      writeTaskEvent.map { ps =>
        List(createTaskBind(
          ps = ps,
          schedulerId = taskScheduledEvent.schedulerId,
          taskId = taskScheduledEvent.taskId,
          updated = taskScheduledEvent.updated,
          externalId = taskScheduledEvent.externalId,
          created = taskScheduledEvent.created,
          parameters = taskScheduledEvent.parameters,
          schedulePolicy = taskScheduledEvent.schedulePolicy,
          status = taskScheduledEvent.status,
          taskType = taskScheduledEvent.taskType)
        )
      }
    }

    case taskAssignedEvent: TaskAssigned => {
      writeTaskEvent.map { ps =>
        List(createTaskBind(
          ps = ps,
          schedulerId = taskAssignedEvent.schedulerId,
          taskId = taskAssignedEvent.taskId,
          updated = taskAssignedEvent.updated,
          externalId = taskAssignedEvent.externalId,
          created = taskAssignedEvent.created,
          parameters = taskAssignedEvent.parameters,
          schedulePolicy = taskAssignedEvent.schedulePolicy,
          status = taskAssignedEvent.status,
          taskType = taskAssignedEvent.taskType)
        )
      }
    }

    case taskCompletedEvent: CompletedTask => {
      writeTaskEvent.map { ps =>
        List(createTaskBind(
          ps = ps,
          schedulerId = taskCompletedEvent.schedulerId,
          taskId = taskCompletedEvent.taskId,
          updated = taskCompletedEvent.updated,
          externalId = taskCompletedEvent.externalId,
          created = taskCompletedEvent.created,
          parameters = taskCompletedEvent.parameters,
          schedulePolicy = taskCompletedEvent.schedulePolicy,
          status = taskCompletedEvent.status,
          taskType = taskCompletedEvent.taskType)
        )
      }
    }

    case _ => Future {
      Nil
    }

  }
}