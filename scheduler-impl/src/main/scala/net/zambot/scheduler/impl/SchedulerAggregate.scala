package net.zambot.scheduler.impl

import java.time.{Duration, LocalDateTime}
import java.util.UUID

import akka.actor.typed.{ActorRef, Behavior}
import akka.cluster.sharding.typed.scaladsl._
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior, ReplyEffect}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventTag, AkkaTaggerAdapter}
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import org.slf4j.LoggerFactory
import play.api.libs.json.{Format, Json, _}

import scala.collection.immutable.{ListMap, Seq}

object SchedulerBehavior {

  def create(entityContext: EntityContext[SchedulerCommand]): Behavior[SchedulerCommand] = {
    val persistenceId: PersistenceId = PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId)

    create(persistenceId)
      .withTagger(
        AkkaTaggerAdapter.fromLagom(entityContext, SchedulerEvent.Tag)
      ).withTagger(
      AkkaTaggerAdapter.fromLagom(entityContext, TaskEvent.Tag)
    )

  }

  /*
   * This method is extracted to write unit tests that are completely independendant to Akka Cluster.
   */
  private[impl] def create(persistenceId: PersistenceId) = EventSourcedBehavior
    .withEnforcedReplies[SchedulerCommand, SchedulerEvent, SchedulerState](
      persistenceId = persistenceId,
      emptyState = SchedulerState.initial,
      commandHandler = (schedulerAggregate, cmd) =>
        schedulerAggregate.applyCommand(cmd),
      eventHandler = (schedulerAggregate, evt) =>
        schedulerAggregate.applyEvent(evt)
    ).snapshotWhen {
    case (state, event, sequence) => sequence % 100 == 0
  }
}

/**
  * The current state of the Aggregate.
  */
case class SchedulerState(id: String,
                          created: String,
                          timestamp: String,
                          status: String,
                          timeWindowStarted: String,
                          timeWindowSequence: Long,
                          timeWindowDuration: String,
                          recurrentTaskExecutionCount: Int,
                          maxRecurrentTaskExecutions: Int,
                          recurrentTasks: Map[String, Task],
                          oneTimeTaskExecutionCount: Int,
                          maxOneTimeTaskExecutions: Int,
                          oneTimeTasks: Map[String, Task],
                          runningTasks: Map[String, Task],
                          unmanagedTasks: Map[String, Task],
                          version: Long,
                          inboundChannelId: Option[String],
                          outboundChannelId: Option[String],
                          parameters: Map[String, String]) {

  private val log = LoggerFactory.getLogger("SchedulerState")

  def applyCommand(cmd: SchedulerCommand): ReplyEffect[SchedulerEvent, SchedulerState] = status match {
    case "notInitialized" => cmd match {
      case c: InitializeScheduler => onInitializeScheduler(c)
      case c: GetSchedulerInfo => Effect.reply(c.replyTo)(Rejected(s"Scheduler not initialized: $cmd"))
      case c: GetTaskInfo => Effect.reply(c.replyTo)(Rejected(s"Scheduler not initialized: $cmd"))
      case c: ScheduleTask => Effect.reply(c.replyTo)(Rejected(s"Scheduler not initialized: $cmd"))
      case c: UpdateScheduler => Effect.reply(c.replyTo)(Rejected(s"Scheduler not initialized: $cmd"))
      case c: AssignTask => Effect.reply(c.replyTo)(Rejected(s"Scheduler not initialized: $cmd"))
      case c: CompleteTask => Effect.reply(c.replyTo)(Rejected(s"Scheduler not initialized: $cmd"))
    }
    case "paused" => cmd match {
      case c: InitializeScheduler => onInitializeScheduler(c)
      case c: GetSchedulerInfo => onGetSchedulerInfo(c)
      case c: GetTaskInfo => onGetTaskInfo(c)
      case c: ScheduleTask => Effect.reply(c.replyTo)(Rejected(s"Scheduler paused: $cmd"))
      case c: UpdateScheduler => Effect.reply(c.replyTo)(Rejected(s"Scheduler paused: $cmd"))
      case c: AssignTask => Effect.reply(c.replyTo)(Rejected(s"Scheduler paused: $cmd"))
      case c: CompleteTask => Effect.reply(c.replyTo)(Rejected(s"Scheduler paused: $cmd"))
    }
    case "running" => cmd match {
      case c: InitializeScheduler => Effect.reply(c.replyTo)(Rejected(s"Scheduler already initialized"))
      case c: GetSchedulerInfo => onGetSchedulerInfo(c)
      case c: GetTaskInfo => onGetTaskInfo(c)
      case c: ScheduleTask => onScheduleTask(c)
      case c: UpdateScheduler => onUpdateScheduler(c)
      case c: AssignTask => onAssignTask(c)
      case c: CompleteTask => onCompleteTask(c)
    }

  }

  def applyEvent(evt: SchedulerEvent): SchedulerState =
    evt match {
      case e: SchedulerInitialized => initializeScheduler(e)
      case e: TaskScheduled => scheduleTask(e)
      case e: SchedulerUpdated => updateScheduler(e)
      case e: CompletedTask => completeTask(e)
      case e: TaskAssigned => assignTask(e)
      case e: TimeWindowChanged => changeTimeWindow(e)
    }

  def supportedSchedulePolicies = List("oneTime", "recurrent", "unmanaged")

  def supportedSchedulerStatus = List("notInitialized", "running", "paused")

  def validateTaskStatus(taskStatus: String): Seq[String] =
    if (supportedSchedulerStatus.contains(taskStatus)) Nil else Seq(s"Status not supported. Supported Status are: $supportedSchedulerStatus")

  def validateSchedulerPolicy(schedulerPolicy: String): Seq[String] =
    if (supportedSchedulePolicies.contains(schedulerPolicy)) Nil else Seq(s"SchedulerPolicy not supported. Supported Scheduler policies are: ${supportedSchedulePolicies}")

  def validateRecurrentTaskId(taskId: String): Seq[String] =
    if (recurrentTasks.contains(taskId)) Seq(s"Task with id ${taskId} is already scheduled on recurrentTasks.") else Nil

  def validateScheduledTaskId(taskId: String): Seq[String] =
    if (oneTimeTasks.contains(taskId)) Seq(s"Task with id ${taskId} is already scheduled on oneTimeTasks.") else Nil

  def validateTaskId(taskId: String): Seq[String] = validateRecurrentTaskId(taskId) ++ validateScheduledTaskId(taskId)

  def validateScheduleTask(task: ScheduleTask): Seq[String] =
    validateSchedulerPolicy(task.schedulePolicy)

  private def onScheduleTask(cmd: ScheduleTask): ReplyEffect[SchedulerEvent, SchedulerState] = {
    val errorList = validateScheduleTask(cmd)
    errorList match {
      case Nil => {
        val now = LocalDateTime.now
        Effect
          .persist(
            TaskScheduled(
              taskId = cmd.taskId,
              externalId = cmd.externalId.map(id => id),
              taskType = cmd.taskType,
              schedulePolicy = cmd.schedulePolicy,
              parameters = cmd.parameters,
              created = now.toString,
              updated = now.toString,
              timestamp = now.toString,
              status = "scheduled",
              schedulerId = id
            )
          ).thenReply(cmd.replyTo) { _ =>
          TaskId(cmd.taskId)
        }
      }
      case x: Seq[String] => Effect.reply(cmd.replyTo)(Rejected(s"Error creating task: ${errorList.fold("")((acc, s) => acc + ", " + s)}"))
    }
  }

  def validateUpdateScheduler(cmd: UpdateScheduler): Seq[String] = validateTaskStatus(cmd.status.getOrElse(status))

  private def onUpdateScheduler(cmd: UpdateScheduler): ReplyEffect[SchedulerEvent, SchedulerState] = {
    val errorList = validateUpdateScheduler(cmd)
    errorList match {
      case Nil => Effect
        .persist(
          SchedulerUpdated(
            schedulerId = java.util.UUID.fromString(id),
            timeWindowDuration = timeWindowDuration,
            status = cmd.status.getOrElse(status), maxOneTimeTaskExecutions = cmd.maxOneTimeTaskExecutions.getOrElse(maxOneTimeTaskExecutions),
            maxRecurrentTaskExecutions = cmd.maxRecurrentTaskExecutions.getOrElse(maxRecurrentTaskExecutions),
            created = created, timestamp = LocalDateTime.now.toString, version = version + 1, parameters = parameters
          )
        ).thenReply(cmd.replyTo) { _ =>
        Accepted
      }
      case errors: Seq[String] => Effect.reply(cmd.replyTo)(Rejected(s"Error updating scheduler: ${errors.fold("")((acc, s) => acc + ", " + s)}"))
    }
  }

  private def onInitializeScheduler(cmd: InitializeScheduler): ReplyEffect[SchedulerEvent, SchedulerState] =
    status match {
      case "notInitialized" => Effect
        .persist({
          val now = LocalDateTime.now
          SchedulerInitialized(id = cmd.id,
            parameters = cmd.parameters,
            status = "running",
            timeWindowDuration = cmd.timeWindowDuration,
            maxRecurrentTaskExecutions = cmd.maxRecurrentTaskExecutions,
            maxOneTimeTaskExecutions = cmd.maxOneTimeTaskExecutions,
            created = now.toString,
            timestamp = now.toString,
            inboundChannelId = cmd.inboundChannelId,
            outboundChannelId = cmd.outboundChannelId,
            version = 1
          )
        }
        ).thenReply(cmd.replyTo) { _ =>
        SchedulerId(cmd.id)
      }
      case _ => Effect.reply(cmd.replyTo)(Rejected("Already initialized."))

    }

  private def onGetSchedulerInfo(cmd: GetSchedulerInfo): ReplyEffect[SchedulerEvent, SchedulerState] =
    Effect.reply(cmd.replyTo)(SchedulerInfoSummary(
      schedulerId = java.util.UUID.fromString(id),
      inboundChannelId = inboundChannelId.fold(None: Option[UUID])(id => Some(java.util.UUID.fromString(id))),
      outboundChannelId = outboundChannelId.fold(None: Option[UUID])(id => Some(java.util.UUID.fromString(id))),
      created = created,
      status = status,
      timestamp = timestamp,
      maxOneTimeTaskExecutions = maxOneTimeTaskExecutions,
      maxRecurrentTaskExecutions = maxRecurrentTaskExecutions,
      interval = timeWindowDuration,
      version = version,
      parameters = parameters))

  def getTaskResult(result: Option[TaskResult]): Option[TaskInfoResult] = {
    result.fold(None: Option[TaskInfoResult])(ti => Some(TaskInfoResult(
      success = ti.success,
      message = ti.message,
      values = ti.values)))
  }

  private def onGetTaskInfo(cmd: GetTaskInfo): ReplyEffect[SchedulerEvent, SchedulerState] = {
    runningTasks.get(cmd.taskId).fold(
      recurrentTasks.get(cmd.taskId).fold(
        oneTimeTasks.get(cmd.taskId).fold(
          unmanagedTasks.get(cmd.taskId).fold(
            None: Option[Task]
          )(unmanagedTask => Some(unmanagedTask))
        )(oneTimeTask => Some(oneTimeTask))
      )(runningTask => Some(runningTask))
    )(runningTasks => Some(runningTasks)) match {
      case Some(task) => Effect.reply(cmd.replyTo)(
        TaskInfoSummary(
          id = task.id,
          externalId = task.externalId,
          created = LocalDateTime.parse(task.created),
          timestamp = LocalDateTime.parse(task.timestamp),
          taskStatus = task.taskStatus,
          taskType = task.taskType,
          schedulePolicy = task.schedulePolicy,
          parameters = task.parameters,
          executionCount = task.executionCount,
          schedulerId = id
        ))
      case None => Effect.reply(cmd.replyTo)(Rejected("Not found."))
    }

  }

  private def onAssignTask(cmd: AssignTask): ReplyEffect[SchedulerEvent, SchedulerState] = {
    def getNextOneTimeTask(count: Int, max: Int): Option[(String, Task)] = {
      if (count < max) if (oneTimeTasks.isEmpty) None else Some("oneTime", oneTimeTasks.head._2) else None
    }

    def getNextRecurrentTask(count: Int, max: Int): Option[(String, Task)] = if (count < max) if (recurrentTasks.isEmpty) None else Some("recurrent", recurrentTasks.head._2) else None

    def getNextTask(oneTimeTaskExecutionCount: Int, recurrentTaskExecutionCount: Int): Option[(String, Task)] = {
      val nextOneTime = getNextOneTimeTask(oneTimeTaskExecutionCount, maxOneTimeTaskExecutions)
      if (nextOneTime.isEmpty) {
        val nextRecurrentTask = getNextRecurrentTask(recurrentTaskExecutionCount, maxRecurrentTaskExecutions)
        if (nextRecurrentTask.isEmpty) None
        else nextRecurrentTask
      }
      else nextOneTime
    }

    val now = LocalDateTime.now
    val newTimeWindow = now.isAfter(LocalDateTime.parse(timeWindowStarted).plus(Duration.parse(timeWindowDuration)))

    val nextTask: Option[(String, Task)] = if (newTimeWindow)
      getNextTask(oneTimeTaskExecutionCount = 0, recurrentTaskExecutionCount = 0)
    else getNextTask(oneTimeTaskExecutionCount = oneTimeTaskExecutionCount, recurrentTaskExecutionCount = recurrentTaskExecutionCount)

    val timeWindowChangedEvent = if (newTimeWindow) Seq(TimeWindowChanged(now.toString, timeWindowSequence + 1)) else None.toList

    if (nextTask.isEmpty) Effect.reply(cmd.replyTo)(Rejected("There are no available tasks to be run at this time."))
    else {
      val now = LocalDateTime.now.toString
      val t = nextTask.get._2
      Effect
        .persist(
          timeWindowChangedEvent ++ Seq(
            TaskAssigned(
              taskId = t.id,
              externalId = t.externalId,
              taskType = t.taskType,
              schedulePolicy = t.schedulePolicy,
              parameters = t.parameters,
              created = t.created,
              updated = now,
              status = "running",
              schedulerId = id
            ))
        ).thenReply(cmd.replyTo) { _ =>
        TaskInfoSummary(
          id = t.id,
          externalId = t.externalId,
          created = java.time.LocalDateTime.parse(t.created),
          timestamp = java.time.LocalDateTime.parse(now),
          taskStatus = t.taskStatus,
          taskType = t.taskType,
          schedulePolicy = t.schedulePolicy,
          parameters = t.parameters,
          executionCount = t.executionCount,
          schedulerId = id)

      }
    }
  }

  def onCompleteTask(cmd: CompleteTask): ReplyEffect[SchedulerEvent, SchedulerState] = {
    val taskId = cmd.taskId.toString
    val task: Option[Task] = runningTasks.get(taskId).fold(
      recurrentTasks.get(taskId).fold(
        unmanagedTasks.get(taskId).fold(
          None: Option[Task]
        )(t => Some(t))
      )(t => Some(t))
    )(t => Some(t))
    task match {
      case Some(t) => Effect.persist(
        CompletedTask(
          taskId = t.id,
          externalId = t.externalId,
          taskType = t.taskType,
          schedulePolicy = t.schedulePolicy,
          parameters = t.parameters,
          created = t.created,
          updated = t.timestamp,
          status = "finished",
          message = cmd.result.message,
          success = cmd.result.success,
          taskResults = cmd.result.values,
          schedulerId = id,
          when = cmd.when.toString
        )
      ).thenReply(cmd.replyTo) {
        _ =>
          TaskInfoSummary(
            id = t.id,
            externalId = None,
            created = LocalDateTime.parse(t.created),
            timestamp = LocalDateTime.parse(t.timestamp),
            taskStatus = "finished",
            taskType = t.taskType,
            schedulePolicy = t.schedulePolicy,
            schedulerId = id,
            executionCount = t.executionCount,
            parameters = cmd.result.values

          )
      }
      case None => Effect.reply(cmd.replyTo)(Rejected("Task not found."))
    }
  }

  private def initializeScheduler(event: SchedulerInitialized) =
    copy(id = event.id,
      status = event.status,
      timeWindowDuration = event.timeWindowDuration,
      maxOneTimeTaskExecutions = event.maxOneTimeTaskExecutions,
      maxRecurrentTaskExecutions = event.maxRecurrentTaskExecutions,
      timestamp = LocalDateTime.now.toString,
      version = event.version,
      parameters = event.parameters

    )

  private def scheduleTask(event: TaskScheduled): SchedulerState = {
    val task = Task(
      id = event.taskId,
      externalId = event.externalId.map(id => id),
      created = LocalDateTime.now.toString,
      timestamp = LocalDateTime.now.toString,
      taskStatus = "scheduled",
      taskType = event.taskType,
      schedulePolicy = event.schedulePolicy,
      executionCount = 0,
      parameters = event.parameters
    )

    event.schedulePolicy match {
      case "recurrent" => copy(
        recurrentTasks = recurrentTasks + (task.id -> task)
      )
      case "oneTime" => copy(
        oneTimeTasks = oneTimeTasks + (task.id -> task)
      )
      case "unmanaged" => copy(
        unmanagedTasks = unmanagedTasks + (task.id -> task)
      )
      case _ => if (log.isWarnEnabled) log.warn(s"Invalid SchedulerPolicy: ${event.schedulePolicy}")
        copy()
    }

  }

  private def updateScheduler(event: SchedulerUpdated): SchedulerState = copy(
    status = event.status,
    maxRecurrentTaskExecutions = event.maxRecurrentTaskExecutions,
    maxOneTimeTaskExecutions = event.maxOneTimeTaskExecutions,
    timestamp = event.timestamp,
    version = event.version
  )

  private def completeTask(event: CompletedTask): SchedulerState = {
    event.schedulePolicy match {
      case "recurrent" => runningTasks.get(event.taskId).fold(
        copy()
      )(
        task => copy(
          runningTasks = runningTasks - event.taskId,
          recurrentTasks = recurrentTasks + (
            task.id -> task.copy(executionCount = task.executionCount + 1, taskStatus = "scheduled")
            )
        )
      )
      case "oneTime" => copy(
        runningTasks = runningTasks - event.taskId
      )
      case "unmanaged" => unmanagedTasks.get(event.taskId).fold(
        copy()
      )(task => copy(unmanagedTasks = unmanagedTasks + (task.id -> task.copy(executionCount = task.executionCount + 1))))
      case _ => copy()
    }
  }

  private def assignTask(event: TaskAssigned): SchedulerState = {
    event.schedulePolicy match {
      case "recurrent" => {
        copy(recurrentTaskExecutionCount = recurrentTaskExecutionCount + 1,
          recurrentTasks = recurrentTasks - event.taskId,
          runningTasks = runningTasks + (event.taskId -> recurrentTasks.get(event.taskId).get.copy(taskStatus = event.status)),
          timestamp = event.updated
        )
      }
      case "oneTime" => copy(
        oneTimeTaskExecutionCount = oneTimeTaskExecutionCount + 1,
        oneTimeTasks = oneTimeTasks - event.taskId,
        runningTasks = runningTasks + (event.taskId -> oneTimeTasks.get(event.taskId).get.copy(taskStatus = event.status)),
        timestamp = event.updated
      )
      case _ => {
        // log WARN here
        copy(timestamp = LocalDateTime.now.toString)

      }

    }
  }

  private def changeTimeWindow(event: TimeWindowChanged): SchedulerState = {
    copy(oneTimeTaskExecutionCount = 0,
      recurrentTaskExecutionCount = 0,
      timeWindowStarted = event.startTime,
      timeWindowSequence = event.sequence,
      timestamp = event.startTime,
    )
  }

}

object SchedulerState {

  def apply(id: String,
            created: String,
            timestamp: String,
            status: String,
            timeWindowStarted: String,
            timeWindowSequence: Long,
            timeWindowDuration: String,
            recurrentTaskExecutionCount: Int,
            maxRecurrentTaskExecutions: Int,
            recurrentTasks: Map[String, Task],
            oneTimeTaskExecutionCount: Int,
            maxOneTimeTaskExecutions: Int,
            oneTimeTasks: Map[String, Task],
            runningTasks: Map[String, Task],
            unmanagedTasks: Map[String, Task],
            version: Long,
            inboundChannelId: Option[String],
            outboundChannelId: Option[String],
            parameters: Map[String, String]): SchedulerState = {

    new SchedulerState(id,
      created,
      timestamp,
      status,
      timeWindowStarted,
      timeWindowSequence,
      timeWindowDuration,
      recurrentTaskExecutionCount,
      maxRecurrentTaskExecutions,
      ListMap(recurrentTasks.toList: _*),
      oneTimeTaskExecutionCount,
      maxOneTimeTaskExecutions,
      ListMap(oneTimeTasks.toList: _*),
      runningTasks,
      unmanagedTasks,
      version,
      inboundChannelId,
      outboundChannelId,
      parameters)
  }

  /**
    * The initial state. This is used if there is no snapshotted state to be found.
    */
  def initial: SchedulerState = SchedulerState(id = "",
    parameters = Map.empty,
    created = LocalDateTime.now.toString,
    timestamp = LocalDateTime.now.toString,
    status = "notInitialized",
    timeWindowStarted = LocalDateTime.now.toString,
    timeWindowSequence = 0,
    timeWindowDuration = "PT1H",
    recurrentTaskExecutionCount = 0,
    maxRecurrentTaskExecutions = 2,
    recurrentTasks = ListMap.empty,
    oneTimeTaskExecutionCount = 0,
    maxOneTimeTaskExecutions = 8,
    oneTimeTasks = ListMap.empty,
    runningTasks = ListMap.empty,
    unmanagedTasks = ListMap.empty,
    inboundChannelId = None,
    outboundChannelId = None,
    version = 0)

  /**
    * The [[EventSourcedBehavior]] instances (aka Aggregates) run on sharded actors inside the Akka Cluster.
    * When sharding actors and distributing them across the cluster, each aggregate is
    * namespaced under a typekey that specifies a name and also the type of the commands
    * that sharded actor can receive.
    */
  val typeKey = EntityTypeKey[SchedulerCommand]("SchedulerAggregate")

  /**
    * Format for the Scheduler state.
    *
    * Persisted entities get snapshotted every configured number of events. This
    * means the state gets stored to the database, so that when the aggregate gets
    * loaded, you don't need to replay all the events, just the ones since the
    * snapshot. Hence, a JSON format needs to be declared so that it can be
    * serialized and deserialized when storing to and from the database.
    */

  import play.api.libs.functional.syntax._
  import play.api.libs.json.Reads._
  import play.api.libs.json._

  implicit val taskFormat: Format[Task] = Json.format

  implicit val schedulerStateReads: Reads[SchedulerState] = {

    (
      (JsPath \ "id").read[String] and
        (JsPath \ "created").read[String] and
        (JsPath \ "timestamp").read[String] and
        (JsPath \ "status").read[String] and
        (JsPath \ "timeWindowStarted").read[String] and
        (JsPath \ "timeWindowSequence").read[Long] and
        (JsPath \ "timeWindowDuration").read[String] and
        (JsPath \ "recurrentTaskExecutionCount").read[Int] and
        (JsPath \ "maxRecurrentTaskExecutions").read[Int] and
        (JsPath \ "recurrentTasks").read[Map[String, Task]] and
        (JsPath \ "oneTimeTaskExecutionCount").read[Int] and
        (JsPath \ "maxOneTimeTaskExecutions").read[Int] and
        (JsPath \ "oneTimeTasks").read[Map[String, Task]] and
        (JsPath \ "runningTasks").read[Map[String, Task]] and
        (JsPath \ "unmanagedTasks").read[Map[String, Task]] and
        (JsPath \ "version").read[Long] and
        (JsPath \ "inboundChannelId").readNullable[String] and
        (JsPath \ "outboundChannelId").readNullable[String] and
        (JsPath \ "parameters").read[Map[String, String]]
      ) (SchedulerState.apply _)
  }

  implicit val schedulerStateFormat: Format[SchedulerState] = Json.format

}

case class Task(id: String, externalId: Option[String], created: String, timestamp: String,
                taskStatus: String, taskType: String, schedulePolicy: String, executionCount: Int,
                parameters: Map[String, String])

case class TaskResult(success: Boolean, message: String, values: Map[String, String])

object TaskResult {
  implicit val format: Format[TaskResult] = Json.format
}


/**
  * This interface defines all the events that the SchedulerAggregate supports.
  */
sealed trait SchedulerEvent extends AggregateEvent[SchedulerEvent]

object SchedulerEvent {
  val Tag: AggregateEventTag[SchedulerEvent] = AggregateEventTag[SchedulerEvent]
}

object TaskEvent {
  val Tag: AggregateEventTag[SchedulerEvent] = AggregateEventTag[SchedulerEvent]
}

object TimeWindowChangedEvent {
  val Tag: AggregateEventTag[SchedulerEvent] = AggregateEventTag[SchedulerEvent]
}

/**
  * Events
  */
case class SchedulerInitialized(id: String,
                                status: String,
                                timeWindowDuration: String,
                                maxRecurrentTaskExecutions: Int,
                                maxOneTimeTaskExecutions: Int,
                                timestamp: String,
                                created: String,
                                version: Long,
                                inboundChannelId: Option[String],
                                outboundChannelId: Option[String],
                                parameters: Map[String, String]) extends SchedulerEvent {
  def aggregateTag: AggregateEventTag[SchedulerEvent] = SchedulerEvent.Tag

}

object SchedulerInitialized {
  implicit val format: Format[SchedulerInitialized] = Json.format

}

case class SchedulerUpdated(schedulerId: UUID, status: String, maxOneTimeTaskExecutions: Int,
                            timeWindowDuration: String, maxRecurrentTaskExecutions: Int, timestamp: String,
                            created: String, version: Long, parameters: Map[String, String]) extends SchedulerEvent {

  def aggregateTag: AggregateEventTag[SchedulerEvent] = SchedulerEvent.Tag

}

case class TimeWindowChanged(startTime: String, sequence: Long) extends SchedulerEvent {
  def aggregateTag: AggregateEventTag[SchedulerEvent] = TimeWindowChangedEvent.Tag
}

object TimeWindowChanged {

  implicit val format: Format[TimeWindowChanged] = Json.format
}

object SchedulerUpdated {
  implicit val format: Format[SchedulerUpdated] = Json.format
}

case class TaskScheduled(taskId: String, externalId: Option[String], schedulerId: String,
                         taskType: String, schedulePolicy: String, parameters: Map[String, String],
                         created: String, updated: String, status: String, timestamp: String) extends SchedulerEvent {
  def aggregateTag: AggregateEventTag[SchedulerEvent] = TaskEvent.Tag
}

object TaskScheduled {
  implicit val format: Format[TaskScheduled] = Json.format
}

case class TaskAssigned(taskId: String, externalId: Option[String], schedulerId: String,
                        taskType: String, schedulePolicy: String, parameters: Map[String, String],
                        created: String, updated: String, status: String) extends SchedulerEvent {
  def aggregateTag: AggregateEventTag[SchedulerEvent] = TaskEvent.Tag
}

object TaskAssigned {
  implicit val format: Format[TaskAssigned] = Json.format
}

case class CompletedTask(taskId: String, externalId: Option[String], schedulerId: String, taskType: String,
                         schedulePolicy: String, parameters: Map[String, String],
                         created: String, updated: String, status: String, message: String, success: Boolean,
                         taskResults: Map[String, String], when: String) extends SchedulerEvent {
  def aggregateTag: AggregateEventTag[SchedulerEvent] = TaskEvent.Tag
}

object CompletedTask {
  implicit val format: Format[CompletedTask] = Json.format
}

/**
  * Commands
  */
trait SchedulerCommandSerializable

sealed trait SchedulerCommand extends SchedulerCommandSerializable

case class InitializeScheduler(id: String,
                               timeWindowDuration: String,
                               maxOneTimeTaskExecutions: Int,
                               maxRecurrentTaskExecutions: Int,
                               parameters: Map[String, String],
                               inboundChannelId: Option[String],
                               outboundChannelId: Option[String],
                               replyTo: ActorRef[Confirmation]) extends SchedulerCommand

case class ScheduleTask(taskId: String,
                        externalId: Option[String],
                        taskType: String,
                        schedulePolicy: String,
                        parameters: Map[String, String],
                        replyTo: ActorRef[Confirmation]) extends SchedulerCommand

case class UpdateScheduler(status: Option[String],
                           maxOneTimeTaskExecutions: Option[Int],
                           maxRecurrentTaskExecutions: Option[Int],
                           replyTo: ActorRef[Confirmation]) extends SchedulerCommand

case class AssignTask(externalId: Option[String],
                      replyTo: ActorRef[Confirmation]) extends SchedulerCommand

case class CompleteTask(schedulerId: UUID,
                        taskId: UUID,
                        when: LocalDateTime,
                        result: TaskResult,
                        replyTo: ActorRef[Confirmation]) extends SchedulerCommand

case class GetSchedulerInfo(replyTo: ActorRef[Confirmation]) extends SchedulerCommand

case class GetTaskInfo(taskId: String,
                       replyTo: ActorRef[Confirmation]) extends SchedulerCommand


/**
  * Replies
  */
sealed trait Confirmation

case object Confirmation {
  implicit val format: Format[Confirmation] = new Format[Confirmation] {
    override def reads(json: JsValue): JsResult[Confirmation] = {
      if ((json \ "reason").isDefined)
        Json.fromJson[Rejected](json)
      else
        Json.fromJson[Accepted](json)
    }

    override def writes(o: Confirmation): JsValue = {
      o match {
        case acc: Accepted => Json.toJson(acc)
        case schedulerInfoSummary: SchedulerInfoSummary => Json.toJson(schedulerInfoSummary)
        case taskInfoSummary: TaskInfoSummary => Json.toJson(taskInfoSummary)
        case schedulerId: SchedulerId => Json.toJson(schedulerId)
        case taskId: TaskId => Json.toJson(taskId)
        case rej: Rejected => Json.toJson(rej)
      }
    }
  }
}

sealed trait Accepted extends Confirmation

case object Accepted extends Accepted {
  implicit val format: Format[Accepted] =
    Format(Reads(_ => JsSuccess(Accepted)), Writes(_ => Json.obj()))
}

case class Rejected(reason: String) extends Confirmation

object Rejected {
  implicit val format: Format[Rejected] = Json.format
}


case class SchedulerInfoSummary(schedulerId: UUID,
                                inboundChannelId: Option[UUID],
                                outboundChannelId: Option[UUID],
                                created: String,
                                status: String,
                                timestamp: String,
                                maxOneTimeTaskExecutions: Int,
                                maxRecurrentTaskExecutions: Int,
                                interval: String,
                                parameters: Map[String, String],
                                version: Long) extends Confirmation

object SchedulerInfoSummary {
  implicit val format: Format[SchedulerInfoSummary] = Json.format
}

case class TaskInfoSummary(id: String, externalId: Option[String], created: LocalDateTime,
                           timestamp: LocalDateTime, taskStatus: String, taskType: String, schedulePolicy: String,
                           schedulerId: String, executionCount: Long, parameters: Map[String, String]) extends Confirmation


object TaskInfoSummary {
  implicit val format: Format[TaskInfoSummary] = Json.format
}

case class TaskInfoResult(success: Boolean, message: String, values: Map[String, String])

object TaskInfoResult {
  implicit val format: Format[TaskInfoResult] = Json.format
}

case class TaskId(value: String) extends Confirmation

object TaskId {
  implicit val format: Format[TaskId] = Json.format
}

case class SchedulerId(value: String) extends Confirmation

object SchedulerId {
  implicit val format: Format[SchedulerId] = Json.format
}

/**
  * Akka serialization, used by both persistence and remoting, needs to have
  * serializers registered for every type serialized or deserialized. While it's
  * possible to use any serializer you want for Akka messages, out of the box
  * Lagom provides support for JSON, via this registry abstraction.
  *
  * The serializers are registered here, and then provided to Lagom in the
  * application loader.
  */
object SchedulerSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[SchedulerState],
    JsonSerializer[SchedulerInitialized],
    JsonSerializer[SchedulerUpdated],
    JsonSerializer[SchedulerInfoSummary],
    JsonSerializer[TimeWindowChanged],
    JsonSerializer[TaskScheduled],
    JsonSerializer[TaskAssigned],
    JsonSerializer[CompletedTask],
    JsonSerializer[Confirmation],
    JsonSerializer[Accepted],
    JsonSerializer[Rejected]
  )
}
