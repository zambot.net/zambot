package net.zambot.scheduler.impl

import java.time.{LocalDateTime, ZoneId}
import java.util.{Date, UUID}

import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, EntityRef}
import akka.persistence.query.Offset
import akka.stream.scaladsl.Sink
import akka.util.Timeout
import akka.{Done, NotUsed}
import com.datastax.driver.core.SimpleStatement
import com.datastax.driver.core.utils.UUIDs
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.transport.{BadRequest, NotFound}
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry, ReadSide}
import com.softwaremill.macwire.wire
import net.zambot.scheduler.api
import net.zambot.scheduler.api.{SchedulerService, TaskEvent}
import net.zambot.scheduler.model.{TaskResult, _}
import org.slf4j.LoggerFactory

import scala.annotation.tailrec
import scala.collection.immutable.ListMap
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.jdk.CollectionConverters._

/**
  * Implementation of the SchedulerService.
  */
class SchedulerServiceImpl(
                            clusterSharding: ClusterSharding,
                            persistentEntityRegistry: PersistentEntityRegistry,
                            cassandraSession: CassandraSession, cassandraReadSide: CassandraReadSide,
                            readSide: ReadSide)(implicit ec: ExecutionContext,
                                                materializer: akka.stream.Materializer) extends SchedulerService {

  private val log = LoggerFactory.getLogger("SchedulerServiceImpl")

  readSide.register[SchedulerEvent](wire[SchedulerEventProcessor])
  readSide.register[SchedulerEvent](wire[TaskEventProcessor])
  readSide.register[SchedulerEvent](wire[TaskResultEventProcessor])

  /**
    * Looks up the entity for the given ID.
    */
  private def entityRef(id: String): EntityRef[SchedulerCommand] =
    clusterSharding.entityRefFor(SchedulerState.typeKey, id)

  implicit val timeout: Timeout = Timeout(5.seconds)

  /**
    * Assigns a task to a runner
    * Assigns a task to a runner
    *
    * @param schedulerId The Scheduler identifier
    * @return TaskInfo Body Parameter  Assigns a task to a task runner
    */

  override def assignTask(schedulerId: UUID): ServiceCall[TaskRunner, AssignTaskToRunnerResult] = ServiceCall { taskRunner =>
    entityRef(schedulerId.toString).ask[Confirmation](replyTo =>
      GetSchedulerInfo(
        replyTo = replyTo)
    ).map {
      case schedulerInfo: SchedulerInfoSummary =>
        entityRef(schedulerId.toString).ask[Confirmation](
          replyTo => AssignTask(Some(taskRunner.externalId.toString), replyTo)
        ).map {
          case assignedTaskSummary: net.zambot.scheduler.impl.TaskInfoSummary =>
            AssignTaskToRunnerResult("true", "ok", Some(TaskInfo(
              taskId = java.util.UUID.fromString(assignedTaskSummary.id),
              externalId = if (assignedTaskSummary.externalId.isEmpty) None: Option[UUID] else Some(UUID.fromString(assignedTaskSummary.externalId.get)),
              inboundChannelId = schedulerInfo.inboundChannelId,
              outboundChannelId = schedulerInfo.outboundChannelId,
              created = LocalDateTime.parse(assignedTaskSummary.created.toString),
              timestamp = LocalDateTime.parse(assignedTaskSummary.timestamp.toString),
              status = assignedTaskSummary.taskStatus,
              taskType = assignedTaskSummary.taskType,
              schedulePolicy = assignedTaskSummary.schedulePolicy,
              parameters = TaskParameters(values = assignedTaskSummary.parameters),
              executionCount = assignedTaskSummary.executionCount,
              schedulerId = java.util.UUID.fromString(assignedTaskSummary.schedulerId)
            )))
          case Rejected(reason) => AssignTaskToRunnerResult("false", s"${reason}", None)
          case _ => AssignTaskToRunnerResult("false", "Bad request", None)
        }
      case _ => throw NotFound(s"Scheduler ${schedulerId.toString} was not found.")
    }.flatMap(result => result)

  }

  /**
    * Creates a scheduler
    * Creates a scheduler
    *
    * @return EntityId Body Parameter  Creates a scheduler
    */
  override def createScheduler(): ServiceCall[CreateScheduler, EntityId] = { request =>
    val newSchedulerId = java.util.UUID.randomUUID()
    entityRef(newSchedulerId.toString)
      .ask[Confirmation](replyTo =>
        InitializeScheduler(
          id = newSchedulerId.toString,
          parameters = request.parameters.values,
          timeWindowDuration = request.interval,
          maxOneTimeTaskExecutions = request.maxOneTimeTaskExecutions,
          maxRecurrentTaskExecutions = request.maxRecurrentTaskExecutions,
          inboundChannelId = request.inboundChannelId.fold(None: Option[String])(id => Some(id.toString)),
          outboundChannelId = request.outboundChannelId.fold(None: Option[String])(id => Some(id.toString)),
          replyTo = replyTo)
      ).map {
      case SchedulerId(id) => EntityId(java.util.UUID.fromString(id))
      case Rejected(reason) => throw BadRequest(reason)
      case _ => throw BadRequest("Invalid request.")
    }
  }

  /**
    * Retrieves Scheduler Information
    *
    * @param schedulerId Scheduler Identifier
    * @return SchedulerInfo
    */
  override def getSchedulerInfo(schedulerId: UUID): ServiceCall[NotUsed, SchedulerInfo] = { _ =>
    entityRef(schedulerId.toString).ask[Confirmation](replyTo =>
      GetSchedulerInfo(
        replyTo = replyTo)
    ).map {
      case schedulerInfo: SchedulerInfoSummary => SchedulerInfo(
        schedulerId = schedulerId,
        inboundChannelId = schedulerInfo.inboundChannelId,
        outboundChannelId = schedulerInfo.outboundChannelId,
        created = LocalDateTime.parse(schedulerInfo.created),
        status = schedulerInfo.status,
        timestamp = LocalDateTime.parse(schedulerInfo.timestamp),
        maxOneTimeTaskExecutions = schedulerInfo.maxOneTimeTaskExecutions,
        maxRecurrentTaskExecutions = schedulerInfo.maxRecurrentTaskExecutions,
        interval = schedulerInfo.interval,
        version = schedulerInfo.version,
        parameters = SchedulerParameters(values = schedulerInfo.parameters)
      )
      case _ => throw NotFound(s"Scheduler ${schedulerId.toString} was not found.")
    }
  }

  /**
    * Schedules a task
    * Schedules a task to be managed by the scheduler identified by {schedulerId}
    *
    * @param schedulerId The Scheduler identifier
    * @return EntityId Body Parameter  Schedules a task to be managed by the scheduler identified by {schedulerId}
    */
  override def createTask(schedulerId: UUID): ServiceCall[CreateTask, EntityId] = { request =>
    val newTaskId = java.util.UUID.randomUUID()
    val ref = entityRef(schedulerId.toString)
    ref.ask[Confirmation](replyTo => ScheduleTask(
      taskId = newTaskId.toString, externalId = request.externalId.map(id => id.toString),
      taskType = request.taskType, schedulePolicy = request.schedulePolicy, parameters = request.parameters.values, replyTo = replyTo)
    ).map {
      case TaskId(id) => EntityId(java.util.UUID.fromString(id))
      case Rejected(reason) => throw BadRequest(reason)
      case _ => throw BadRequest("Invalid Request")
    }
  }

  /**
    * List Schedulers
    * List Markets
    *
    * @param nextPageToken     (optional)
    * @param previousPageToken (optional)
    * @param pageSize          (optional)
    * @return PaginatedSchedulerInfo
    */
  override def findScheduler(nextPageToken: Option[UUID],
                             previousPageToken: Option[UUID],
                             pageSize: Option[Int],
                             inboundChannelId: Option[UUID],
                             outboundChannelId: Option[UUID]): ServiceCall[NotUsed, SchedulerInfoList] = { _ =>


    def generateWhereClause(pageSize: Int, in: Option[UUID], out: Option[UUID], previousPageToken: Option[UUID], nextPageToken: Option[UUID]): String = {

      @tailrec
      def whereClause(item: Option[String], list: List[Option[String]], counter: Int, currentClause: String = ""): String = {
        item match {
          case None => if (list.isEmpty) currentClause else whereClause(list.head, list.tail, counter, currentClause)
          case Some(s) => if (counter == 0)
            if (list.isEmpty) "where " + s else whereClause(list.head, list.tail, counter + 1, "where " + s)
          else if (list.isEmpty) currentClause + " and " + s
          else whereClause(list.head, list.tail, counter + 1, currentClause + " and " + s)

        }
      }

      val tokenFilter: Option[String] = previousPageToken.fold(
        nextPageToken.fold(None: Option[String])(_ => Some("token(scheduler_id) > token(?)"))
      )(_ => Some("token(scheduler_id) < token(?)"))

      val filterList: List[Option[String]] = List(
        tokenFilter,
        if (in.isEmpty) None: Option[String] else Some("inbound_channel_id = ?"),
        if (out.isEmpty) None: Option[String] else Some("outbound_channel_id = ?")
      )
      if (filterList.isEmpty) "" else whereClause(filterList.head, filterList.tail, 0)

    }

    def generateParams(pageSize: Int, previousPageToken: Option[UUID], nextPageToken: Option[UUID]): Array[Object] = {
      val pageToken: Seq[Object] = previousPageToken.fold(nextPageToken.fold(Nil: Seq[UUID])(nextPageToken => Seq(nextPageToken)))(previousPageToken => Seq(previousPageToken))
      val ps: Seq[Object] = Seq(pageSize.asInstanceOf[Object])
      val in: Seq[Object] = inboundChannelId.fold(Nil: Seq[Object])(id => Seq(id.asInstanceOf[Object]))
      val out: Seq[Object] = outboundChannelId.fold(Nil: Seq[Object])(id => Seq(id.asInstanceOf[Object]))
      val params: Seq[Object] = pageToken ++ in ++ out ++ ps

      params.toArray

    }

    val ps = pageSize.getOrElse(10)
    val whereClause = generateWhereClause(ps, inboundChannelId, outboundChannelId, previousPageToken, nextPageToken)
    val params = generateParams(ps, previousPageToken, nextPageToken)

    val statement = new SimpleStatement(
      "SELECT scheduler_id, inbound_channel_id, outbound_channel_id, created, interval, " +
        "max_onetime_task_executions, max_recurrent_task_executions, status,updated, version, parameters" +
        s" FROM scheduler.scheduler_list ${whereClause} " +
        s"LIMIT ? ALLOW FILTERING", params: _*)
    statement.setFetchSize(ps)

    val source = cassandraSession.select(statement).map(row =>
      SchedulerInfo(
        schedulerId = row.getUUID("scheduler_id"),
        created = java.time.LocalDateTime.ofInstant(row.getTimestamp("created").toInstant, ZoneId.systemDefault()),
        timestamp = java.time.LocalDateTime.ofInstant(row.getTimestamp("updated").toInstant, ZoneId.systemDefault()),
        parameters = SchedulerParameters(
          values = row.getMap("parameters", classOf[String], classOf[String]).asScala.toMap),
        interval = row.getString("interval"),
        maxOneTimeTaskExecutions = row.getInt("max_onetime_task_executions"),
        maxRecurrentTaskExecutions = row.getInt("max_recurrent_task_executions"),
        status = row.getString("status"),
        version = row.getLong("version"),
        inboundChannelId = if (row.isNull("inbound_channel_id")) None
        else Some(row.getUUID("inbound_channel_id")),
        outboundChannelId = if (row.isNull("outbound_channel_id")) None
        else Some(row.getUUID("outbound_channel_id"))

      )
    )

    source.runWith(Sink.fold[Seq[SchedulerInfo], SchedulerInfo](Seq())((seq, elem) => seq :+ elem))
      .map(e => SchedulerInfoList(e))
  }

  /**
    * Lists tasks for a scheduler
    *
    * @param schedulerId       The Scheduler identifier
    * @param nextPageToken     (optional)
    * @param previousPageToken (optional)
    * @param pageSize          (optional)
    * @return PaginatedTaskInfo
    */
  override def findTask(schedulerId: UUID,
                        nextPageToken: Option[UUID],
                        previousPageToken: Option[UUID],
                        pageSize: Option[Int],
                        taskType: Option[String],
                        taskStatus: Option[String],
                        externalId: Option[UUID]): ServiceCall[NotUsed, TaskInfoSummaryList] = { _ =>

    def generateWhereClause(previousPageToken: Option[UUID], nextPageToken: Option[UUID],
                            taskType: Option[String], taskStatus: Option[String], externalId: Option[UUID]): String = {

      @tailrec
      def whereClause(item: Option[String], list: List[Option[String]], counter: Int, currentClause: String = ""): String = {
        item match {
          case None => if (list.isEmpty) currentClause else whereClause(list.head, list.tail, counter, currentClause)
          case Some(s) => if (counter == 0)
            if (list.isEmpty) "where " + s else whereClause(list.head, list.tail, counter + 1, "where " + s)
          else if (list.isEmpty) currentClause + " and " + s
          else whereClause(list.head, list.tail, counter + 1, currentClause + " and " + s)

        }
      }

      val tokenFilter: Option[String] = previousPageToken.fold(
        nextPageToken.fold(None: Option[String])(_ => Some("token(task_id) > token(?)"))
      )(_ => Some("token(scheduler_id) < token(?)"))

      val schedulerIdFilter = Some("scheduler_id = ?")
      val taskTypeFilter: Option[String] = taskType.fold(None: Option[String])(s => Some("task_type = ?"))
      val taskStatusFilter: Option[String] = taskStatus.fold(None: Option[String])(s => Some("status = ?"))
      val externalIdFilter: Option[String] = externalId.fold(None: Option[String])(s => Some("external_id = ?"))

      val filterList = List(tokenFilter, schedulerIdFilter, taskTypeFilter, taskStatusFilter, externalIdFilter)
      if (filterList.isEmpty) "" else whereClause(filterList.head, filterList.tail, 0)

    }

    def generateParams(pageSize: Int, previousPageToken: Option[UUID], nextPageToken: Option[UUID],
                       taskType: Option[String], taskStatus: Option[String], externalId:Option[UUID]): Array[Object] = {

      val token = previousPageToken.fold(nextPageToken.fold(None: Option[UUID])(nextPageToken =>
        Some(nextPageToken)))(previousPageToken => Some(previousPageToken))

      val parameters: Seq[Object] = Nil ++ (token match {
        case Some(o) => Seq(o.asInstanceOf[Object]);
        case _ => Nil
      }) ++
        Seq(schedulerId.asInstanceOf[Object]) ++
        (taskType match {
          case Some(o) => Seq(o.asInstanceOf[Object]);
          case _ => Nil
        }) ++
        (taskStatus match {
          case Some(o) => Seq(o.asInstanceOf[Object]);
          case _ => Nil
        }) ++
        (externalId match {
          case Some(o) => Seq(o.asInstanceOf[Object]);
          case _ => Nil
        }) ++
        Seq(pageSize.asInstanceOf[Object])

      parameters.toArray

    }

    val ps = pageSize.getOrElse(10)
    val params = generateParams(ps, previousPageToken, nextPageToken, taskType, taskStatus, externalId)
    val whereClause = generateWhereClause(previousPageToken, nextPageToken, taskType, taskStatus, externalId)

    val statement = new SimpleStatement(
      s"SELECT scheduler_id, task_id, updated, created, external_id, parameters, schedule_policy," +
        s" status,task_type FROM scheduler.scheduler_task_list ${whereClause} LIMIT ? ALLOW FILTERING",
      params: _*)
    statement.setFetchSize(ps)

    val source = cassandraSession.select(statement).map(row =>
      TaskInfoSummary(
        taskId = row.getUUID("task_id"),
        externalId = if (row.isNull("external_id")) None else Some(row.getUUID("external_id")),
        schedulerId = row.getUUID("scheduler_id"),
        created = java.time.LocalDateTime.ofInstant(row.getTimestamp("created").toInstant, ZoneId.systemDefault()),
        timestamp = java.time.LocalDateTime.ofInstant(row.getTimestamp("updated").toInstant, ZoneId.systemDefault()),
        status = row.getString("status"),
        taskType = row.getString("task_type"),
        schedulePolicy = row.getString("schedule_policy"),
        parameters = TaskParameters(
          values = row.getMap("parameters", classOf[String], classOf[String]).asScala.toMap
        )
      )
    )

    source.runWith(Sink.fold[Seq[TaskInfoSummary], TaskInfoSummary](Seq())((seq, elem) => seq :+ elem))
      .map(e => TaskInfoSummaryList(e))

  }

  /**
    * Lists results for a task
    *
    * @param schedulerId The Scheduler identifier
    * @param taskId      The Task identifier
    * @param pageSize    (optional)
    * @return PaginatedTaskInfo
    */
  def findTaskResult(schedulerId: UUID, taskId: UUID, pageSize: Option[Int] = None, startDate: Option[String],
                     endDate: Option[String], success: Option[Boolean]): ServiceCall[NotUsed, TaskResultList] = { _ =>

    val ps = pageSize.getOrElse(10)

    val after: Option[LocalDateTime] = startDate.fold(None: Option[LocalDateTime])(s => Some(java.time.LocalDateTime.parse(s)))
    val before: Option[LocalDateTime] = endDate.fold(None: Option[LocalDateTime])(s => Some(java.time.LocalDateTime.parse(s)))

    def generateWhereClause(after: Option[LocalDateTime], before: Option[LocalDateTime],
                            taskStatus: Option[Boolean]): String = {

      @tailrec
      def whereClause(item: Option[String], list: List[Option[String]], counter: Int, currentClause: String = ""): String = {
        item match {
          case None => if (list.isEmpty) currentClause else whereClause(list.head, list.tail, counter, currentClause)
          case Some(s) => if (counter == 0)
            if (list.isEmpty) "where " + s else whereClause(list.head, list.tail, counter + 1, "where " + s)
          else if (list.isEmpty) currentClause + " and " + s
          else whereClause(list.head, list.tail, counter + 1, currentClause + " and " + s)

        }
      }

      val dateFilter: Option[String] = after.fold(
        before.fold(None: Option[String])(_ => Some("task_result_id < ?"))
      )(_ => Some("task_result_id > ?"))

      val taskStatusFilter: Option[String] = taskStatus.fold(None: Option[String])(s => Some("success = ?"))

      val filterList = List(Some("TOKEN(scheduler_id, task_id) = TOKEN(?,?)"), dateFilter, taskStatusFilter)
      if (filterList.isEmpty) "" else whereClause(filterList.head, filterList.tail, 0)

    }

    def generateParams(after: Option[LocalDateTime], before: Option[LocalDateTime],
                       taskStatus: Option[Boolean]): Array[Object] = {

      val date: Option[UUID] = after.fold(before.fold(None: Option[UUID])(after =>
        Some(UUIDs.startOf(Date.from(after.atZone(ZoneId.systemDefault()).toInstant).getTime))))(before => Some(UUIDs.startOf(Date.from(before.atZone(ZoneId.systemDefault()).toInstant).getTime)))

      val parameters: Seq[Object] = Seq(schedulerId.asInstanceOf[Object], taskId.asInstanceOf[Object]) ++ (date match {
        case Some(o) => Seq(o.asInstanceOf[Object]);
        case _ => Nil
      }) ++
        (taskStatus match {
          case Some(o) => Seq(o.asInstanceOf[Object]);
          case _ => Nil
        }) ++
        Seq(ps.asInstanceOf[Object])

      parameters.toArray

    }

    val statement = new SimpleStatement(
      s"SELECT scheduler_id, task_id, task_result_id, external_id, message, success, task_type, " +
        s"values, toTimestamp(task_result_id) as task_result_ts FROM scheduler.scheduler_task_result " +
        generateWhereClause(after, before, success) + " LIMIT ? ALLOW FILTERING", generateParams(after, before, success): _*)

    statement.setFetchSize(ps)

    val source = cassandraSession.select(statement).map(row => TaskResult(
      schedulerId = row.getUUID("scheduler_id"),
      taskId = row.getUUID("task_id"),
      externalId = Some(row.getUUID("external_id")),
      success = row.getBool("success"),
      message = row.getString("message"),
      timestamp = java.time.LocalDateTime.ofInstant(row.getTimestamp("task_result_ts").toInstant, ZoneId.systemDefault()),
      values = row.getMap("values", classOf[String], classOf[String]).asScala.toMap

    ))

    source.runWith(Sink.fold[Seq[TaskResult], TaskResult](Seq())((seq, elem) => seq :+ elem))
      .map(e => TaskResultList(e))

  }

  /**
    * Retrieves task information
    *
    * @param schedulerId The scheduler identifier
    * @param taskId      The task identifier
    * @return TaskInfo
    */
  override def getTaskInfo(schedulerId: UUID, taskId: UUID): ServiceCall[NotUsed, TaskInfo] = { _ =>
    entityRef(schedulerId.toString).ask[Confirmation](replyTo =>
      GetSchedulerInfo(
        replyTo = replyTo)
    ).map {
      case schedulerInfo: SchedulerInfoSummary =>
        entityRef(schedulerId.toString)
          .ask[Confirmation](replyTo => GetTaskInfo(
            taskId = taskId.toString,
            replyTo = replyTo)
          ).map {
          case taskInfoSummary: net.zambot.scheduler.impl.TaskInfoSummary => TaskInfo(
            taskId = taskId,
            schedulerId = schedulerInfo.schedulerId,
            externalId = if (taskInfoSummary.externalId.isEmpty) None: Option[UUID] else Some(UUID.fromString(taskInfoSummary.externalId.get)),
            inboundChannelId = schedulerInfo.inboundChannelId,
            outboundChannelId = schedulerInfo.outboundChannelId,
            created = taskInfoSummary.created,
            timestamp = taskInfoSummary.timestamp,
            status = taskInfoSummary.taskStatus,
            taskType = taskInfoSummary.taskType,
            schedulePolicy = taskInfoSummary.schedulePolicy,
            executionCount = taskInfoSummary.executionCount,
            parameters = TaskParameters(taskInfoSummary.parameters))
          case _ => throw NotFound(s"Task ${taskId.toString} was not found on scheduler ${schedulerId.toString}.")
        }
      case _ => throw NotFound(s"Scheduler ${schedulerId.toString} was not found.")
    }.flatMap(x => x)

  }

  /**
    * Updates a scheduler
    * Updates a scheduler
    *
    * @param schedulerId Scheduler identifier
    * @return void Body Parameter  Updates a given scheduler
    */
  override def updateScheduler(schedulerId: UUID): ServiceCall[UpdateSchedulerInfo, Done] = {
    request =>
      val ref = entityRef(schedulerId.toString)
      ref.ask[Confirmation](replyTo => UpdateScheduler(
        status = request.status,
        maxRecurrentTaskExecutions = request.maxRecurrentTaskExecutions,
        maxOneTimeTaskExecutions = request.maxOneTimeTaskExecutions,
        replyTo = replyTo)
      ).map {
        case Accepted => Done
        case Rejected(reason) => throw BadRequest(reason)
        case _ => throw BadRequest("Invalid request")
      }
  }

  /**
    * Completes a task
    * Completes a task
    *
    * @param schedulerId The scheduler identifier
    * @param taskId      The task identifier
    * @return void Body Parameter
    */
  override def completeTask(schedulerId: UUID, taskId: UUID): ServiceCall[CompleteTaskInfo, CompletedTaskInfo] = { request =>
    val ref = entityRef(schedulerId.toString)
    ref.ask[Confirmation](replyTo => CompleteTask(
      schedulerId = schedulerId,
      taskId = taskId,
      result = net.zambot.scheduler.impl.TaskResult(
        success = request.result.success,
        message = request.result.message,
        values = request.result.values),
      when = request.result.when,
      replyTo = replyTo)
    ).map {
      case taskInfo: net.zambot.scheduler.impl.TaskInfoSummary => {
        CompletedTaskInfo(
          id = taskInfo.id.toString,
          inboundChannelId = None,
          outboundChannelId = None,
          created = taskInfo.created,
          timestamp = taskInfo.timestamp,
          taskStatus = taskInfo.taskStatus,
          taskType = taskInfo.taskType,
          schedulePolicy = taskInfo.schedulePolicy,
          schedulerId = taskInfo.schedulerId.toString,
          executionCount = taskInfo.executionCount,
          parameters = taskInfo.parameters
        )
      }
      case Rejected(reason) => throw BadRequest(reason)
      case _ => throw BadRequest("Bad request")
    }
  }

  override def schedulerEventsTopic(): Topic[api.SchedulerEvent] = TopicProducer.singleStreamWithOffset {
    fromOffset =>
      persistentEntityRegistry.eventStream(SchedulerEvent.Tag, fromOffset)
        .mapConcat(filterSchedulerEvents)
  }

  private def filterSchedulerEvents(ev: EventStreamElement[SchedulerEvent]):
  scala.collection.immutable.Iterable[(api.SchedulerEvent, Offset)] = ev match {
    case ev@EventStreamElement(_, e: SchedulerInitialized, offset) => scala.collection.immutable.Iterable((convertSchedulerInitializedEventToSchedulerEvent(e), offset))
    case ev@EventStreamElement(_, e: SchedulerUpdated, offset) => scala.collection.immutable.Iterable((convertSchedulerUpdatedEventToSchedulerEvent(e), offset))
    case _ => Nil
  }

  private def convertSchedulerInitializedEventToSchedulerEvent(e: SchedulerInitialized): api.SchedulerEvent = api.SchedulerEvent(
    schedulerId = e.id,
    created = e.created,
    status = e.status,
    timestamp = e.timestamp,
    maxOneTimeTaskExecutions = e.maxRecurrentTaskExecutions,
    maxRecurrentTaskExecutions = e.maxRecurrentTaskExecutions,
    interval = e.timeWindowDuration,
    version = e.version)

  private def convertSchedulerUpdatedEventToSchedulerEvent(e: SchedulerUpdated): api.SchedulerEvent = api.SchedulerEvent(
    schedulerId = e.schedulerId.toString,
    created = e.created,
    status = e.status,
    timestamp = e.timestamp,
    maxOneTimeTaskExecutions = e.maxRecurrentTaskExecutions,
    maxRecurrentTaskExecutions = e.maxRecurrentTaskExecutions,
    interval = e.timeWindowDuration,
    version = e.version)

  override def taskEventsTopic(): Topic[api.TaskEvent] = TopicProducer.singleStreamWithOffset {
    fromOffset =>
      persistentEntityRegistry.eventStream(net.zambot.scheduler.impl.TaskEvent.Tag, fromOffset)
        .mapConcat(filterTaskEvents)
  }

  private def filterTaskEvents(ev: EventStreamElement[SchedulerEvent]): scala.collection.immutable.Iterable[(api.TaskEvent, Offset)] = ev match {
    case ev@EventStreamElement(_, e: TaskScheduled, offset) => scala.collection.immutable.Iterable((convertTaskScheduledEventToTaskEvent(e), offset))
    case ev@EventStreamElement(_, e: TaskAssigned, offset) => scala.collection.immutable.Iterable((convertTaskAssignedEventToTaskEvent(e), offset))
    case ev@EventStreamElement(_, e: CompletedTask, offset) => scala.collection.immutable.Iterable((convertTaskCompletedEventToTaskEvent(e), offset))
    case _ => Nil
  }

  private def convertTaskAssignedEventToTaskEvent(e: TaskAssigned): api.TaskEvent = api.TaskEvent(
    taskId = java.util.UUID.fromString(e.taskId),
    created = java.time.LocalDateTime.parse(e.created),
    externalId = e.externalId.fold(None: Option[UUID])(e => Some(java.util.UUID.fromString(e))),
    parameters = e.parameters,
    result = ListMap.empty,
    message = None,
    success = None,
    scheduler_policy = e.schedulePolicy,
    status = e.status,
    timestamp = java.time.LocalDateTime.parse(e.updated)
  )

  private def convertTaskScheduledEventToTaskEvent(e: TaskScheduled): api.TaskEvent = api.TaskEvent(
    taskId = java.util.UUID.fromString(e.taskId),
    created = java.time.LocalDateTime.parse(e.created),
    externalId = e.externalId match {
      case Some(s) => Some(java.util.UUID.fromString(s))
      case None => None: Option[UUID]
    },
    parameters = e.parameters,
    result = ListMap.empty,
    message = None,
    success = None,
    scheduler_policy = e.schedulePolicy,
    status = e.status,
    timestamp = java.time.LocalDateTime.parse(e.timestamp)
  )

  private def convertTaskCompletedEventToTaskEvent(e: CompletedTask): api.TaskEvent = api.TaskEvent(
    taskId = java.util.UUID.fromString(e.taskId),
    created = java.time.LocalDateTime.parse(e.created),
    externalId = e.externalId.fold(None: Option[UUID])(e => Some(java.util.UUID.fromString(e))),
    parameters = e.parameters,
    result = e.taskResults,
    scheduler_policy = e.schedulePolicy,
    message = Some(e.message),
    status = e.status,
    success = Some(e.success),
    timestamp = java.time.LocalDateTime.parse(e.updated)
  )


  override def completedTaskEventsTopic(): Topic[api.CompletedTaskEvent] = TopicProducer.singleStreamWithOffset {
    fromOffset =>
      persistentEntityRegistry.eventStream(net.zambot.scheduler.impl.TaskEvent.Tag, fromOffset)
        .mapConcat(filterCompletedTaskEvents)
  }

  private def filterCompletedTaskEvents(ev: EventStreamElement[SchedulerEvent]): scala.collection.immutable.Iterable[(api.CompletedTaskEvent, Offset)] = ev match {
    case ev@EventStreamElement(_, e: CompletedTask, offset) => scala.collection.immutable.Iterable((convertTaskCompletedEventToCompletedTaskEvent(e), offset))
    case _ => Nil
  }

  private def convertTaskCompletedEventToCompletedTaskEvent(e: CompletedTask): api.CompletedTaskEvent = api.CompletedTaskEvent(
    schedulerId = java.util.UUID.fromString(e.schedulerId),
    taskId = java.util.UUID.fromString(e.taskId),
    created = java.time.LocalDateTime.parse(e.created),
    externalId = e.externalId.fold(None: Option[UUID])(e => Some(java.util.UUID.fromString(e))),
    result = e.taskResults,
    message = e.message,
    success = e.success,
    timestamp = java.time.LocalDateTime.parse(e.updated),
    completedTimestamp = java.time.LocalDateTime.parse(e.when)
  )

}