package net.zambot.scheduler.impl

import java.time.{LocalDateTime, ZoneId}
import java.util.Date

import akka.Done
import com.datastax.driver.core.utils.UUIDs
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, EventStreamElement, ReadSideProcessor}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.jdk.CollectionConverters._


class TaskResultEventProcessor(session: CassandraSession, readSide: CassandraReadSide)
                              (implicit ec: ExecutionContext) extends ReadSideProcessor[SchedulerEvent] {

  override def aggregateTags: Set[AggregateEventTag[SchedulerEvent]] = Set(TaskEvent.Tag)

  override def buildHandler(): ReadSideProcessor.ReadSideHandler[SchedulerEvent] = {
    readSide.builder[SchedulerEvent]("eventProcessor-scheduler_task_result_events")
      .setGlobalPrepare(() => globalPrepare)
      .setEventHandler[TaskScheduled](processTaskEvent)
      .setEventHandler[TaskAssigned](processTaskEvent)
      .setEventHandler[CompletedTask](processTaskEvent)
      .setPrepare(ev => prepareTaskSession)
      .build

  }

  private def globalPrepare: Future[Done] = {
    //    createTaskType.flatMap(_ => createTaskEventTable)
    createTaskEventTable
  }

  private def createTaskEventTable: Future[Done] = {
    session.executeCreateTable("CREATE TABLE IF NOT EXISTS scheduler.scheduler_task_result (scheduler_id uuid, " +
      "task_id uuid, task_result_id timeuuid, external_id uuid, values frozen<map<text,text>>, message text," +
      " success boolean, task_type text, exchange text, PRIMARY KEY ((scheduler_id, task_id), task_result_id))")

  }

  private val writeTaskPromise = Promise[PreparedStatement] // initialized in prepare
  private def writeTaskEvent: Future[PreparedStatement] = writeTaskPromise.future

  private def prepareTaskSession: Future[Done] = {
    val f = session.prepare("INSERT INTO scheduler.scheduler_task_result (scheduler_id, task_id, " +
      "task_result_id, external_id, values, message, success, task_type, exchange) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)")

    writeTaskPromise.completeWith(f)
    f.map(_ => Done)

  }

  private def processTaskEvent(ev: EventStreamElement[SchedulerEvent]): Future[List[BoundStatement]] = {

    def createTaskBind(ps: PreparedStatement, schedulerId:String, taskId:String, when:String,
                       externalId:Option[String], values:Map[String, String], message:String, success:Boolean,
                       taskType: String, exchange: String): BoundStatement = {

      val bind = ps.bind()
        .setUUID("scheduler_id", java.util.UUID.fromString(schedulerId))
        .setUUID("task_id", java.util.UUID.fromString(taskId))
        .setUUID("task_result_id", UUIDs.startOf(Date.from(LocalDateTime.parse(when).atZone(ZoneId.systemDefault()).toInstant).getTime))
        .setMap("values", values.asJava)
        .setString("message", message)
        .setBool("success", success)
        .setString("task_type", taskType)
        .setString("exchange", exchange)

      externalId match {
        case Some(s) => bind.setUUID("external_id", java.util.UUID.fromString(s))
        case None => bind.setToNull("external_id")

      }
      bind

    }

    ev.event match {
      case taskCompletedEvent: CompletedTask => {
        writeTaskEvent.map { ps =>
          List(createTaskBind(
            ps = ps,
            schedulerId = taskCompletedEvent.schedulerId,
            taskId = taskCompletedEvent.taskId,
            when = taskCompletedEvent.when,
            externalId = taskCompletedEvent.externalId,
            values = taskCompletedEvent.taskResults,
            message = taskCompletedEvent.message,
            success = taskCompletedEvent.success,
            taskType = taskCompletedEvent.taskType,
            exchange = taskCompletedEvent.taskType)
          )
        }
      }
      case _ => Future {Nil}

    }

  }

}