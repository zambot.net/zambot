package net.zambot.scheduler.batch

import java.nio.file.Paths
import java.nio.file.StandardOpenOption._
import java.time.LocalDateTime
import java.util.UUID

import akka.actor.ActorSystem
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{FileIO, Keep, Sink, Source}
import akka.util.ByteString
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

object Export extends App {

  private val log = LoggerFactory.getLogger(Export.getClass.getCanonicalName)

  implicit val actorSystem = ActorSystem()
  implicit val ec = actorSystem.dispatcher
  log.info("Zambot Scheduler Exporter.Exports task_results from a given scheduler_id and task_id \n@2020 by Guilherme Zambon.")

  if (args.size < 2) {
    log.error("Invalid number of arguments.")
    log.error("Usage: importer <scheduler_id> <task_id> start_date end_date ")
    actorSystem.terminate
    System.exit(-1)
  }

  val schedulerId = UUID.fromString(args(0))
  val taskId = UUID.fromString(args(1))
  val startDate: Option[LocalDateTime] = if (args.size > 2) Some(LocalDateTime.parse(args(2))) else None: Option[LocalDateTime]
  val endDate: Option[LocalDateTime] = if (args.size > 3) Some(LocalDateTime.parse(args(3))) else None: Option[LocalDateTime]
  val outputFilePath = s"${schedulerId}-${taskId}_completed_tasks.zb"


  log.info(s"Exporting completed tasks from:\n" +
    s"scheduler_id -> ${schedulerId}\n" +
    s"task_id -> ${taskId}\n" +
    s"start_date -> ${startDate.fold("not specified")(s => s.toString)}\n" +
    s"end_date -> ${endDate.fold("not specified")(s => s.toString)}\n" +
    s"output file -> ${outputFilePath}")

  val source = Source.queue[String](1024 * 1024, OverflowStrategy.backpressure)
    .map(s => ByteString(s + "\n"))

  val sink = FileIO.toPath(Paths.get(outputFilePath), Set(SYNC, WRITE, TRUNCATE_EXISTING, CREATE))

  val (queue, file) = source
    .toMat(sink)(Keep.both)
    .run

    Source(1 to 5000)
    .mapAsync(1)(x => queue.offer(x.toString))
    .runWith(Sink.ignore)
    .onComplete { _ => queue.complete }


}
