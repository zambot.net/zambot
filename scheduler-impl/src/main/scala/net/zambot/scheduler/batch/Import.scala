package net.zambot.scheduler.batch

import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.stream.scaladsl.{FileIO, Framing, Keep, Sink}
import akka.util.ByteString

import scala.concurrent.Future
import scala.language.postfixOps

object Import extends App {

  implicit val actorSystem = ActorSystem()
  implicit val ec = actorSystem.dispatcher

  val source = FileIO.fromPath(Paths.get("Output"))
    .via(Framing.delimiter(ByteString("\n"), maximumFrameLength = 1024))
    .map(_.utf8String).mapAsync(1)(x => Future(println(x)))
    .toMat(Sink.ignore)(Keep.right)
    .run
    .onComplete(_ => actorSystem.terminate)

  //  val js = Json.toJson(CompletedTaskEvent(schedulerId = UUID.randomUUID(), taskId = UUID.randomUUID(), externalId = None, created = LocalDateTime.now,
  //    timestamp = LocalDateTime.now, completedTimestamp = LocalDateTime.now, message = "OK",
  //    success = true, result = ListMap("A" -> "B", "C" -> "D")))
  //
  //  val str = Json.stringify(js);
  //  println(s"AEEE: $str")
  //
  //  val cc = Json.fromJson[CompletedTaskEvent](Json.parse(str)).get
  //  println(s"AEEE: $cc")

}
