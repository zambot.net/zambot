package net.zambot.tracker.impl

import akka.cluster.sharding.typed.scaladsl.Entity
import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.playjson.JsonSerializerRegistry
import com.lightbend.lagom.scaladsl.server._
import com.softwaremill.macwire._
import net.zambot.tracker.api.TrackerService
import org.slf4j.LoggerFactory
import play.api.libs.ws.ahc.AhcWSComponents

import scala.language.postfixOps

class TrackerLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new TrackApplication(context) {
      override def serviceLocator: ServiceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new TrackApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[TrackerService])
}

abstract class TrackApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with CassandraPersistenceComponents
    with LagomKafkaComponents
    with AhcWSComponents {

  private val log = LoggerFactory.getLogger("TrackApplication")

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[TrackerService](wire[TrackerServiceImpl])

  // Register the JSON serializer registry
  override lazy val jsonSerializerRegistry: JsonSerializerRegistry = TrackSerializerRegistry

  // Initialize the sharding of the Aggregate. The following starts the aggregate Behavior under
  // a given sharding entity typeKey.
  clusterSharding.init(
    Entity(TrackState.typeKey)(
      entityContext => TrackBehavior.create(entityContext)
    )
  )
}
