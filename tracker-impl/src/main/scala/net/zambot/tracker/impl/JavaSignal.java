package net.zambot.tracker.impl;

import java.util.Map;

public class JavaSignal {

    public final String created;
    public final String timestamp;
    public final String start;
    public final String lastTimestamp;
    public final long position;
    public final long sequence;
    public final long version;
    public final Map<String, String> data;

    public JavaSignal(String created, String timestamp, String start, String lastTimestamp,
                      long position, long sequence, long version, Map<String, String> data) {
        this.created = created;
        this.timestamp = timestamp;
        this.start = start;
        this.lastTimestamp = lastTimestamp;
        this.position = position;
        this.sequence = sequence;
        this.version = version;
        this.data = data;

    }

}
