package net.zambot.tracker.impl;

import java.util.Map;

public class JavaSample {

    public final String timestamp;
    public final Map<String, String> data;

    public JavaSample(String timestamp, Map<String, String> data) {
        this.timestamp = timestamp;
        this.data = data;
    }

}
