package net.zambot.tracker.impl

import java.time.{LocalDateTime, ZoneId}
import java.util.Date

import akka.Done
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, EventStreamElement, ReadSideProcessor}

import scala.concurrent.{ExecutionContext, Future, Promise}

import scala.jdk.CollectionConverters._
class TrackEventProcessor(session: CassandraSession, readSide: CassandraReadSide)
                             (implicit ec: ExecutionContext) extends ReadSideProcessor[TrackEvent] {

  override def aggregateTags: Set[AggregateEventTag[TrackEvent]] = Set(TrackEvent.Tag)

  override def buildHandler(): ReadSideProcessor.ReadSideHandler[TrackEvent] = {
    readSide.builder[TrackEvent]("eventProcessor-track_events")
      .setGlobalPrepare(() => createTrackListTable)
      .setEventHandler[TrackInitialized](processTrackEvent)
      .setEventHandler[TrackStatusUpdated](processTrackEvent)
      .setPrepare(ev => prepareTrackSession)
      .build

  }

  private def createTrackListTable: Future[Done] = {
    session.executeCreateTable("CREATE TABLE IF NOT EXISTS tracker.track_list (track_id UUID," +
      " inbound_channel_id UUID, outbound_channel_id UUID, created timestamp, updated timestamp, status text," +
      " parameters frozen<map<text, text>>, interval text, version bigint," +
      " PRIMARY KEY(track_id))")
  }

  private val writeTrackPromise = Promise[PreparedStatement] // initialized in prepare
  private def writeTracker: Future[PreparedStatement] = writeTrackPromise.future


  private def prepareTrackSession: Future[Done] = {
    val f = session.prepare("INSERT INTO tracker.track_list (track_id, inbound_channel_id, outbound_channel_id," +
      " status, created, updated, interval, version, parameters) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)")

    writeTrackPromise.completeWith(f)
    f.map(_ => Done)

  }

  private def processTrackEvent(ev: EventStreamElement[TrackEvent]): Future[List[BoundStatement]] = ev.event match {
    case e: TrackInitialized => {
      writeTracker.map {
        ps =>
          val binds = {
            val bind = ps.bind()
              .setUUID("track_id", java.util.UUID.fromString(e.id))
              .setString("status", e.status)
              .setTimestamp("created", Date.from(LocalDateTime.parse(e.created).atZone(ZoneId.systemDefault()).toInstant()))
              .setTimestamp("updated", Date.from(LocalDateTime.parse(e.timestamp).atZone(ZoneId.systemDefault()).toInstant()))
              .setString("interval", e.timeWindowDuration)
              .setLong("version", e.version)
              .setMap("parameters", e.parameters.asJava)

            e.inboundChannelId match {
              case Some(s) => bind.setUUID("inbound_channel_id", java.util.UUID.fromString(s))
              case None => bind.setToNull("v")
            }

            e.outboundChannelId match {
              case Some(s) => bind.setUUID("outbound_channel_id", java.util.UUID.fromString(s))
              case None => bind.setToNull("outbound_channel_id")
            }

            bind
          }
          List(binds)
      }
    }

    case e: TrackStatusUpdated => {
      writeTracker.map {
        ps =>
          val binds = ps.bind()
            .setUUID("track_id", e.trackId)
            .setString("status", e.status)
            .setTimestamp("created", Date.from(LocalDateTime.parse(e.created).atZone(ZoneId.systemDefault()).toInstant()))
            .setTimestamp("updated", Date.from(LocalDateTime.parse(e.timestamp).atZone(ZoneId.systemDefault()).toInstant()))
            .setString("interval", e.interval)
            .setLong("version", e.version)
            .setMap("parameters", e.parameters.asJava)
          List(binds)
      }
    }

    case _ => Future {
      Nil
    }

  }

}