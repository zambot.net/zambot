package net.zambot.tracker.impl

import java.time.{LocalDateTime, ZoneId}
import java.util.{Date, UUID}

import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, EntityRef}
import akka.persistence.query.Offset
import akka.stream.scaladsl.{Flow, Sink}
import akka.util.Timeout
import akka.{Done, NotUsed}
import com.datastax.driver.core.SimpleStatement
import com.datastax.driver.core.utils.UUIDs
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.transport.{BadRequest, NotFound}
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry, ReadSide}
import com.softwaremill.macwire.wire
import net.zambot.tracker.api.{SignalEvent, TrackerService}
import net.zambot.tracker.model
import net.zambot.tracker.model.{EntityId, TrackInfo, _}
import org.slf4j.LoggerFactory

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.jdk.CollectionConverters._


/**
  * Implementation of the SchedulerService.
  */
class TrackerServiceImpl(clusterSharding: ClusterSharding, persistentEntityRegistry: PersistentEntityRegistry,
                         cassandraSession: CassandraSession, cassandraReadSide: CassandraReadSide,
                         readSide: ReadSide)(implicit ec: ExecutionContext,
                                             materializer: akka.stream.Materializer) extends TrackerService {

  private val log = LoggerFactory.getLogger("TrackerServiceImpl")

  readSide.register[TrackEvent](wire[TrackEventProcessor])
  readSide.register[TrackEvent](wire[SignalEventProcessor])

  /**
    * Looks up the entity for the given ID.
    */
  private def entityRef(id: String): EntityRef[TrackCommand] =
    clusterSharding.entityRefFor(TrackState.typeKey, id)

  implicit val timeout: Timeout = Timeout(5.seconds)

  /**
    * Play Signals for a track
    *
    * @param trackId   The Track identifier
    * @param startDate (optional)
    * @param endDate   (optional)
    * @param pageSize  (optional)
    * @return SignalInfoList
    */
  override def findSignal(trackId: UUID, startDate: Option[String], endDate: Option[String],
                          pageSize: Option[Int]): ServiceCall[NotUsed, SignalInfoSummaryList] = { _ =>

    def generateWhereClause(after: Option[LocalDateTime], before: Option[LocalDateTime]): String = {

      @tailrec
      def whereClause(item: Option[String], list: List[Option[String]], counter: Int, currentClause: String = ""): String = {
        item match {
          case None => if (list.isEmpty) currentClause else whereClause(list.head, list.tail, counter, currentClause)
          case Some(s) => if (counter == 0)
            if (list.isEmpty) "where " + s else whereClause(list.head, list.tail, counter + 1, "where " + s)
          else if (list.isEmpty) currentClause + " and " + s
          else whereClause(list.head, list.tail, counter + 1, currentClause + " and " + s)

        }
      }

      val dateFilter: Option[String] = after.fold(
        before.fold(None: Option[String])(_ => Some("signal_id < ?"))
      )(_ => Some("signal_id > ?"))

      val filterList: List[Option[String]] = List(Some("track_id = ?"), dateFilter)
      if (filterList.isEmpty) "" else whereClause(filterList.head, filterList.tail, 0)

    }

    def generateParams(pageSize: Int, after: Option[LocalDateTime], before: Option[LocalDateTime]): Array[Object] = {

      val date: Option[UUID] = after.fold(before.fold(None: Option[UUID])(after =>
        Some(UUIDs.startOf(Date.from(after.atZone(ZoneId.systemDefault()).toInstant).getTime))))(before => Some(UUIDs.startOf(Date.from(before.atZone(ZoneId.systemDefault()).toInstant).getTime)))

      val parameters: Seq[Object] = Seq(trackId.asInstanceOf[Object]) ++ (date match {
        case Some(o) => Seq(o.asInstanceOf[Object]);
        case _ => Nil
      }) ++
        Seq(pageSize.asInstanceOf[Object])

      parameters.toArray

    }

    val ps = pageSize.getOrElse(10)

    val after: Option[LocalDateTime] = startDate.fold(None: Option[LocalDateTime])(s => Some(java.time.LocalDateTime.parse(s)))
    val before: Option[LocalDateTime] = endDate.fold(None: Option[LocalDateTime])(s => Some(java.time.LocalDateTime.parse(s)))

    entityRef(trackId.toString).ask[Confirmation](replyTo =>
      GetTrackInfo(replyTo = replyTo)
    ).map {
      case r: TrackInfoSummary => {
        val statement = new SimpleStatement(
          s"SELECT track_id, signal_id, created, ts, start, last_timestamp," +
            s"sequence, position, version, data FROM tracker.emitted_signal " +
            generateWhereClause(after, before) + " LIMIT ? ALLOW FILTERING",
          generateParams(ps, after, before): _*)

        statement.setFetchSize(ps)

        val source = cassandraSession.select(statement).map(row =>
          SignalInfoSummary(
            signalId = row.getUUID("signal_id"),
            lastTimestamp = java.time.LocalDateTime.ofInstant(row.getTimestamp("last_timestamp").toInstant, ZoneId.systemDefault()),
            created = java.time.LocalDateTime.ofInstant(row.getTimestamp("created").toInstant, ZoneId.systemDefault()),
            timestamp = java.time.LocalDateTime.ofInstant(row.getTimestamp("ts").toInstant, ZoneId.systemDefault()),
            start = java.time.LocalDateTime.ofInstant(row.getTimestamp("start").toInstant, ZoneId.systemDefault()),
            position = row.getLong("position"),
            sequence = row.getLong("sequence"),
            version = row.getLong("version"),
            data = row.getMap("data", classOf[String], classOf[String]).asScala.toMap
          )
        )

        source.runWith(Sink.seq)
          .map(e => SignalInfoSummaryList(e.reverse))
      }
      case _ => throw NotFound(s"Track ${trackId.toString} was not found.")

    }.flatMap(a => a)

  }

  /**
    * List Tracks
    * List Tracks
    *
    * @param nextPageToken     (optional)
    * @param previousPageToken (optional)
    * @param pageSize          (optional)
    * @return TrackInfoList
    */
  override def findTrack(nextPageToken: Option[UUID], previousPageToken: Option[UUID], pageSize: Option[Int],
                         inboundChannelId: Option[UUID], outboundChannelId: Option[UUID]): ServiceCall[NotUsed, TrackInfoList] = { _ =>

    val ps = pageSize.getOrElse(10)

    def generateWhereClause(previousPageToken: Option[UUID], nextPageToken: Option[UUID]): String = {
      @tailrec
      def whereClause(item: Option[String], list: List[Option[String]], counter: Int, currentClause: String = ""): String = {
        item match {
          case None => if (list.isEmpty) currentClause else whereClause(list.head, list.tail, counter, currentClause)
          case Some(s) => if (counter == 0)
            if (list.isEmpty) "where " + s else whereClause(list.head, list.tail, counter + 1, "where " + s)
          else if (list.isEmpty) currentClause + " and " + s
          else whereClause(list.head, list.tail, counter + 1, currentClause + " and " + s)

        }
      }

      val pageTokenFilter: Option[String] = previousPageToken.fold(
        nextPageToken.fold(None: Option[String])(_ => Some("token(agent_id) > token(?)")))(_ => Some("token(agent_id) < token(?)"))

      val filterList: List[Option[String]
      ] = List(pageTokenFilter,
        inboundChannelId.fold(None: Option[String])(e => Some("inbound_channel_id=?")),
        outboundChannelId.fold(None: Option[String])(e => Some("outbound_channel_id=?")))
      if (filterList.isEmpty) "" else whereClause(filterList.head, filterList.tail, 0)

    }

    def generateParams(previousPageToken: Option[UUID], nextPageToken: Option[UUID], inboundChannelId: Option[UUID],
                       outboundChannelId: Option[UUID]): Array[Object] = {
      val pageToken = previousPageToken.fold(nextPageToken.fold(None: Option[UUID])(nextPageToken => Some(nextPageToken)))(previousPageToken => Some(previousPageToken))
        .fold(Seq(ps.asInstanceOf[Object]))(token => Seq(token.asInstanceOf[Object], ps.asInstanceOf[Object]))

      val parameters: Seq[Object] = inboundChannelId.fold(Nil: Seq[Object])(e =>
        Seq(e.asInstanceOf[Object])
      ) ++ outboundChannelId.fold(Nil: Seq[Object])(e =>
        Seq(e.asInstanceOf[Object])
      ) ++ pageToken

      parameters.toArray

    }

    val statement = new SimpleStatement(
      "SELECT track_id, inbound_channel_id, outbound_channel_id, created, updated, status, interval, " +
        s"parameters, version FROM tracker.track_list ${generateWhereClause(previousPageToken, nextPageToken)} LIMIT ? ALLOW FILTERING",
      generateParams(previousPageToken, nextPageToken, inboundChannelId, outboundChannelId): _*)

    statement.setFetchSize(ps)

    val source = cassandraSession.select(statement).map(row =>
      TrackInfo(
        trackId = row.getUUID("track_id"),
        inboundChannelId = if (row.isNull("inbound_channel_id")) None else Some(row.getUUID("inbound_channel_id")),
        outboundChannelId = if (row.isNull("outbound_channel_id")) None else Some(row.getUUID("outbound_channel_id")),
        created = java.time.LocalDateTime.ofInstant(row.getTimestamp("created").toInstant, ZoneId.systemDefault()),
        timestamp = java.time.LocalDateTime.ofInstant(row.getTimestamp("updated").toInstant, ZoneId.systemDefault()),
        interval = row.getString("interval"),
        status = row.getString("status"),
        version = row.getLong("version"),
        parameters = TrackParameters(values = row.getMap("parameters", classOf[String], classOf[String]).asScala.toMap)
      ))

    source.runWith(Sink.fold[Seq[TrackInfo], TrackInfo](Seq())((seq, elem) => seq :+ elem))
      .map(e => TrackInfoList(e))
  }

  /**
    * Retrieves Track Information
    *
    * @param trackId Track identifier
    * @return TrackInfo
    */
  override def getTrackInfo(trackId: UUID): ServiceCall[NotUsed, TrackInfo] = { _ =>
    val ref = entityRef(trackId.toString)
    ref.ask[Confirmation](replyTo => GetTrackInfo(
      replyTo = replyTo)
    ).map {
      case r: TrackInfoSummary => TrackInfo(
        trackId = trackId,
        inboundChannelId = r.inboundChannelId,
        outboundChannelId = r.outboundChannelId,
        created = LocalDateTime.parse(r.created),
        status = r.status,
        timestamp = LocalDateTime.parse(r.timestamp),
        interval = r.interval,
        version = r.version,
        parameters = TrackParameters(values = r.parameters)
      )
      case _ => throw NotFound(s"Track ${trackId.toString} was not found.")

    }

  }

  /**
    * Initializes a Track
    * Initializes a Track
    *
    * @return EntityId Body Parameter  Track Initialization Info
    */
  override def initializeTrack(): ServiceCall[CreateTrack, EntityId] = { request =>
    val newTrackId = java.util.UUID.randomUUID()
    val ref = entityRef(newTrackId.toString)
    ref.ask[Confirmation](replyTo => InitializeTrack(id = newTrackId.toString,
      inboundChannelId = request.inboundChannelId.fold(None: Option[String])(e => Some(e.toString)),
      outboundChannelId = request.outboundChannelId.fold(None: Option[String])(e => Some(e.toString)),
      timeWindowDuration = request.interval,
      start = request.start, parameters = request.parameters.values, replyTo = replyTo)
    ).map {
      case TrackId(id) => EntityId(java.util.UUID.fromString(id))
      case Rejected(reason) => throw BadRequest(reason)
      case _ => throw BadRequest("Invalid request.")
    }


  }

  /**
    * Records a Sample
    * Records a Sample
    *
    * @param trackId The Track identifier
    * @return EntityId Body Parameter  Records a Sample
    */
  override def sample(trackId: UUID): ServiceCall[model.Sample, SignalInfo] = { request =>
    import scala.language.postfixOps

    val ref = entityRef(trackId.toString)
    ref.ask[Confirmation](replyTo =>
      net.zambot.tracker.impl.Sample(timestamp = request.timestamp,
        data = request.data,
        replyTo = replyTo)
    )
      .map {
        case s: GeneratedUnstableSignal => net.zambot.tracker.model.SignalInfo(
          created = LocalDateTime.parse(s.created),
          timestamp = LocalDateTime.parse(s.timestamp),
          start = LocalDateTime.parse(s.start),
          lastTimestamp = LocalDateTime.parse(s.lastTimestamp),
          position = s.position, sequence = s.sequence,
          version = s.version,
          data = s.data,
          signalId = java.util.UUID.fromString(s.signalId),
          inboundChannelId = s.inboundChannelId,
          outboundChannelId = s.outboundChannelId
        )
        case Rejected(reason) => throw BadRequest(s"$trackId, $reason")
        case _ => throw BadRequest("Invalid request.")
      }

  }

  /**
    * Updates track status
    * Updates track status
    *
    * @param trackId Track identifier
    * @return void Body Parameter  Track Status
    */
  override def updateTrackStatus(trackId: UUID): ServiceCall[model.UpdateTrackStatusInfo, Done] = { request =>
    val ref = entityRef(trackId.toString)
    ref.ask[Confirmation](replyTo => UpdateTrackStatus(status = request.status, replyTo = replyTo)
    ).map {
      case Accepted => Done
      case Rejected(reason) => throw BadRequest(reason)
      case _ => throw BadRequest("Invalid request")
    }

  }

  override def signalEventsTopic(): Topic[SignalEvent] = TopicProducer.singleStreamWithOffset {
    fromOffset =>
      persistentEntityRegistry.eventStream(net.zambot.tracker.impl.SignalEvent.Tag, fromOffset)
        .mapConcat(filterSignalEvents)
  }

  private def filterSignalEvents(ev: EventStreamElement[TrackEvent]): scala.collection.immutable.Iterable[(SignalEvent, Offset)] = ev match {
    case ev@EventStreamElement(_, e: SignalEmitted, offset) => scala.collection.immutable.Iterable((convertGeneratedSignalToSignal(e), offset))
    case _ => Nil
  }

  private def convertGeneratedSignalToSignal(e: SignalEmitted): SignalEvent = SignalEvent(
    trackId = java.util.UUID.fromString(e.trackId),
    inboundChannelId = e.inboundChannelId.fold(None: Option[UUID])(e => Some(java.util.UUID.fromString(e))),
    outboundChannelId = e.outboundChannelId.fold(None: Option[UUID])(e => Some(java.util.UUID.fromString(e))),
    signalId = java.util.UUID.fromString(e.signalId),
    created = java.time.LocalDateTime.parse(e.created),
    timestamp = java.time.LocalDateTime.parse(e.timestamp),
    lastTimestamp = java.time.LocalDateTime.parse(e.lastTimestamp),
    start = java.time.LocalDateTime.parse(e.start),
    data = e.data,
    version = e.version,
    position = e.position,
    sequence = e.sequence,
    stable = e.stable
  )

}