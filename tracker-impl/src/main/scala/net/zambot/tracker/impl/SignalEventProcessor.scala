package net.zambot.tracker.impl

import java.time.{LocalDateTime, ZoneId}
import java.util.Date

import akka.Done
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, EventStreamElement, ReadSideProcessor}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.jdk.CollectionConverters._


class SignalEventProcessor(session: CassandraSession, readSide: CassandraReadSide)
                          (implicit ec: ExecutionContext) extends ReadSideProcessor[TrackEvent] {

  override def aggregateTags: Set[AggregateEventTag[TrackEvent]] = Set(SignalEvent.Tag)

  override def buildHandler(): ReadSideProcessor.ReadSideHandler[TrackEvent] = {
    readSide.builder[TrackEvent]("eventProcessor-track_signal_events")
      .setGlobalPrepare(() => globalPrepare)
      .setEventHandler[SignalEmitted](processTrackEvent)
      .setPrepare(ev => prepareEmittedSignalSession)
      .build

  }

  private def globalPrepare: Future[Done] = {
    createEmittedSignalEventTable
  }

  private def createEmittedSignalEventTable: Future[Done] = {
    session.executeCreateTable("CREATE TABLE IF NOT EXISTS tracker.emitted_signal (track_id uuid, " +
      "signal_id timeuuid, created timestamp, start timestamp, ts timestamp, last_timestamp timestamp, sequence bigint, " +
      "position bigint, version bigint, data frozen<map<text, text>>,  PRIMARY KEY ((track_id), signal_id)) " +
      "WITH CLUSTERING ORDER BY (signal_id desc)")

  }

  private val writeEmittedSignalPromise = Promise[PreparedStatement] // initialized in prepare
  private def writeEmittedSignalEvent: Future[PreparedStatement] = writeEmittedSignalPromise.future

  private def prepareEmittedSignalSession: Future[Done] = {
    val f = session.prepare("INSERT INTO tracker.emitted_signal (track_id, signal_id," +
      "start, created, ts, last_timestamp, sequence, position, version, data) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

    writeEmittedSignalPromise.completeWith(f)
    f.map(_ => Done)

  }

  private def processTrackEvent(ev: EventStreamElement[TrackEvent]): Future[List[BoundStatement]] = {

    def createTrackBind(ps: PreparedStatement, trackId: String, signalId: String, lastTimestamp: String, start: String,
                        created: String, timestamp: String, sequence: Long, position: Long, version: Long,
                        data: Map[String, String]): BoundStatement = {

      val bind = ps.bind()
        .setUUID("track_id", java.util.UUID.fromString(trackId))
        .setUUID("signal_id", java.util.UUID.fromString(signalId))
        .setTimestamp("created", Date.from(LocalDateTime.parse(created).atZone(ZoneId.systemDefault()).toInstant()))
        .setTimestamp("start", Date.from(LocalDateTime.parse(start).atZone(ZoneId.systemDefault()).toInstant()))
        .setTimestamp("last_timestamp", Date.from(LocalDateTime.parse(lastTimestamp).atZone(ZoneId.systemDefault()).toInstant()))
        .setTimestamp("ts", Date.from(LocalDateTime.parse(timestamp).atZone(ZoneId.systemDefault()).toInstant()))
        .setLong("sequence", sequence)
        .setLong("position", position)
        .setLong("version", version)
        .setMap("data", data.asJava)

      bind

    }

    ev.event match {
      case signalEmitted: SignalEmitted => {
        if (signalEmitted.stable) {
          writeEmittedSignalEvent.map { ps =>
            List(createTrackBind(
              ps = ps,
              trackId = signalEmitted.trackId,
              lastTimestamp = signalEmitted.lastTimestamp,
              start = signalEmitted.start,
              sequence = signalEmitted.sequence,
              position = signalEmitted.position,
              version = signalEmitted.version,
              data = signalEmitted.data,
              signalId = signalEmitted.signalId,
              timestamp = signalEmitted.timestamp,
              created = signalEmitted.created))
          }
        } else Future {
          Nil
        }
      }
      case _ => Future {

        Nil
      }

    }

  }

}