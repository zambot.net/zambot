package net.zambot.tracker.impl

import java.io.InputStreamReader
import java.time.{Duration, LocalDateTime, ZoneId}
import java.util.{Date, UUID}

import akka.actor.typed.{ActorRef, Behavior}
import akka.cluster.sharding.typed.scaladsl._
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior, ReplyEffect}
import com.datastax.driver.core.utils.UUIDs
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventTag, AkkaTaggerAdapter}
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import org.slf4j.LoggerFactory
import play.api.libs.json.{Format, Json, _}

import scala.collection.immutable.{ListMap, Seq}
import scala.jdk.CollectionConverters._

object TrackBehavior {

  val jsInvocable: javax.script.Invocable = {
    val engine = new javax.script.ScriptEngineManager().getEngineByName("JavaScript")
    engine.eval(new InputStreamReader(this.getClass.getClassLoader.getResourceAsStream("track-rules.js")))
    engine.asInstanceOf[javax.script.Invocable]
  }

  def create(entityContext: EntityContext[TrackCommand]): Behavior[TrackCommand] = {
    val persistenceId: PersistenceId = PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId)

    create(persistenceId)
      .withTagger(
        AkkaTaggerAdapter.fromLagom(entityContext, TrackEvent.Tag)
      ).withTagger(
      AkkaTaggerAdapter.fromLagom(entityContext, SignalEvent.Tag)
    )

  }

  /*
   * This method is extracted to write unit tests that are completely independendant to Akka Cluster.
   */
  private[impl] def create(persistenceId: PersistenceId) = EventSourcedBehavior
    .withEnforcedReplies[TrackCommand, TrackEvent, TrackState](
      persistenceId = persistenceId,
      emptyState = TrackState.initial,
      commandHandler = (tracker, cmd) => tracker.applyCommand(cmd),
      eventHandler = (tracker, evt) => tracker.applyEvent(evt)
    ).snapshotWhen {
    case (state, event, sequence) => sequence % 100 == 0
  }
}

/**
  * The current state of the Aggregate.
  */
final case class TrackState(id: Option[String], inboundChannelId: Option[String], outboundChannelId: Option[String],
                            created: String, status: String, timestamp: String, timeWindowStart: String,
                            timeWindowSequence: Long, timeWindowDuration: String, parameters: Map[String, String],
                            stableSignal: Option[Signal], unstableSignal: Option[Signal], version: Long) {

  private val log = LoggerFactory.getLogger("TrackState")

  def applyCommand(cmd: TrackCommand): ReplyEffect[TrackEvent, TrackState] = status match {
    case "notInitialized" => cmd match {
      case c: InitializeTrack => onInitializeTrack(c)
      case c: GetTrackInfo => Effect.reply(c.replyTo)(Rejected(s"Track not initialized"))
      case c: UpdateTrackStatus => Effect.reply(c.replyTo)(Rejected(s"Track not initialized"))
      case c: Sample => Effect.reply(c.replyTo)(Rejected(s"Track not initialized"))
    }
    case "paused" => cmd match {
      case c: InitializeTrack => onInitializeTrack(c)
      case c: GetTrackInfo => onGetTrackInfo(c)
      case c: UpdateTrackStatus => onUpdateTrackStatus(c)
      case c: Sample => onSample(c)
    }
    case "recording" => cmd match {
      case c: InitializeTrack => Effect.reply(c.replyTo)(Rejected(s"Track already initialized"))
      case c: GetTrackInfo => onGetTrackInfo(c)
      case c: UpdateTrackStatus => onUpdateTrackStatus(c)
      case c: Sample => onSample(c)
    }

  }

  def applyEvent(evt: TrackEvent): TrackState =
    evt match {
      case e: TrackInitialized => initializeTrack(e)
      case e: TrackStatusUpdated => updateTrackStatus(e)
      case e: SignalEmitted => emitSignal(e)
    }

  def supportedTrackStatus = List("notInitialized", "recording", "paused")

  def validateTrackStatus(trackStatus: String): Seq[String] =
    if (supportedTrackStatus.contains(trackStatus)) Nil else Seq(s"Status not supported. Supported Status are: $supportedTrackStatus")

  def validateUpdateTrackStatus(cmd: UpdateTrackStatus): Seq[String] = validateTrackStatus(cmd.status)

  private def onUpdateTrackStatus(cmd: UpdateTrackStatus): ReplyEffect[TrackEvent, TrackState] = {
    val errorList = validateUpdateTrackStatus(cmd)
    errorList match {
      case Nil => Effect
        .persist(
          TrackStatusUpdated(
            trackId = java.util.UUID.fromString(id.get),
            timestamp = LocalDateTime.now.toString,
            status = cmd.status,
            version = version + 1,
            interval = timeWindowDuration,
            created = created,
            parameters = parameters
          )
        ).thenReply(cmd.replyTo) { _ =>
        Accepted
      }
      case errors: Seq[String] => Effect.reply(cmd.replyTo)(Rejected(s"Error setting status: ${errors.fold("")((acc, s) => acc + ", " + s)}"))
    }
  }

  private def onInitializeTrack(cmd: InitializeTrack): ReplyEffect[TrackEvent, TrackState] =
    status match {
      case "notInitialized" => Effect
        .persist({
          val now = LocalDateTime.now
          TrackInitialized(id = cmd.id,
            inboundChannelId = cmd.inboundChannelId.fold(None: Option[String])(e => Some(e.toString)),
            outboundChannelId = cmd.outboundChannelId.fold(None: Option[String])(e => Some(e.toString)),
            status = "recording",
            timeWindowDuration = cmd.timeWindowDuration,
            created = now.toString,
            timestamp = now.toString,
            start = cmd.start,
            version = 1,
            parameters = cmd.parameters
          )
        }).thenReply(cmd.replyTo) { _ =>
        TrackId(cmd.id)
      }
      case _ => Effect.reply(cmd.replyTo)(Rejected("Already initialized."))

    }

  private def onGetTrackInfo(cmd: GetTrackInfo): ReplyEffect[TrackEvent, TrackState] =
    Effect.reply(cmd.replyTo)(TrackInfoSummary(
      trackId = java.util.UUID.fromString(id.get),
      inboundChannelId = inboundChannelId.fold(None: Option[UUID])(e => Some(java.util.UUID.fromString(e))),
      outboundChannelId = outboundChannelId.fold(None: Option[UUID])(e => Some(java.util.UUID.fromString(e))),
      created = created, status = status, timestamp = timestamp, parameters = parameters,
      interval = timeWindowDuration, version = version))

  private def onSample(cmd: Sample): ReplyEffect[TrackEvent, TrackState] = {

    def process(sample: Sample): List[SignalEmitted] = {

      def initializeSignal(sample: Sample): Signal = {
        if (log.isDebugEnabled()) log.debug("I N I T I A L I Z E D ! ! !")
        val now = LocalDateTime.now.toString

        val positionOffset = Duration.between(
          LocalDateTime.parse(timeWindowStart), sample.timestamp).toMillis / Duration.parse(timeWindowDuration).toMillis

        val start = LocalDateTime.parse(timeWindowStart).plus(Duration.parse(timeWindowDuration).multipliedBy(positionOffset))

        Signal(created = now,
          timestamp = sample.timestamp.toString,
          start = start.toString,
          lastTimestamp = sample.timestamp.toString,
          position = positionOffset,
          sequence = 1,
          version = 0,
          signalId = UUIDs.startOf(Date.from(start.atZone(ZoneId.systemDefault()).toInstant).getTime).toString,
          data = ListMap.empty

        )

      }

      def generateSignal(reference: Signal, unstable: Signal, sample: Sample): List[SignalEmitted] = {
        val now = LocalDateTime.now.toString

        val positionOffset = Duration.between(
          LocalDateTime.parse(unstable.start), sample.timestamp).toMillis / Duration.parse(timeWindowDuration).toMillis

        val start = LocalDateTime.parse(unstable.start).plus(Duration.parse(timeWindowDuration).multipliedBy(positionOffset))

        if (log.isDebugEnabled()) log.debug(s"currentPosition: $positionOffset, reference.position: ${reference.position}")

        if (positionOffset > 0) {
          if (log.isDebugEnabled()) log.debug(s"Stable ${reference.position} and Unstable ${unstable.position}")

          val newSignalId = UUIDs.startOf(Date.from(start.atZone(ZoneId.systemDefault()).toInstant).getTime)
          val newSequence = unstable.sequence + 1
          val newLastTimestamp = sample.timestamp
          val newPosition = unstable.position + positionOffset
          val newVersion: Long = 1

          // new stable and unstable to be created
          List(/** stable */
            SignalEmitted(trackId = id.get,
              inboundChannelId = inboundChannelId,
              outboundChannelId = outboundChannelId,
              stable = true,
              created = unstable.created,
              timestamp = now,
              start = unstable.start,
              lastTimestamp = unstable.lastTimestamp,
              position = unstable.position,
              sequence = unstable.sequence,
              version = unstable.version,
              signalId = unstable.signalId,
              data = unstable.data),

            /** create a brand new unstable */
            SignalEmitted(trackId = id.get,
              inboundChannelId = inboundChannelId,
              outboundChannelId = outboundChannelId,
              stable = false,
              created = now,
              timestamp = now,
              start = start.toString,
              lastTimestamp = newLastTimestamp.toString,
              position = newPosition,
              sequence = newSequence,
              version = newVersion,
              signalId = newSignalId.toString,
              data = applyRules(unstable, unstable.copy(created = now, timestamp = now, start = start.toString,
                lastTimestamp = newLastTimestamp.toString, position = newPosition, version = 1), sample = sample, parameters = parameters))
          )
        } else {
          // unstable needs to be updated
          val newVersion = unstable.version + 1
          val newLastTimestamp = sample.timestamp
          List(SignalEmitted(trackId = id.get,
            inboundChannelId = inboundChannelId,
            outboundChannelId = outboundChannelId,
            stable = false,
            created = unstable.created,
            timestamp = now,
            start = unstable.start,
            lastTimestamp = newLastTimestamp.toString,
            position = unstable.position,
            sequence = unstable.sequence,
            version = newVersion,
            signalId = unstable.signalId,
            data = applyRules(reference, unstable.copy(lastTimestamp = newLastTimestamp.toString, timestamp = now, version = newVersion), sample = sample, parameters = parameters)))
        }

      }

      def applyRules(reference: Signal, unstable: Signal, sample: Sample, parameters: Map[String, String]): Map[String, String] = {

        def convertSignal2JavaSignal(signal: Signal): JavaSignal =
          new JavaSignal(signal.created, signal.timestamp, signal.start, signal.lastTimestamp,
            signal.position, signal.sequence, signal.version, signal.data.asJava)

        def convertSample2JavaSample(sample: Sample): JavaSample =
          new JavaSample(sample.timestamp.toString, sample.data.asJava)


        val start = System.currentTimeMillis
        val res = TrackBehavior.jsInvocable.invokeFunction("process",
          convertSignal2JavaSignal(reference),
          convertSignal2JavaSignal(unstable),
          convertSample2JavaSample(sample),
          parameters.asJava
        ) match {
          case m: java.util.Map[String, String] => m.asScala.toMap
          case d: java.lang.Double => ListMap("result" -> d.toString)
          case f: java.lang.Float => ListMap("result" -> f.toString)
          case i: java.lang.Integer => ListMap("result" -> i.toString)
          case l: java.lang.Long => ListMap("result" -> l.toString)
          case s: java.lang.String => ListMap("result" -> s)
          case o => if (log.isDebugEnabled()) log.debug(s"reference: $reference"); throw new RuntimeException(s"Unrecognized object: $o")

        }
        val end = System.currentTimeMillis

        //        print(s"Took ${end - start} ms")
        res

      }

      stableSignal.fold({
        if (log.isDebugEnabled()) log.debug("Stable not found.")
        unstableSignal.fold({
          if (log.isDebugEnabled()) log.debug("Unstable not found.")
          val newSignal = initializeSignal(sample)
          generateSignal(newSignal, newSignal, sample)

        })(unstableSignal => {
          if (log.isDebugEnabled()) log.debug("Unstable found!")
          generateSignal(unstableSignal, unstableSignal, sample)
        })
      })(stableSignal => {
        if (log.isDebugEnabled()) log.debug("Stable found!")
        generateSignal(stableSignal, unstableSignal.get, sample)
      })

    }

    def validateSample(sample: Sample): List[String] = {
      unstableSignal.fold(Nil: List[String])(unstable =>
        if (sample.timestamp.isBefore(LocalDateTime.parse(unstable.lastTimestamp))) {
          List("Error: sample is before lastTimestamp")
        } else Nil: List[String])
    }

    val errorList = validateSample(cmd)
    errorList match {
      case Nil => Effect
        .persist(process(cmd)).thenReply(cmd.replyTo) { newState =>
        GeneratedUnstableSignal(
          signalId = newState.unstableSignal.get.signalId,
          created = newState.unstableSignal.get.created,
          timestamp = newState.unstableSignal.get.timestamp,
          start = newState.unstableSignal.get.start,
          lastTimestamp = newState.unstableSignal.get.lastTimestamp,
          position = newState.unstableSignal.get.position,
          sequence = newState.unstableSignal.get.sequence,
          version = newState.unstableSignal.get.version,
          inboundChannelId = inboundChannelId,
          outboundChannelId = outboundChannelId,
          data = newState.unstableSignal.get.data)
      }
      case errors: Seq[String] => Effect.reply(cmd.replyTo)(Rejected(s"Error processing sample: ${errors.fold("")((acc, s) => acc + ", " + s)}"))
    }

  }

  private def initializeTrack(event: TrackInitialized) =
    copy(id = Some(event.id),
      status = event.status,
      inboundChannelId = event.inboundChannelId,
      outboundChannelId = event.outboundChannelId,
      timeWindowStart = event.start,
      timeWindowDuration = event.timeWindowDuration,
      timestamp = LocalDateTime.now.toString,
      version = event.version,
      parameters = event.parameters
    )

  private def updateTrackStatus(event: TrackStatusUpdated): TrackState = copy(
    status = event.status,
    timestamp = event.timestamp,
    version = event.version
  )

  private def emitSignal(emitted: SignalEmitted): TrackState = if (emitted.stable) {
    if (log.isDebugEnabled()) log.debug(s"new stable: $emitted")
    copy(
      timestamp = emitted.timestamp, stableSignal = Some(convertSignalEmittedToSignal(emitted)))
  } else {
    if (log.isDebugEnabled()) log.debug(s"new unstable: $emitted")
    copy(
      timestamp = emitted.timestamp, unstableSignal = Some(convertSignalEmittedToSignal(emitted)))
  }

  def convertSignalEmittedToSignal(emitted: SignalEmitted): Signal = Signal(
    created = emitted.created,
    timestamp = emitted.timestamp,
    start = emitted.start,
    lastTimestamp = emitted.lastTimestamp,
    position = emitted.position,
    sequence = emitted.sequence,
    version = emitted.version,
    signalId = emitted.signalId,
    data = emitted.data
  )

}

object TrackState {

  /**
    * The initial state. This is used if there is no snapshotted state to be found.
    */
  def initial: TrackState = TrackState(id = None,
    created = LocalDateTime.now.toString, timestamp = LocalDateTime.now.toString, status = "notInitialized",
    timeWindowStart = LocalDateTime.now.toString, timeWindowSequence = 0, timeWindowDuration = "PT10S",
    unstableSignal = None, stableSignal = None, version = 0, parameters = Map.empty, inboundChannelId = None,
    outboundChannelId = None)

  /**
    * The [[EventSourcedBehavior]] instances (aka Aggregates) run on sharded actors inside the Akka Cluster.
    * When sharding actors and distributing them across the cluster, each aggregate is
    * namespaced under a typekey that specifies a name and also the type of the commands
    * that sharded actor can receive.
    */
  val typeKey: EntityTypeKey[TrackCommand] = EntityTypeKey[TrackCommand]("TrackAggregate")

  /**
    * Format for the Scheduler state.
    *
    * Persisted entities get snapshotted every configured number of events. This
    * means the state gets stored to the database, so that when the aggregate gets
    * loaded, you don't need to replay all the events, just the ones since the
    * snapshot. Hence, a JSON format needs to be declared so that it can be
    * serialized and deserialized when storing to and from the database.
    */


  implicit val signalFormat: Format[Signal] = Json.format
  implicit val trackStateFormat: Format[TrackState] = Json.format
}


/**
  * This interface defines all the events that the SchedulerAggregate supports.
  */
sealed trait TrackEvent extends AggregateEvent[TrackEvent]

object TrackEvent {
  val Tag: AggregateEventTag[TrackEvent] = AggregateEventTag[TrackEvent]
}

object SignalEvent {
  val Tag: AggregateEventTag[TrackEvent] = AggregateEventTag[TrackEvent]
}

/**
  * Events
  */
final case class TrackInitialized(id: String, inboundChannelId: Option[String], outboundChannelId: Option[String], status: String, timeWindowDuration: String, timestamp: String, created: String,
                                  parameters: Map[String, String], start: String, version: Long) extends TrackEvent {
  def aggregateTag: AggregateEventTag[TrackEvent] = TrackEvent.Tag

}

object TrackInitialized {
  implicit val format: Format[TrackInitialized] = Json.format

}

final case class TrackStatusUpdated(trackId: UUID, status: String, created: String, timestamp: String, interval: String,
                                    version: Long, parameters: Map[String, String]) extends TrackEvent {
  def aggregateTag: AggregateEventTag[TrackEvent] = TrackEvent.Tag

}

object TrackStatusUpdated {
  implicit val format: Format[TrackStatusUpdated] = Json.format
}

final case class SignalEmitted(trackId: String, signalId: String, inboundChannelId: Option[String],
                               outboundChannelId: Option[String], stable: Boolean, created: String, timestamp: String,
                               start: String, lastTimestamp: String, position: Long, sequence: Long, version: Long,
                               data: Map[String, String]) extends TrackEvent {
  def aggregateTag: AggregateEventTag[TrackEvent] = SignalEvent.Tag
}

object SignalEmitted {
  implicit val format: Format[SignalEmitted] = Json.format
}

/**
  * Commands
  */
trait TrackCommandSerializable

sealed trait TrackCommand extends TrackCommandSerializable

final case class InitializeTrack(id: String, inboundChannelId: Option[String], outboundChannelId: Option[String],
                                 timeWindowDuration: String, start: String, parameters: Map[String, String],
                                 replyTo: ActorRef[Confirmation]) extends TrackCommand

final case class GetTrackInfo(replyTo: ActorRef[Confirmation]) extends TrackCommand

final case class UpdateTrackStatus(status: String, replyTo: ActorRef[Confirmation]) extends TrackCommand

final case class Sample(timestamp: LocalDateTime, data: Map[String, String], replyTo: ActorRef[Confirmation]) extends TrackCommand

/**
  * Replies
  */
sealed trait Confirmation

case object Confirmation {
  implicit val format: Format[Confirmation] = new Format[Confirmation] {
    override def reads(json: JsValue): JsResult[Confirmation] = {
      if ((json \ "reason").isDefined)
        Json.fromJson[Rejected](json)
      else
        Json.fromJson[Accepted](json)
    }

    override def writes(o: Confirmation): JsValue = {
      o match {
        case acc: Accepted => Json.toJson(acc)
        case schedulerInfoSummary: TrackInfoSummary => Json.toJson(schedulerInfoSummary)
        case schedulerId: TrackId => Json.toJson(schedulerId)
        case generatedUnstableSignal: GeneratedUnstableSignal => Json.toJson(generatedUnstableSignal)
        case rej: Rejected => Json.toJson(rej)
      }
    }
  }
}

sealed trait Accepted extends Confirmation

case object Accepted extends Accepted {
  implicit val format: Format[Accepted] =
    Format(Reads(_ => JsSuccess(Accepted)), Writes(_ => Json.obj()))
}

final case class Rejected(reason: String) extends Confirmation

object Rejected {
  implicit val format: Format[Rejected] = Json.format
}

final case class TrackInfoSummary(trackId: UUID, inboundChannelId: Option[UUID], outboundChannelId: Option[UUID],
                                  created: String, status: String, timestamp: String, interval: String, version: Long,
                                  parameters: Map[String, String]) extends Confirmation

final case class Signal(signalId: String, created: String, timestamp: String, start: String, lastTimestamp: String,
                        position: Long, sequence: Long, version: Long, data: Map[String, String])

object TrackInfoSummary {
  implicit val format: Format[TrackInfoSummary] = Json.format
}

final case class TrackId(value: String) extends Confirmation

object TrackId {
  implicit val format: Format[TrackId] = Json.format
}

final case class GeneratedUnstableSignal(signalId: String, created: String, timestamp: String, start: String,
                                         lastTimestamp: String, inboundChannelId: Option[String],
                                         outboundChannelId: Option[String], position: Long, sequence: Long,
                                         version: Long, data: Map[String, String]) extends Confirmation

object GeneratedUnstableSignal {
  implicit val format: Format[GeneratedUnstableSignal] = Json.format
}

/**
  * Akka serialization, used by both persistence and remoting, needs to have
  * serializers registered for every type serialized or deserialized. While it's
  * possible to use any serializer you want for Akka messages, out of the box
  * Lagom provides support for JSON, via this registry abstraction.
  *
  * The serializers are registered here, and then provided to Lagom in the
  * application loader.
  */
object TrackSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[TrackState],
    JsonSerializer[TrackInitialized],
    JsonSerializer[TrackStatusUpdated],
    JsonSerializer[TrackInfoSummary],
    JsonSerializer[Confirmation],
    JsonSerializer[SignalEmitted],
    JsonSerializer[Accepted],
    JsonSerializer[Rejected]
  )
}
