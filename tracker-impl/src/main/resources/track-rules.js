function getNumber(map, key, def) {
    var o = Number(map.get(key));
    if (o == NaN) return def;
    else return o;
}

function EMA_K(periods) {
    return 2 / (periods + 1);

}

function EMA(current, lastEma, periods) {
    var k = EMA_K(periods);
    return (current * k) + (lastEma * (1 - k));
}

function process(reference, unstable, sample, parameters) {
    var parameter_low_ema = getNumber(parameters, "low_ema", 12);
    var parameter_high_ema = getNumber(parameters, "high_ema", 26);
    var parameter_MACD_signal_ema = getNumber(parameters, "MACD_signal_ema", 9);

    var sample_last = getNumber(sample.data, "last", 0);


    var unstable_seq = getNumber(unstable.data, "seq", 0);
    var unstable_high = getNumber(unstable.data, "high", -1);
    var unstable_low = getNumber(unstable.data, "low", Number.MAX_VALUE);
    var unstable_open = getNumber(unstable.data, "open", -1);

    var reference_low_ema = getNumber(reference.data, "low_ema", sample_last);
    var low_ema = EMA(sample_last, reference_low_ema, parameter_low_ema)

    var reference_high_ema = getNumber(reference.data, "high_ema", sample_last);
    var high_ema = EMA(sample_last, reference_high_ema, parameter_high_ema)

    var macd_line = low_ema - high_ema;

    var reference_macd_line = getNumber(reference.data, "MACD_line", macd_line);
    var macd_signal = EMA(macd_line, reference_macd_line, parameter_MACD_signal_ema);

    var macd_histogram = macd_line - macd_signal;

    var seq = unstable_seq + 1;
    var low = Math.min(unstable_low, sample_last);
    var high = Math.max(unstable_high, sample_last);
    var open = unstable_open;
    var close = sample_last;

    if (unstable.version == 1) {
        open = sample_last;
        low = sample_last;
        high = sample_last;
        close = sample_last;
    }

    return {
        "MACD_histogram": macd_histogram.toString(),
        "MACD_signal": macd_signal.toString(),
        "MACD_line": macd_line.toString(),
        "high_ema": high_ema.toString(),
        "low_ema": low_ema.toString(),
        "low": low.toString(),
        "high": high.toString(),
        "open": open.toString(),
        "close": close.toString(),
        "time" : "",
        "seq": seq.toString()
    }

}