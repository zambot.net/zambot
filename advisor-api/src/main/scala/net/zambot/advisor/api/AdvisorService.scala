package net.zambot.advisor.api

import java.util.UUID

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.broker.kafka.{KafkaProperties, PartitionKeyStrategy}
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import net.zambot.advisor.model._
import play.api.libs.json.{Format, Json}

object AdvisorService {
  val ADVISOR_ADVICE_EVENTS_TOPIC_NAME = "advisor_advice_events"
}

/**
  * The scheduler service interface.
  * <p>
  * This describes everything that Lagom needs to know about how to serve and
  * consume the AdvisorService.
  */
trait AdvisorService extends Service {

  final override def descriptor: Descriptor = {
    import Service._
    named("AdvisorApi")
      .withCalls(
        restCall(Method.POST, "/advisor/:agentId/fact", advise _),
        restCall(Method.GET, "/advisor/:agentId/advice?startDate&endDate&code&value&pageSize", findAdviceAction _),
        restCall(Method.GET, "/advisor?nextPageToken&previousPageToken&pageSize&inboundChannelId&outboundChannelId", findAgent _),
        restCall(Method.GET, "/advisor/:agentId", getAgentInfo _),
        restCall(Method.POST, "/advisor", initializeAgent _),
        restCall(Method.PUT, "/advisor/:agentId/status", updateAgentStatus _))
      .withTopics(
        topic(AdvisorService.ADVISOR_ADVICE_EVENTS_TOPIC_NAME, agentAdviceEventsTopic())
          .addProperty(
            KafkaProperties.partitionKeyStrategy,
            PartitionKeyStrategy[AdviceEvent](e => s"${e.agentId}")
          )
      )
      .withAutoAcl(true)
  }


  /**
    * Presents a fact to the agent
    * Generate advice from fact
    *
    * @param agentId The agent identifier
    * @return EntityId Body Parameter  Generates an advice from fact
    */
  def advise(agentId: UUID): ServiceCall[Fact, GeneratedAdviceInfo]

  /**
    * Shows agent advices
    *
    * @param agentId   The Agent identifier
    * @param startDate (optional)
    * @param endDate   (optional)
    * @param pageSize  (optional)
    * @return AdviceInfoList
    */
  def findAdviceAction(agentId: UUID, startDate: Option[String], endDate: Option[String], code: Option[String], value: Option[String], pageSize: Option[Int]): ServiceCall[NotUsed, AdviceInfoList]

  /**
    * List advisor agents
    * List Advisor Agents
    *
    * @param nextPageToken     (optional)
    * @param previousPageToken (optional)
    * @param pageSize          (optional)
    * @return AgentInfoList
    */
  def findAgent(nextPageToken: Option[UUID] = None, previousPageToken: Option[UUID] = None, pageSize: Option[Int] = None, inboundChannelId: Option[UUID], outboundChannelId: Option[UUID]): ServiceCall[NotUsed, AgentInfoList]

  /**
    * Retrieves agent information
    *
    * @param agentId Agent identifier
    * @return AgentInfo
    */
  def getAgentInfo(agentId: UUID): ServiceCall[NotUsed, AgentInfo]

  /**
    * Initializes an advisor agent
    * Initializes an Advisor Agent
    *
    * @return EntityId Body Parameter  Advisor Agent Initialization Info
    */
  def initializeAgent(): ServiceCall[CreateAgent, AgentId]

  /**
    * Updates agent status
    * Updates agent status
    *
    * @param agentId Advisor Agent identifier
    * @return void Body Parameter  Agent Status
    */
  def updateAgentStatus(agentId: UUID): ServiceCall[UpdateAgentStatus, Done]

  def agentAdviceEventsTopic(): Topic[AdviceEvent]

}

case class AdviceActionEvent(code: String, value: String, data: Map[String, String])

object AdviceActionEvent {
  implicit val format: Format[AdviceActionEvent] = Json.format[AdviceActionEvent]
}

case class AdviceEvent(agentId: String, adviceId: String, timestamp: String, sequence: Long, actions: Seq[AdviceActionEvent])

object AdviceEvent {
  implicit val format: Format[AdviceEvent] = Json.format[AdviceEvent]
}
