package net.zambot.tracker.api

import java.time.LocalDateTime
import java.util.UUID

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.broker.kafka.{KafkaProperties, PartitionKeyStrategy}
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import net.zambot.tracker.model._
import play.api.libs.json.{Format, Json}

object TrackerService {
    val SIGNAL_EVENTS_TOPIC_NAME = "signal_events"
}

/**
  * The track service interface.
  *
  */
trait TrackerService extends Service {

  final override def descriptor = {
    import Service._
    named("TrackerApi").withCalls(
      restCall(Method.GET, "/tracker/:trackId/playback?startDate&endDate&pageSize", findSignal _),
      restCall(Method.GET, "/tracker?nextPageToken&previousPageToken&pageSize&incomingChannelId&outgoingChannelId", findTrack _),
      restCall(Method.GET, "/tracker/:trackId", getTrackInfo _),
      restCall(Method.POST, "/tracker", initializeTrack _),
      restCall(Method.POST, "/tracker/:trackId/record", sample _),
      restCall(Method.PUT, "/tracker/:trackId/status", updateTrackStatus _)
    ).withTopics(
      topic(TrackerService.SIGNAL_EVENTS_TOPIC_NAME, signalEventsTopic())
        .addProperty(
          KafkaProperties.partitionKeyStrategy,
          PartitionKeyStrategy[SignalEvent](e => s"${e.trackId}")
        )
    ).withAutoAcl(true)
  }

  /**
    * Play Signals for a track
    *
    * @param trackId   The Track identifier
    * @param startDate (optional)
    * @param endDate   (optional)
    * @param pageSize  (optional)
    * @return SignalInfoList
    */
  def findSignal(trackId: UUID, startDate: Option[String], endDate: Option[String], pageSize: Option[Int]): ServiceCall[NotUsed, SignalInfoSummaryList]

  /**
    * List Tracks
    * List Tracks
    *
    * @param nextPageToken     (optional)
    * @param previousPageToken (optional)
    * @param pageSize          (optional)
    * @return TrackInfoList
    */
  def findTrack(nextPageToken: Option[UUID] = None, previousPageToken: Option[UUID] = None,
                pageSize: Option[Int] = None, inboundChannelId:Option[UUID], outboundChannelId:Option[UUID]
               ): ServiceCall[NotUsed, TrackInfoList]

  /**
    * Retrieves Track Information
    *
    * @param trackId Track identifier
    * @return TrackInfo
    */
  def getTrackInfo(trackId: UUID): ServiceCall[NotUsed, TrackInfo]

  /**
    * Initializes a Track
    * Initializes a Track
    *
    * @return EntityId Body Parameter  Track Initialization Info
    */
  def initializeTrack(): ServiceCall[CreateTrack, EntityId]

  /**
    * Records a Sample
    * Records a Sample
    *
    * @param trackId The Track identifier
    * @return EntityId Body Parameter  Records a Sample
    */
  def sample(trackId: UUID): ServiceCall[Sample, SignalInfo]

  /**
    * Updates track status
    * Updates track status
    *
    * @param trackId Track identifier
    * @return void Body Parameter  Track Status
    */
  def updateTrackStatus(trackId: UUID): ServiceCall[UpdateTrackStatusInfo, Done]

  def signalEventsTopic(): Topic[SignalEvent]


}

case class SignalEvent(trackId: UUID, inboundChannelId: Option[UUID], outboundChannelId: Option[UUID], signalId: UUID,
                       created: LocalDateTime, timestamp: LocalDateTime, start: LocalDateTime,
                       lastTimestamp: LocalDateTime, position: Long, sequence: Long, version: Long, stable:Boolean,
                       data: Map[String, String])

object SignalEvent {
  implicit val format: Format[SignalEvent] = Json.format[SignalEvent]

}