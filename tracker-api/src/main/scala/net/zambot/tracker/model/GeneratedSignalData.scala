package net.zambot.tracker.model

import play.api.libs.json._

case class GeneratedSignalData(values: Map[String, String])

object GeneratedSignalData {
  implicit val format: Format[GeneratedSignalData] = Json.format
}

