package net.zambot.tracker.model

import play.api.libs.json._

case class GeneratedSignal(created: String, timestamp: String, start: String, lastTimestamp: String, position: Long, sequence: Long, version: Long, data: GeneratedSignalData)

object GeneratedSignal {
    implicit val format: Format[GeneratedSignal] = Json.format

}
