package net.zambot.controller.impl

import java.time.LocalDateTime

import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Keep, Sink, Source}
import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server._
import com.softwaremill.macwire._
import net.zambot.advisor.api.AdvisorService
import net.zambot.controller.api.ControllerService
import net.zambot.controller.impl.dsl.ZFlowDSL.ZTick
import net.zambot.controller.impl.dsl._
import net.zambot.exchange.external.bittrex.api.v1.{BittrexAuthenticatedService, BittrexPublicService}
import net.zambot.exchange.external.bittrex.api.v3.{BittrexSocketClient, BittrexSymbols}
import net.zambot.scheduler.api.SchedulerService
import net.zambot.scheduler.model._
import net.zambot.tracker.api.TrackerService
import org.slf4j.LoggerFactory
import play.api.libs.json.Json
import play.api.libs.ws.ahc.AhcWSComponents

import scala.collection.immutable.Seq
import scala.concurrent.ExecutionContext
import scala.language.postfixOps


class ControllerLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new ControllerApplication(context) {
      override def serviceLocator: ServiceLocator = NoServiceLocator

    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new ControllerApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[ControllerService])

}

abstract class ControllerApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents
    with CassandraPersistenceComponents
    with LagomKafkaComponents {

  private val log = LoggerFactory.getLogger("ControllerApplication")


  implicit lazy val bittrexAuthenticatedService = serviceClient.implement[BittrexAuthenticatedService]
  implicit lazy val bittrexPublicService = serviceClient.implement[BittrexPublicService]

  implicit lazy val schedulerService = serviceClient.implement[SchedulerService]
  implicit lazy val trackerService = serviceClient.implement[TrackerService]
  implicit lazy val advisorService = serviceClient.implement[AdvisorService]

  override lazy val lagomServer: LagomServer = serverFor[ControllerService](wire[ControllerServiceImpl])

  implicit lazy val dispatcher: ExecutionContext = actorSystem.dispatcher

  import java.util.Base64
  import java.util.zip.Inflater

  def decodeMessage(encodedData: String): String = {
    val compressedData = Base64
      .getDecoder
      .decode(encodedData)
    val inflater = new Inflater(true)
    inflater
      .setInput(compressedData)
    var buffer = new Array[Byte](1024)
    val resultBuilder = new StringBuilder
    while ( {
      inflater
        .inflate(buffer) > 0
    }) {
      resultBuilder.append(new String(buffer, "UTF-8"))
      buffer = new Array[Byte](1024)
    }
    inflater
      .end
    resultBuilder
      .toString
      .trim
  }

  def printSocketMessage(msgType: String, compressedData: String): Unit = {
    try {
      println(decodeMessage(compressedData))
    } catch {
      case e: Exception =>
        println(s"Error decompressing message: ${e.toString}  data: [${compressedData}]")
    }
  }


  implicit def createTask(schedulerTask: ZSchedulerTask): CreateTask = schedulerTask.decisionAction.code match {
    case "place_order" => createPlaceOrderTask(schedulerTask)
    case "cancel_order" => createCancelOrderTask(schedulerTask)
    case "get_order_info" => createGetOrderInfoTask(schedulerTask)
  }

  def createPlaceOrderTask(schedulerTask: ZSchedulerTask): CreateTask = CreateTask(
    externalId = None,
    taskType = schedulerTask.decisionAction.code,
    schedulePolicy = "oneTime",
    parameters = TaskParameters(
      Map(
        "marketSymbol" -> schedulerTask.decisionAction.data("marketSymbol"),
        "direction" -> schedulerTask.decisionAction.value,
        "type" -> schedulerTask.decisionAction.data("type"),
        "quantity" -> schedulerTask.decisionAction.data("quantity"),
        "limit" -> schedulerTask.decisionAction.data("limit"),
        "timeInForce" -> schedulerTask.decisionAction.data("timeInForce"),
        "useAwards" -> "false"
      )
    )
  )

  def createCancelOrderTask(schedulerTask: ZSchedulerTask): CreateTask = CreateTask(
    externalId = None,
    taskType = schedulerTask.decisionAction.code,
    schedulePolicy = "oneTime",
    parameters = TaskParameters(
      Map(
        "orderId" -> schedulerTask.decisionAction.data("orderId")
      )
    )
  )

  def createGetOrderInfoTask(schedulerTask: ZSchedulerTask): CreateTask = CreateTask(
    externalId = None,
    taskType = schedulerTask.decisionAction.code,
    schedulePolicy = "oneTime",
    parameters = TaskParameters(
      Map(
        "orderId" -> schedulerTask.decisionAction.data("orderId")
      )
    )
  )

  val bufferSize = 1000
  val (queue, mat) = Source
    .queue[ZTick](bufferSize, OverflowStrategy.backpressure)
    .via(ZFlowDSL.findTickScheduler)
    .via(ZFlowDSL.progressPrinter[ZCompletedTaskFlow]("+"))
    .via(ZFlowDSL.askAdvice)
    .via(ZFlowDSL.progressPrinter[ZAgentFact]("."))
    .via(ZFlowDSL.advise)
    .via(ZFlowDSL.progressPrinter[ZDecisionAction]("*"))
    .via(ZFlowDSL.adviceActionToSchedulers)
    .via(ZFlowDSL.progressPrinter[ZSchedulerTask]("%"))
    .via(ZFlowDSL.scheduleTask)
    .via(ZFlowDSL.progressPrinter[ZTaskId]("!"))
    .toMat(Sink.ignore)(Keep.both)
    .run()

  //  Source
  //    .tick(0 seconds, 5 seconds, "task")
  //    .via(allSchedulers)
  //    .via(assignTask)
  //    .via(completeTask)
  //    .via(printer[ZCompletedTaskFlow])
  //    .via(askAdvice)
  //    .via(progressPrinter[ZAgentFact]("."))
  //    .via(advise)
  //    .via(progressPrinter[ZDecisionAction]("*"))
  //    .via(adviceActionToSchedulers)
  //    .via(progressPrinter[ZSchedulerTask]("%"))
  //    .via(scheduleTask(createTask))
  //    .via(progressPrinter[ZTaskId]("!"))
  //    .runWith(Sink.ignore)

  val msgHandler = new Object() {

    def heartbeat {
      System.out.println("<heartbeat>")
    }

    def trade(compressedData: String) = {
      printSocketMessage("Trade", compressedData)
    }

    def ticker(compressedData: String) = {
      val tick = Json.parse(decodeMessage(compressedData))
      queue.offer(
        ZTick(
          timestamp = LocalDateTime.now,
          symbol = (tick \ "symbol").as[String],
          ask = (tick \ "askRate").as[String].toDouble,
          bid = (tick \ "bidRate").as[String].toDouble,
          last = (tick \ "lastTradeRate").as[String].toDouble
        )
      )
    }

    def balance(compressedData: String) = {
      printSocketMessage("Balance", compressedData)
    }
  }

  val client = new BittrexSocketClient("https://socket-v3.bittrex.com/signalr")
    .connect
    .setMessageHandler(msgHandler)
    .subscribe(BittrexSymbols
      .lookupTable
      .keySet
      .map(key => s"ticker_${key}")
      .toArray)

  println("Zambot/Controller (MVP1) is running now.")

  override lazy val jsonSerializerRegistry: JsonSerializerRegistry =
    ControllerSerializerRegistry

}

object ControllerSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq()
}

