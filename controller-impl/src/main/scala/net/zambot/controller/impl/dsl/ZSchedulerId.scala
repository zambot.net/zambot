package net.zambot.controller.impl.dsl

import java.util.UUID

case class ZSchedulerId(value: UUID)
