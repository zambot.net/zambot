package net.zambot.controller.impl.dsl

import java.time.LocalDateTime
import java.util.UUID

import net.zambot.scheduler.model.TaskParameters

case class ZAssignedTask(taskId: UUID,
                         schedulerId: UUID,
                         externalId: Option[UUID],
                         inboundChannelId: Option[UUID],
                         outboundChannelId: Option[UUID],
                         created: LocalDateTime,
                         timestamp: LocalDateTime,
                         status: String,
                         taskType: String,
                         schedulePolicy: String,
                         executionCount: Long,
                         parameters: TaskParameters)
