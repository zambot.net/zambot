package net.zambot.controller.impl.dsl

import java.util.UUID

case class ZSchedulerTask (schedulerId: UUID, externalId: Option[UUID], decisionAction: ZDecisionAction)
