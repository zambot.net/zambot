package net.zambot.controller.impl.dsl

import java.time.LocalDateTime
import java.util.UUID

case class ZCompletedTaskFlow(typeTag: ZFlow,
                              outboundChannelId: UUID,
                              schedulerId: UUID,
                              taskId: UUID,
                              taskType: String,
                              schedulerPolicy: String,
                              when: LocalDateTime,
                              success: Boolean,
                              message: String,
                              values: Map[String, String])


trait ZFlow

case class ZSampleFlow() extends ZFlow

object ZSampleFlow {
  val typeTag = ZSampleFlow()
}

case class ZAdviceFlow() extends ZFlow

object ZAdviceFlow {
  val typeTag = ZAdviceFlow()
}

case class ZIgnoreFlow() extends ZFlow

object ZIgnoreFlow {
  val typeTag = ZIgnoreFlow()
}


