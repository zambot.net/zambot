package net.zambot.controller.impl.dsl

import java.util.UUID

case class ZAgentFact(agentId:UUID, fact:ZFact)
