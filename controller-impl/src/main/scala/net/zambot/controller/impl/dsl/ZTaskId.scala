package net.zambot.controller.impl.dsl

import java.util.UUID

case class ZTaskId(schedulerId: UUID, value: UUID)
