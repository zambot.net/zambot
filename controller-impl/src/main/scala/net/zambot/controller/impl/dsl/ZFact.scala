package net.zambot.controller.impl.dsl

import java.time.LocalDateTime
import java.util.UUID

case class ZFact(timestamp: LocalDateTime, spec:String, outboundChannelId: UUID, data: Map[String, String])
