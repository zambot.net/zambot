package net.zambot.controller.impl.dsl

import java.time.LocalDateTime
import java.util.UUID

import akka.NotUsed
import akka.stream.ActorAttributes.supervisionStrategy
import akka.stream.FlowShape
import akka.stream.Supervision.resumingDecider
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Merge}
import net.zambot.advisor.api.AdvisorService
import net.zambot.advisor.model.Fact
import net.zambot.exchange.external.bittrex.api.v1.BittrexPublicService
import net.zambot.exchange.external.bittrex.api.v3.BittrexSymbols
import net.zambot.scheduler.api.SchedulerService
import net.zambot.scheduler.model.{CompleteTaskInfo, CreateTask, TaskInfoResult, TaskParameters, TaskRunner}
import net.zambot.tracker.api.TrackerService
import net.zambot.tracker.model.Sample

import scala.collection.immutable.{HashMap, ListMap}
import scala.concurrent.{ExecutionContext, Future}

object ZFlowDSL {

  def progressPrinter[T](symbol: String): Flow[T, T, NotUsed] =
    Flow[T].map(e => {
      print(symbol);
      e
    })

  def printer[T]: Flow[T, T, NotUsed] =
    Flow[T].map(e => {
      println(e);
      e
    })

  val parallelism = 2

  case class ZTask(symbol: String, schedulerInboundChannelId: UUID, externalTaskId: UUID)

  case class ZTick(timestamp: LocalDateTime, symbol: String, ask: Double, bid: Double, last: Double)

  def findTickScheduler(implicit executionContext: ExecutionContext, schedulerService: SchedulerService): Flow[ZTick, ZCompletedTaskFlow, NotUsed] =
    Flow[ZTick]
      .mapAsyncUnordered(parallelism)(tick => {
        schedulerService
          .findScheduler(inboundChannelId =
            Some(BittrexSymbols
              .lookupTable(tick.symbol)
              .schedulerInboundChannelId)
          )
          .invoke
          .map(schedulerList =>
            schedulerList
              .values
              .map(schedulerInfo => (tick, schedulerInfo))
          )
      }
      )
      .withAttributes(supervisionStrategy(resumingDecider))
      .mapConcat(element => element)
      .mapAsyncUnordered(parallelism)(streamTuple => {
        val tick = streamTuple._1
        val schedulerInfo = streamTuple._2
        schedulerService
          .findTask(schedulerId = schedulerInfo.schedulerId, externalId = Some(BittrexSymbols.lookupTable(tick.symbol).externalTaskId))
          .invoke
          .map(taskInfoList =>
            taskInfoList
              .values
              .map(taskInfo =>
                (tick, schedulerInfo, taskInfo)
              )
          )
      })
      .withAttributes(supervisionStrategy(resumingDecider))
      .mapConcat(streamTuple =>
        streamTuple
          .map(streamTuple => {
            ZCompletedTaskFlow(
              typeTag = ZSampleFlow.typeTag,
              outboundChannelId = streamTuple._3.externalId.get,
              schedulerId = streamTuple._2.schedulerId,
              taskId = streamTuple._3.taskId,
              taskType = streamTuple._3.taskType,
              schedulerPolicy = streamTuple._3.schedulePolicy,
              when = streamTuple._1.timestamp,
              success = true,
              message = "ok",
              values = HashMap(
                "last" -> streamTuple._1.last.toString,
                "ask" -> streamTuple._1.ask.toString,
                "bid" -> streamTuple._1.bid.toString,
                "exchange" -> "bittrex",
                "symbol" -> streamTuple._1.symbol)
            )

          }
          )
      )
      .withAttributes(supervisionStrategy(resumingDecider))

  def taskToTrack(implicit executionContext: ExecutionContext, trackerService: TrackerService): Flow[ZCompletedTaskFlow, ZTrackerSample, NotUsed] =
    Flow[ZCompletedTaskFlow]
      .filter(flowRoute => flowRoute.typeTag match {
        case ZSampleFlow.typeTag => true
        case _ => false
      })
      .mapAsyncUnordered(parallelism)(completedTask =>
        trackerService
          .findTrack(inboundChannelId = Some(completedTask.outboundChannelId),
            outboundChannelId = None,
            pageSize = Some(1000)
          ).invoke
          .map(trackInfoList =>
            trackInfoList.values.map(track =>
              ZTrackerSample(
                trackId = track.trackId,
                completedTask = completedTask)
            )
          )
      )
      .withAttributes(supervisionStrategy(resumingDecider))
      .mapConcat(e => e)

  def sample(implicit executionContext: ExecutionContext, trackerService: TrackerService): Flow[ZTrackerSample, ZFact, NotUsed] =
    Flow[ZTrackerSample]
      .mapAsyncUnordered(parallelism)(trackerSample =>
        trackerService
          .sample(trackerSample.trackId)
          .invoke(
            Sample(
              timestamp = trackerSample.completedTask.when,
              data = trackerSample.completedTask.values)
          )
          .map(signalInfo =>
            ZFact(
              timestamp = signalInfo.timestamp,
              outboundChannelId = UUID.fromString(signalInfo.outboundChannelId.get),
              spec = trackerSample.completedTask.taskType,
              data = signalInfo.data
            )
          )
      )
      .withAttributes(supervisionStrategy(resumingDecider))

  def sampleToAgent(implicit executionContext: ExecutionContext, advisorService: AdvisorService): Flow[ZFact, ZAgentFact, NotUsed] =
    Flow[ZFact]
      .mapAsyncUnordered(parallelism)(signal => {
        advisorService
          .findAgent(inboundChannelId = Some(signal.outboundChannelId), outboundChannelId = None, pageSize = Some(1000))
          .invoke
          .map(agentInfoList => {
            agentInfoList
              .values
              .map(agentInfo => {
                ZAgentFact(
                  agentId = agentInfo.agentId,
                  fact = signal)
              }
              )
          }
          )
      }
      )
      .withAttributes(supervisionStrategy(resumingDecider))
      .mapConcat(e => e)

  def advise(implicit executionContext: ExecutionContext, advisorService: AdvisorService): Flow[ZAgentFact, ZDecisionAction, NotUsed] =
    Flow[ZAgentFact]
      .mapAsyncUnordered(parallelism)(agentFact => {
        advisorService
          .advise(agentFact.agentId)
          .invoke(
            Fact(timestamp = agentFact.fact.timestamp,
              code = agentFact.fact.spec,
              data = agentFact.fact.data)
          )
          .map(advice =>
            advice.actions.map(action =>
              ZDecisionAction(
                agentId = agentFact.agentId,
                adviceId = advice.adviceId,
                outboundChannelId = advice.outboundChannelId.get,
                code = action.code,
                value = action.value,
                data = action.data
              )
            )
          )
      })
      .withAttributes(supervisionStrategy(resumingDecider))
      .mapConcat(e => e)

  def adviceActionToSchedulers(implicit executionContext: ExecutionContext, schedulerService: SchedulerService): Flow[ZDecisionAction, ZSchedulerTask, NotUsed] =
    Flow[ZDecisionAction]
      .mapAsyncUnordered(parallelism)(decisionAction =>
        schedulerService
          .findScheduler(pageSize = Some(1000),
            inboundChannelId = Some(decisionAction.outboundChannelId))
          .invoke
          .map(schedulerInfoList =>
            schedulerInfoList.values.map(schedulerInfo =>
              ZSchedulerTask(
                schedulerId = schedulerInfo.schedulerId,
                externalId = Some(decisionAction.agentId),
                decisionAction = decisionAction
              )
            )
          )
      )
      .withAttributes(supervisionStrategy(resumingDecider))
      .mapConcat(e => e)

  def scheduleTask(implicit executionContext: ExecutionContext, schedulerService: SchedulerService, createTask: ZSchedulerTask => CreateTask): Flow[ZSchedulerTask, ZTaskId, NotUsed] =
    Flow[ZSchedulerTask]
      .mapAsyncUnordered(parallelism)(schedulerTask =>
        schedulerService
          .createTask(schedulerTask.schedulerId)
          .invoke(createTask(schedulerTask))
          .map(taskId =>
            ZTaskId(
              schedulerId = schedulerTask.schedulerId,
              value = taskId.value
            )
          )
      )
      .withAttributes(supervisionStrategy(resumingDecider))


  def runGetOrderInfoBittrex(implicit executionContext: ExecutionContext, assignedTask: ZAssignedTask): Future[ZCompletedTaskFlow] = Future {
    ZCompletedTaskFlow(
      typeTag = ZAdviceFlow.typeTag,
      outboundChannelId = UUID.randomUUID,
      schedulerId = assignedTask.schedulerId,
      taskId = assignedTask.taskId,
      taskType = assignedTask.taskType,
      schedulerPolicy = assignedTask.schedulePolicy,
      when = LocalDateTime.now,
      success = false,
      message = "Not Implemented",
      values = Map.empty
    )

  }


  def runPlaceOrderBittrex(implicit executionContext: ExecutionContext, assignedTask: ZAssignedTask): Future[ZCompletedTaskFlow] = Future {
    ZCompletedTaskFlow(
      typeTag = ZAdviceFlow.typeTag,
      outboundChannelId = UUID.randomUUID,
      schedulerId = assignedTask.schedulerId,
      taskId = assignedTask.taskId,
      taskType = assignedTask.taskType,
      schedulerPolicy = assignedTask.schedulePolicy,
      when = LocalDateTime.now,
      success = false,
      message = "Not Implemented",
      values = Map.empty
    )

  }

  def runPlaceOrder(implicit executionContext: ExecutionContext, bittrexPublicService: BittrexPublicService, assignedTask: ZAssignedTask): Future[ZCompletedTaskFlow] = assignedTask.parameters.values.get("exchange") match {
    case Some("bittrex") => runPlaceOrderBittrex
    case _ => runPlaceOrderBittrex

  }

  def runTickBittrex[T <: ZFlow](implicit executionContext: ExecutionContext, bittrexPublicService: BittrexPublicService, assignedTask: ZAssignedTask): Future[ZCompletedTaskFlow] = {
    val symbol = assignedTask.parameters.values("symbol")
    bittrexPublicService
      .getTicker(symbol)
      .invoke
      .map(getTickerResult =>
        ZCompletedTaskFlow(
          typeTag = ZSampleFlow.typeTag,
          outboundChannelId = assignedTask.externalId.get,
          schedulerId = assignedTask.schedulerId,
          taskId = assignedTask.taskId,
          taskType = assignedTask.taskType,
          schedulerPolicy = assignedTask.schedulePolicy,
          when = LocalDateTime.now,
          message = "ok",
          success = true,
          values = ListMap("symbol" -> symbol,
            "ask" -> getTickerResult.result.get.Ask.toString,
            "bid" -> getTickerResult.result.get.Bid.toString,
            "last" -> getTickerResult.result.get.Last.toString,
            "time" -> LocalDateTime.now.toString)
        )
      )
      .recover { error =>
        println(s"Error : ${error}")
        ZCompletedTaskFlow(
          typeTag = ZIgnoreFlow.typeTag,
          outboundChannelId = assignedTask.externalId.get,
          schedulerId = assignedTask.schedulerId,
          taskId = assignedTask.taskId,
          taskType = assignedTask.taskType,
          schedulerPolicy = assignedTask.schedulePolicy,
          when = LocalDateTime.now,
          message = s"${error}",
          success = false,
          values = Map.empty)
      }
  }

  def runCancelOrderBittrex(implicit executionContext: ExecutionContext, assignedTask: ZAssignedTask): Future[ZCompletedTaskFlow] = Future {
    ZCompletedTaskFlow(
      typeTag = ZAdviceFlow.typeTag,
      outboundChannelId = UUID.randomUUID,
      schedulerId = assignedTask.schedulerId,
      taskId = assignedTask.taskId,
      taskType = assignedTask.taskType,
      schedulerPolicy = assignedTask.schedulePolicy,
      when = LocalDateTime.now,
      success = false,
      message = "Not Implemented",
      values = Map.empty
    )
  }

  def runTick[T <: ZFlow](implicit executionContext: ExecutionContext, bittrexPublicService: BittrexPublicService, assignedTask: ZAssignedTask): Future[ZCompletedTaskFlow] = assignedTask.parameters.values.get("exchange") match {
    case Some("bittrex") => runTickBittrex[T]
    case _ => runTickBittrex[T]

  }

  def runCancelOrder(implicit executionContext: ExecutionContext, assignedTask: ZAssignedTask): Future[ZCompletedTaskFlow] = assignedTask.parameters.values("exchange") match {
    case "bittrex" => runCancelOrderBittrex
    case _ => runCancelOrderBittrex

  }

  def runGetOrderInfo(implicit executionContext: ExecutionContext, assignedTask: ZAssignedTask): Future[ZCompletedTaskFlow] = assignedTask.parameters.values("exchange") match {
    case "bittrex" => runGetOrderInfoBittrex
    case _ => runGetOrderInfoBittrex

  }

  def runError(implicit executionContext: ExecutionContext, assignedTask: ZAssignedTask): Future[ZCompletedTaskFlow] = Future {
    ZCompletedTaskFlow(
      typeTag = ZAdviceFlow.typeTag,
      outboundChannelId = UUID.randomUUID,
      schedulerId = assignedTask.schedulerId,
      taskId = assignedTask.taskId,
      taskType = assignedTask.taskType,
      schedulerPolicy = assignedTask.schedulePolicy,
      when = LocalDateTime.now,
      success = false,
      message = "Not Implemented",
      values = Map.empty
    )

  }

  def runTask(implicit executionContext: ExecutionContext, bittrexPublicService: BittrexPublicService, assignedTask: ZAssignedTask): Future[ZCompletedTaskFlow] = assignedTask.taskType match {
    case "place_order" => runPlaceOrder
    case "tick" => runTick
    case "cancel_order" => runCancelOrder
    case "get_order_info" => runGetOrderInfo
    case _ => runError

  }

  def completeTask(implicit executionContext: ExecutionContext, bittrexPublicService: BittrexPublicService, schedulerService: SchedulerService): Flow[ZAssignedTask, ZCompletedTaskFlow, NotUsed] =
    Flow[ZAssignedTask]
      .mapAsyncUnordered(parallelism)(assignedTask =>
        runTask(executionContext, bittrexPublicService, assignedTask)
      )
      .withAttributes(supervisionStrategy(resumingDecider))
      .mapAsyncUnordered(parallelism)(completedTask =>
        schedulerService.completeTask(completedTask.schedulerId, completedTask.taskId)
          .invoke(
            CompleteTaskInfo(
              result = TaskInfoResult(
                schedulerId = completedTask.schedulerId,
                taskId = completedTask.taskId,
                when = completedTask.when,
                success = completedTask.success,
                message = completedTask.message,
                values = completedTask.values
              )
            )
          )
          .map(complete => completedTask)
      )
      .withAttributes(supervisionStrategy(resumingDecider))

  def allSchedulers(implicit executionContext: ExecutionContext, schedulerService: SchedulerService): Flow[String, ZSchedulerId, NotUsed] =
    Flow[String]
      .mapAsyncUnordered(1)(s =>
        schedulerService.findScheduler(None, None, Some(1000))
          .invoke
          .map(schedulerInfoList =>
            schedulerInfoList
              .values
              .map(schedulerInfo => ZSchedulerId(schedulerInfo.schedulerId))
          )
      )
      .withAttributes(supervisionStrategy(resumingDecider))
      .mapConcat(schedulerInfo => schedulerInfo)

  def assignTask(implicit executionContext: ExecutionContext, schedulerService: SchedulerService): Flow[ZSchedulerId, ZAssignedTask, NotUsed] =
    Flow[ZSchedulerId]
      .mapAsyncUnordered(parallelism)(id =>
        schedulerService.assignTask(id.value)
          .invoke(TaskRunner(externalId = UUID.randomUUID))
      )
      .withAttributes(supervisionStrategy(resumingDecider))
      .filter(assignedTask => "true".equals(assignedTask.success))
      .map(assignedTask => assignedTask.result.get)
      .map(result =>
        ZAssignedTask(
          taskId = result.taskId,
          externalId = result.externalId,
          schedulerId = result.schedulerId,
          inboundChannelId = result.inboundChannelId,
          outboundChannelId = result.outboundChannelId,
          created = result.created,
          timestamp = result.timestamp,
          status = result.status,
          taskType = result.taskType,
          schedulePolicy = result.schedulePolicy,
          executionCount = result.executionCount,
          parameters = result.parameters
        )
      )

  def taskToAgent(implicit executionContext: ExecutionContext, advisorService: AdvisorService): Flow[ZCompletedTaskFlow, ZAgentFact, NotUsed] =
    Flow[ZCompletedTaskFlow]
      .filter(flowRoute => flowRoute.typeTag match {
        //        case ZCompletedTaskFlow(ZAdviceFlow.typeTag, _, _, _, _, _, _, _, _, _) => true
        case _ => false
      })
      .mapAsyncUnordered(parallelism)(completedTask =>
        advisorService
          .findAgent(inboundChannelId = Some(completedTask.outboundChannelId), outboundChannelId = None, pageSize = Some(1000))
          .invoke
          .map(agentInfoList =>
            agentInfoList
              .values
              .map(agentInfo =>
                ZAgentFact(
                  agentId = agentInfo.agentId,
                  fact = ZFact(
                    timestamp = completedTask.when,
                    outboundChannelId = completedTask.outboundChannelId,
                    spec = completedTask.taskType,
                    data = completedTask.values
                  )
                )
              )
          )
      ).mapConcat(e => e)


  def askAdvice(implicit executionContext: ExecutionContext, trackerService: TrackerService, advisorService: AdvisorService) = GraphDSL.create() { implicit b =>
    import GraphDSL.Implicits._

    val broadcast = b.add(Broadcast[ZCompletedTaskFlow](2))
    val merge = b.add(Merge[ZAgentFact](2))

    broadcast.out(0) ~>
      taskToTrack ~> progressPrinter[ZTrackerSample]("_") ~>
      sample ~> progressPrinter[ZFact]("-") ~>
      sampleToAgent ~> merge.in(0)

    broadcast.out(1) ~> taskToAgent ~> merge.in(1)

    FlowShape(broadcast.in, merge.out)

  }

}
