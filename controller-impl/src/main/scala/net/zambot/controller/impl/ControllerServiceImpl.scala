package net.zambot.controller.impl

import java.time.{Duration, LocalDateTime}
import java.util.UUID

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import net.zambot.advisor.api.AdvisorService
import net.zambot.advisor.model.{AgentParameters, CreateAgent}
import net.zambot.controller.api.ControllerService
import net.zambot.scheduler.api.SchedulerService
import net.zambot.scheduler.model.{CreateScheduler, CreateTask, SchedulerParameters, TaskParameters}
import net.zambot.tracker.api.TrackerService
import net.zambot.tracker.model.{CreateTrack, TrackInfoList, TrackParameters}
import org.slf4j.LoggerFactory
import play.api.libs.json.{JsValue, Json}

import scala.collection.immutable.{HashMap, Seq}
import scala.concurrent.{ExecutionContext, Future}

/**
  * Implementation of the SchedulerService.
  */
class ControllerServiceImpl()(implicit ec: ExecutionContext,
                              materializer: akka.stream.Materializer,
                              schedulerService: SchedulerService,
                              trackerService: TrackerService,
                              advisorService: AdvisorService) extends ControllerService {

  private val log = LoggerFactory.getLogger("ControllerServiceImpl")

  private def createScheduler(maxRecurrentTaskExecutions: Int,
                              maxOneTimeTaskExecutions: Int,
                              interval: String,
                              exchange: String,
                              inboundChannelId: UUID): Future[UUID] = {
    schedulerService
      .createScheduler
      .invoke(
        CreateScheduler(
          parameters = SchedulerParameters(values = HashMap(
            "exchange" -> exchange,
          )),
          maxRecurrentTaskExecutions = maxRecurrentTaskExecutions,
          maxOneTimeTaskExecutions = maxOneTimeTaskExecutions,
          interval = interval,
          inboundChannelId = Some(inboundChannelId),
          outboundChannelId = None))
      .map(id => id.value)

  }


  private def createTask(schedulerId: UUID,
                         externalId: UUID,
                         exchange: String,
                         symbol: String,
                         taskType: String,
                         schedulerPolicy: String): Future[UUID] = {
    schedulerService
      .createTask(schedulerId)
      .invoke(CreateTask(
        externalId = Some(externalId),
        taskType = taskType,
        schedulePolicy = schedulerPolicy,
        parameters = TaskParameters(values =
          HashMap(
            "exchange" -> exchange,
            "symbol" -> symbol,
          )
        )
      ))
      .map(id => id.value)
  }

  private def createTrack(inboundChannelId: UUID,
                          outboundChannelId: UUID,
                          interval: Duration,
                          start: LocalDateTime,
                          symbol: String,
                          MACD_signal_ema: Double,
                          low_ema: Double,
                          high_ema: Double
                         ): Future[UUID] = {
    trackerService
      .initializeTrack
      .invoke(
        CreateTrack(inboundChannelId = Some(inboundChannelId),
          outboundChannelId = Some(outboundChannelId),
          interval = interval.toString,
          start = start.toString,
          parameters = TrackParameters(
            values = HashMap(
              "symbol" -> symbol,
              "MACD_signal_ema" -> MACD_signal_ema.toString,
              "low_ema" -> low_ema.toString,
              "high_ema" -> high_ema.toString,

            )
          )
        )
      )
      .map(id => id.entityId)

  }

  private def createAgent(inboundChannelId: UUID,
                          outboundChannelId: UUID,
                          symbol: String): Future[UUID] = {

    advisorService
      .initializeAgent
      .invoke(
        CreateAgent(
          inboundChannelId = Some(inboundChannelId),
          outboundChannelId = Some(outboundChannelId),
          parameters = AgentParameters(values =
            HashMap(
              "symbol" -> symbol
            )
          )
        )
      )
      .map(id => id.entityId)
  }

  override def setup: ServiceCall[NotUsed, String] = { _ =>
    val config = scala.io.Source.fromResource("zconfig.json")
    val configJson = Json.parse(config.getLines.mkString)
    println(configJson)

    (configJson \ "schedulers")
      .as[Seq[JsValue]]
      .map(scheduler => {
        createScheduler(maxRecurrentTaskExecutions = (scheduler \ "maxRecurrentTaskExecutions").as[Int],
          maxOneTimeTaskExecutions = (scheduler \ "maxOneTimeTaskExecutions").as[Int],
          interval = (scheduler \ "interval").as[Duration].toString,
          exchange = (scheduler \ "exchange").as[String],
          inboundChannelId = (scheduler \ "inboundChannelId").as[UUID])
          .map(id => {
            (scheduler \ "tasks")
              .as[Seq[JsValue]]
              .map(task => {
                createTask(schedulerId = id,
                  externalId = (task \ "externalId").as[UUID],
                  exchange = (task \ "exchange").as[String],
                  symbol = (task \ "symbol").as[String],
                  taskType = (task \ "taskType").as[String],
                  schedulerPolicy = (task \ "schedulerPolicy").as[String]
                ).map(taskId => {
                  (task \ "tracks")
                    .as[Seq[JsValue]]
                    .map(track => {
                      createTrack(
                        inboundChannelId = (track \ "inboundChannelId").as[UUID],
                        outboundChannelId = (track \ "outboundChannelId").as[UUID],
                        interval = (track \ "interval").as[Duration],
                        start = LocalDateTime.now,
                        symbol = (track \ "symbol").as[String],
                        MACD_signal_ema = (track \ "MACD_signal_ema").as[Double],
                        low_ema = (track \ "low_ema").as[Double],
                        high_ema = (track \ "high_ema").as[Double]
                      ).map(id => {
                        (track \ "agents")
                          .as[Seq[JsValue]]
                          .map(agent =>
                            createAgent(
                              inboundChannelId = (agent \ "inboundChannelId").as[UUID],
                              outboundChannelId = (agent \ "outboundChannelId").as[UUID],
                              symbol = (agent \ "symbol").as[String]
                            )
                          )
                      })
                    })
                })
              })
          })
      })

    Future {
      "Ok"
    }
  }

  def zstatus: ServiceCall[NotUsed, String] = { _ =>
    Future {
      "OK"
    }

  }


}