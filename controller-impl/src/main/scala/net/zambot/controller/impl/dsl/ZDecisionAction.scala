package net.zambot.controller.impl.dsl

import java.util.UUID

case class ZDecisionAction(agentId:UUID, adviceId: UUID, outboundChannelId: UUID, code:String, value:String, data:Map[String, String])
