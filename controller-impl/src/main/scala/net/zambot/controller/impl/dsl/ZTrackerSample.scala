package net.zambot.controller.impl.dsl

import java.time.LocalDateTime
import java.util.UUID

case class ZTrackerSample(trackId:UUID, completedTask: ZCompletedTaskFlow)
