organization in ThisBuild := "net.zambot"
version in ThisBuild := "1.0-SNAPSHOT"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.13.2"

resolvers ++= Seq("Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
  "Java.net Maven2 Repository" at "http://download.java.net/maven/2/")

val signalr4j = "com.github.signalr4j" % "signalr4j" % "2.0.4"

val javacpp = "org.bytedeco" % "javacpp" % "1.5.3"

val macwire = "com.softwaremill.macwire" %% "macros" % "2.3.3" % "provided"
val scalaTest = "org.scalatest" %% "scalatest" % "3.1.1" % Test

lagomUnmanagedServices in ThisBuild := Map("bittrex-public-api" -> "https://international.bittrex.com", "bittrex-authenticated-api" -> "https://international.bittrex.com")
//lagomUnmanagedServices in ThisBuild := Map("bittrex-public-api" -> "https://international.bittrex.com", "bittrex-authenticated-api" -> "https://international.bittrex.com", "cas_native" -> "http://192.168.1.2:9042")
//lagomKafkaAddress in ThisBuild := "192.168.1.2:9092"
//lagomKafkaEnabled in ThisBuild := false
//lagomCassandraEnabled in ThisBuild := false

lazy val `zambot` = (project in file("."))
  .aggregate(`scheduler-api`, `scheduler-impl`, `controller-api`, `controller-impl`, `bittrex-api`, `tracker-api`,
    `tracker-impl`, `advisor-api`, `advisor-impl`)

lazy val `scheduler-api` = (project in file("scheduler-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `scheduler-impl` = (project in file("scheduler-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest
    )
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`scheduler-api`)

lazy val `bittrex-api` = (project in file("bittrex-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      signalr4j
    )
  )


lazy val `controller-api` = (project in file("controller-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `controller-impl` = (project in file("controller-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest,
      javacpp,
      "com.lightbend.akka" %% "akka-stream-alpakka-file" % "2.0.2",
    )
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`controller-api`, `scheduler-api`, `bittrex-api`, `tracker-api`, `advisor-api`)

lazy val `tracker-api` = (project in file("tracker-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `tracker-impl` = (project in file("tracker-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest
    )
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`tracker-api`)

lazy val `advisor-api` = (project in file("advisor-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `advisor-impl` = (project in file("advisor-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest
    )
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`advisor-api`)