package net.zambot.exchange.external.bittrex.api.v1

import java.time.LocalDateTime

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import play.api.libs.json.{Format, Json}

/**
  * https://support.bittrex.com/hc/en-us/articles/115003723911-Developer-s-Guide-API
  *
  */
trait BittrexPublicService extends Service {

  def getMarkets: ServiceCall[NotUsed, GetMarketsResult]

  def getCurrencies: ServiceCall[NotUsed, GetCurrenciesResult]

  def getTicker(market: String): ServiceCall[NotUsed, GetTickerResult]

  def getMarketSummaries: ServiceCall[NotUsed, GetMarketSummariesResult]

  def getMarketSummary(market: String): ServiceCall[NotUsed, GetMarketSummaryResult]

  def getOrderBook(market: String, orderbookType: String): ServiceCall[NotUsed, GetOrderBookResult]

  def getMarketHistory(market: String): ServiceCall[NotUsed, GetMarketHistoryResult]

  override final def descriptor = {
    import Service._
    named("bittrex-public-api")
      .withCalls(
        /* public API */
        pathCall("/api/v1.1/public/getmarkets", getMarkets _),
        pathCall("/api/v1.1/public/getcurrencies", getCurrencies _),
        pathCall("/api/v1.1/public/getticker?market", getTicker _),
        pathCall("/api/v1.1/public/getmarketsummaries", getMarketSummaries _),
        pathCall("/api/v1.1/public/getmarketsummary?market", getMarketSummary _),
        pathCall("/api/v1.1/public/getorderbook?market&type", getOrderBook _),
        pathCall("/api/v1.1/public/getmarkethistory?market", getMarketHistory _),

      ).withAutoAcl(true)
  }

}

case class GetMarkets(MarketCurrency: String, BaseCurrency: String, MarketCurrencyLong: String, BaseCurrencyLong: String,
                      MinTradeSize: Double, MarketName: String, IsActive: Boolean, IsRestricted: Boolean, Created: LocalDateTime,
                      Notice: Option[String], IsSponsored: Option[Boolean], LogoUrl: Option[String])

object GetMarkets {
  implicit val format: Format[GetMarkets] = Json.format[GetMarkets]
}

case class GetMarketsResult(success: Boolean, message: String, result: Seq[GetMarkets])

object GetMarketsResult {
  implicit val format: Format[GetMarketsResult] = Json.format[GetMarketsResult]
}


case class GetCurrencies(Currency: String, CurrencyLong: String, MinConfirmation: Int, TxFee: Double, IsActive: Boolean,
                         CoinType: String, BaseAddress: Option[String])

object GetCurrencies {
  implicit val format: Format[GetCurrencies] = Json.format[GetCurrencies]
}

case class GetCurrenciesResult(success: Boolean, message: String, result: Seq[GetCurrencies])

object GetCurrenciesResult {
  implicit val format: Format[GetCurrenciesResult] = Json.format[GetCurrenciesResult]
}

case class GetTicker(Bid: Double, Ask: Double, Last: Double)

object GetTicker {
  implicit val format: Format[GetTicker] = Json.format[GetTicker]
}

case class GetTickerResult(success: Boolean, message: String, result: Option[GetTicker])

object GetTickerResult {
  implicit val format: Format[GetTickerResult] = Json.format[GetTickerResult]
}

case class GetMarketSummaries(MarketName: String, High: Double, Low: Double, Volume: Double, Last: Double, BaseVolume: Double,
                              TimeStamp: LocalDateTime, Bid: Double, Ask: Double, OpenBuyOrders: Int, OpenSellOrders: Int,
                              PrevDay: Double, Created: LocalDateTime, DisplayMarketName: Option[String])

object GetMarketSummaries {
  implicit val format: Format[GetMarketSummaries] = Json.format[GetMarketSummaries]
}

case class GetMarketSummariesResult(success: Boolean, message: String, result: Seq[GetMarketSummaries])

object GetMarketSummariesResult {
  implicit val format: Format[GetMarketSummariesResult] = Json.format[GetMarketSummariesResult]
}

case class GetMarketSummary(MarketName: String, High: Double, Low: Double, Volume: Double, Last: Double, BaseVolume: Double,
                            TimeStamp: LocalDateTime, Bid: Double, Ask: Double, OpenBuyOrders: Int, OpenSellOrders: Int,
                            PrevDay: Double, Created: LocalDateTime, DisplayMarketName: Option[String])

object GetMarketSummary {
  implicit val format: Format[GetMarketSummary] = Json.format[GetMarketSummary]
}

case class GetMarketSummaryResult(success: Boolean, message: String, result: Option[Seq[GetMarketSummary]])

object GetMarketSummaryResult {
  implicit val format: Format[GetMarketSummaryResult] = Json.format[GetMarketSummaryResult]
}


case class GetOrderBook(buy: Seq[GetBuyOrderBook], sell: Seq[GetSellOrderBook])

object GetOrderBook {
  implicit val format: Format[GetOrderBook] = Json.format[GetOrderBook]
}

case class GetSellOrderBook(Quantity: Double, Rate: Double)

object GetSellOrderBook {
  implicit val format: Format[GetSellOrderBook] = Json.format[GetSellOrderBook]
}

case class GetBuyOrderBook(Quantity: Double, Rate: Double)

object GetBuyOrderBook {
  implicit val format: Format[GetBuyOrderBook] = Json.format[GetBuyOrderBook]
}

case class GetOrderBookResult(success: Boolean, message: String, result: Option[GetOrderBook])

object GetOrderBookResult {
  implicit val format: Format[GetOrderBookResult] = Json.format[GetOrderBookResult]
}

case class GetMarketHistory(Id: Long, TimeStamp: LocalDateTime, Price: Double, Total: Double, FillType: String, OrderType: String)

object GetMarketHistory {
  implicit val format: Format[GetMarketHistory] = Json.format[GetMarketHistory]
}

case class GetMarketHistoryResult(success: Boolean, message: String, result: Seq[GetMarketHistory])

object GetMarketHistoryResult {
  implicit val format: Format[GetMarketHistoryResult] = Json.format[GetMarketHistoryResult]
}