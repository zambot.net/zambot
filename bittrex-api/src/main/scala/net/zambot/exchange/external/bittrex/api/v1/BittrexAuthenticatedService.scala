package net.zambot.exchange.external.bittrex.api.v1

import java.time.LocalDateTime

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.transport.{HeaderFilter, RequestHeader, ResponseHeader}
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import play.api.libs.json.{Format, Json}
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

trait BittrexAuthenticatedService extends Service {

  def buyLimit(apikey: String, market: String, quantity: Double, rate: Double, nonce: String): ServiceCall[NotUsed, BuyLimit]

  def sellLimit(apikey: String, market: String, quantity: Double, rate: Double, nonce: String): ServiceCall[NotUsed, SellLimit]

  def cancel(apikey: String, uuid: String, nonce: String): ServiceCall[NotUsed, Cancel]

  def getOpenOrders(apikey: String, market: String, nonce: String): ServiceCall[NotUsed, GetOpenOrders]

  def getBalances(apikey: String, nonce: String): ServiceCall[NotUsed, GetBalancesResult]

  def getBalance(apikey: String, currency: String, nonce: String): ServiceCall[NotUsed, GetBalanceResult]

  def getDepositAddress(apikey: String, currency: String, nonce: String): ServiceCall[NotUsed, GetDepositAddress]

  def withdraw(apikey: String, currency: String, quantity: Double, address: String, paymentid: String, nonce: String): ServiceCall[NotUsed, Withdraw]

  def getOrder(apikey: String, uuid: String, nonce: String): ServiceCall[NotUsed, GetOrderResult]

  def getOrderHistory(apikey: String, market: Option[String], nonce: String): ServiceCall[NotUsed, GetOrderHistoryResult]

  def getWithdrawHistory(apikey: String, currency: Option[String], nonce: String): ServiceCall[NotUsed, GetWithdrawHistoryResult]

  def getDepositHistory(apikey: String, currency: Option[String], nonce: String): ServiceCall[NotUsed, GetDepositHistoryResult]

  override final def descriptor = {
    import Service._
    named("bittrex-authenticated-api")
      .withCalls(

        /* market API */
        pathCall("/api/v1.1/market/buylimit?apikey&market&quantity&rate&nonce", buyLimit _),
        pathCall("/api/v1.1/market/selllimit?apikey&market&quantity&rate&nonce", sellLimit _),
        pathCall("/api/v1.1/market/cancel?apikey&uuid&nonce", cancel _),
        pathCall("/api/v1.1/market/getopenorders?apikey&market&nonce", getOpenOrders _),

        /* account API */
        pathCall("/api/v1.1/account/getbalances?apikey&nonce", getBalances _),
        pathCall("/api/v1.1/account/getbalance?apikey&currency&nonce", getBalance _),
        pathCall("/api/v1.1/account/getdepositaddress?apikey&currency&nonce", getDepositAddress _),
        pathCall("/api/v1.1/account/withdraw?apikey&currency&quantity&address&paymentid&nonce", withdraw _),
        pathCall("/api/v1.1/account/getorder?apikey&uuid&nonce", getOrder _),
        pathCall("/api/v1.1/account/getorderhistory?apikey&market&nonce", getOrderHistory _),
        pathCall("/api/v1.1/account/getwithdrawalhistory?apikey&currency&nonce", getWithdrawHistory _),
        pathCall("/api/v1.1/account/getdeposithistory?apikey&currency&nonce", getDepositHistory _)

      ).withAutoAcl(true).withHeaderFilter(BittrexAuthenticationHeader)
  }

  object BittrexAuthenticationHeader extends HeaderFilter {
    override def transformClientRequest(requestHeader: RequestHeader): RequestHeader = {
      val apisign = hash_hmac("38ba8e07d25d48b49201f54c962d27a5", requestHeader.uri.toString)
      requestHeader.addHeader("apisign", apisign)

    }

    final val hexCode: Array[Char] = "0123456789ABCDEF".toCharArray

    def hash_hmac(secretKey: String, message: String): String = {
      val algorithm = "HmacSHA512"
      val mac = Mac.getInstance(algorithm)

      mac.init(new SecretKeySpec(secretKey.getBytes("UTF-8"), algorithm))

      val mac_data: Array[Byte] = mac.doFinal(message.getBytes("UTF-8"))
      val hash = new StringBuilder(mac_data.length * 2)
      for (b <- mac_data) {
        hash.append(hexCode((b >> 4) & 0xF))
        hash.append(hexCode(b & 0xF))
      }
      hash.toString

    }

    override def transformServerRequest(request: RequestHeader): RequestHeader = request

    override def transformServerResponse(response: ResponseHeader, request: RequestHeader): ResponseHeader = response

    override def transformClientResponse(responseHeader: ResponseHeader, request: RequestHeader): ResponseHeader = responseHeader
  }

}

case class BuyLimitResult(uuid: String)

object BuyLimitResult {
  implicit val format: Format[BuyLimitResult] = Json.format[BuyLimitResult]
}

case class BuyLimit(success: Boolean, message: String, result: Option[BuyLimitResult])

object BuyLimit {
  implicit val format: Format[BuyLimit] = Json.format[BuyLimit]
}

case class SellLimitResult(uuid: String)

object SellLimitResult {
  implicit val format: Format[SellLimitResult] = Json.format[SellLimitResult]
}

case class SellLimit(success: Boolean, message: String, result: Option[SellLimitResult])

object SellLimit {
  implicit val format: Format[SellLimit] = Json.format[SellLimit]
}

case class Cancel(success: Boolean, message: String, result: Option[String])

object Cancel {
  implicit val format: Format[Cancel] = Json.format[Cancel]
}

case class GetOpenOrdersResult(Uuid: String, OrderUuid: String, Exchange: String, OrderType: String, Quantity: Double,
                               QuantityRemaining: Double, Limit: Double, CommissionPaid: Double, Price: Double,
                               PricePerUnit: Option[Double], Opened: LocalDateTime, Closed: Option[LocalDateTime],
                               CancelInitiated: Boolean, ImmediateOrCancel: Boolean, IsConditional: Boolean,
                               Condition: Option[String], ConditionTarget: Option[String])

object GetOpenOrdersResult {
  implicit val format: Format[GetOpenOrdersResult] = Json.format[GetOpenOrdersResult]
}

case class GetOpenOrders(success: Boolean, message: String, result: Seq[GetOpenOrdersResult])

object GetOpenOrders {
  implicit val format: Format[GetOpenOrders] = Json.format[GetOpenOrders]
}

case class GetBalances(Currency: String, Balance: Double, Available: Double, Pending: Double, CryptoAddress: Option[String],
                       Requested: Option[Boolean], Uuid: Option[String])

object GetBalances {
  implicit val format: Format[GetBalances] = Json.format[GetBalances]
}

case class GetBalancesResult(success: Boolean, message: String, result: Seq[GetBalances])

object GetBalanceResult {
  implicit val format: Format[GetBalanceResult] = Json.format[GetBalanceResult]
}

case class GetBalance(Currency: String, Balance: Double, Available: Double, Pending: Double, CryptoAddress: String,
                      Requested: Option[Boolean], Uuid: Option[String])

object GetBalance {
  implicit val format: Format[GetBalance] = Json.format[GetBalance]
}

case class GetBalanceResult(success: Boolean, message: String, result: Option[GetBalance])

object GetBalancesResult {
  implicit val format: Format[GetBalancesResult] = Json.format[GetBalancesResult]
}

case class GetDepositAddressResult(Currency: String, Address: String)

object GetDepositAddressResult {
  implicit val format: Format[GetDepositAddressResult] = Json.format[GetDepositAddressResult]
}

case class GetDepositAddress(success: Boolean, message: String, result: Option[GetDepositAddressResult])

object GetDepositAddress {
  implicit val format: Format[GetDepositAddress] = Json.format[GetDepositAddress]
}

case class WithdrawResult(uuid: String)

object WithdrawResult {
  implicit val format: Format[WithdrawResult] = Json.format[WithdrawResult]
}

case class Withdraw(success: Boolean, message: String, result: Option[WithdrawResult])

object Withdraw {
  implicit val format: Format[Withdraw] = Json.format[Withdraw]
}

case class GetOrder(AccountId: Option[String], OrderUuid: String, Exchange: String, Type: String, Quantity: Double,
                    QuantityRemaining: Double, Limit: Double, Reserved: Double, ReserveRemaining: Double,
                    CommissionReserved: Double, CommissionReserveRemaining: Double, CommissionPaid: Double,
                    Price: Double, PricePerUnit: Option[Double], Opened: LocalDateTime, Closed: Option[LocalDateTime],
                    IsOpen: Boolean, Sentinel: String, CancelInitiated: Boolean, ImmediateOrCancel: Boolean,
                    IsConditional: Boolean, Condition: String /*, ConditionTarget:Option[String] */)

object GetOrder {
  implicit val format: Format[GetOrder] = Json.format[GetOrder]
}

case class GetOrderResult(success: Boolean, message: String, result: Option[GetOrder])

object GetOrderResult {
  implicit val format: Format[GetOrderResult] = Json.format[GetOrderResult]
}

case class GetOrderHistory(Uuid: String, Exchange: String, TimeStamp: LocalDateTime, OrderType: String,
                           Limit: Double, Quantity: Double, QuantityRemaining: Double, Commission: Double,
                           Price: Double, PricePerUnit: Option[Double], IsConditional: Boolean, Condition: Option[String],
                           ConditionTarget: Option[String], ImmediateOrCancel: Boolean)

object GetOrderHistory {
  implicit val format: Format[GetOrderHistory] = Json.format[GetOrderHistory]
}

case class GetOrderHistoryResult(success: Boolean, message: String, result: Option[Seq[GetOrderHistory]])

object GetOrderHistoryResult {
  implicit val format: Format[GetOrderHistoryResult] = Json.format[GetOrderHistoryResult]
}


case class GetWithdrawHistory(PaymentUuid: Option[String], Currency: String, Amount: Double, Address: Option[String], Opened: Option[LocalDateTime],
                              Authorized: Option[Boolean], PendingPayment: Option[Double], TxId: Option[String], Canceled: Option[Boolean],
                              InvalidAddress: Option[Boolean])

object GetWithdrawHistory {
  implicit val format: Format[GetWithdrawHistory] = Json.format[GetWithdrawHistory]
}

case class GetWithdrawHistoryResult(success: Boolean, message: String, result: Option[Seq[GetWithdrawHistory]])

object GetWithdrawHistoryResult {
  implicit val format: Format[GetWithdrawHistoryResult] = Json.format[GetWithdrawHistoryResult]
}

case class GetDepositHistory(PaymentUuid: Option[String], Currency: String, Amount: Double, Address: Option[String], Opened: Option[LocalDateTime],
                             Authorized: Option[Boolean], PendingPayment: Option[Boolean], TxCost: Option[Double], TxId: String, Canceled: Option[Boolean],
                             InvalidAddress: Option[Boolean])

object GetDepositHistory {
  implicit val format: Format[GetDepositHistory] = Json.format[GetDepositHistory]
}

case class GetDepositHistoryResult(success: Boolean, message: String, result: Option[Seq[GetDepositHistory]])

object GetDepositHistoryResult {
  implicit val format: Format[GetDepositHistoryResult] = Json.format[GetDepositHistoryResult]
}