package net.zambot.exchange.external.bittrex.api.v3

import com.github.signalr4j.client.ConnectionState
import com.github.signalr4j.client.hubs.HubConnection

class BittrexSocketClient(url: String) {
  private val hubConnection = new HubConnection(url)
  private val hubProxy = hubConnection.createHubProxy("c3")

  def connect: BittrexSocketClient = {
    hubConnection.start.get
    hubConnection.getState == ConnectionState.Connected
    this

  }

  def setMessageHandler(handler: Object): BittrexSocketClient = {
    hubProxy.subscribe(handler)
    this
  }

  def subscribe(channels: Array[String]): Array[Nothing] =
    hubProxy
      .invoke(
        classOf[Array[Nothing]],
        "Subscribe",
        channels.asInstanceOf[Any]).get


}
