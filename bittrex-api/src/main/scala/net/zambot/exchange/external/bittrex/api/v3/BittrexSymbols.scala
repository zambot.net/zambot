package net.zambot.exchange.external.bittrex.api.v3

import java.util.UUID

import scala.collection.immutable.HashMap

object BittrexSymbols {

  val lookupTable = HashMap(
    "ETH-BTC" -> ZSymbolMapping(
      symbol = "ETH-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000001"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000000"),
    ),
    "XRP-BTC" -> ZSymbolMapping(
      symbol = "XRP-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000002"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000001"),
    ),
    "BCH-BTC" -> ZSymbolMapping(
      symbol = "BCH-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000003"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000002"),
    ),
    "BSV-BTC" -> ZSymbolMapping(
      symbol = "BSV-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000004"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000003"),
    ),
    "LTC-BTC" -> ZSymbolMapping(
      symbol = "LTC-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000005"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000004"),
    ),
    "EOS-BTC" -> ZSymbolMapping(
      symbol = "EOS-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000006"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000005"),
    ),
    "XTZ-BTC" -> ZSymbolMapping(
      symbol = "XTZ-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000007"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000006"),
    ),
    "ADA-BTC" -> ZSymbolMapping(
      symbol = "ADA-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000008"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000007"),
    ),
    "XLM-BTC" -> ZSymbolMapping(
      symbol = "XLM-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000009"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000008"),
    ),
    "CRO-BTC" -> ZSymbolMapping(
      symbol = "CRO-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-00000000000a"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000009"),
    ),
    "LINK-BTC" -> ZSymbolMapping(
      symbol = "LINK-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-00000000000b"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-00000000000a"),
    ),
    "XMR-BTC" -> ZSymbolMapping(
      symbol = "XMR-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-00000000000c"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-00000000000b"),
    ),
    "TRX-BTC" -> ZSymbolMapping(
      symbol = "TRX-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-00000000000d"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-00000000000c"),
    ),
    "NEO-BTC" -> ZSymbolMapping(
      symbol = "NEO-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-00000000000e"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-00000000000d"),
    ),
    "ETC-BTC" -> ZSymbolMapping(
      symbol = "ETC-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-00000000000f"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-00000000000e"),
    ),
    "DASH-BTC" -> ZSymbolMapping(
      symbol = "DASH-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000010"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-00000000000f"),
    ),
    "IOTA-BTC" -> ZSymbolMapping(
      symbol = "IOTA-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000011"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000010"),
    ),
    "ATOM-BTC" -> ZSymbolMapping(
      symbol = "ATOM-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000012"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000011"),
    ),
    "ZEC-BTC" -> ZSymbolMapping(
      symbol = "ZEC-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000013"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000012"),
    ),
    "ONT-BTC" -> ZSymbolMapping(
      symbol = "ONT-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000014"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000013"),
    ),
    "XEM-BTC" -> ZSymbolMapping(
      symbol = "XEM-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000015"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000014"),
    ),
    "DOGE-BTC" -> ZSymbolMapping(
      symbol = "DOGE-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000016"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000015"),
    ),
    "VEM-BTC" -> ZSymbolMapping(
      symbol = "VEM-BTC",
      schedulerInboundChannelId = UUID.fromString("00000000-0000-0000-0000-000000000017"),
      externalTaskId = UUID.fromString("00000000-0000-0000-0000-000000000016"),
    )

  )

  case class ZSymbolMapping(symbol: String, schedulerInboundChannelId: UUID, externalTaskId: UUID)


}
