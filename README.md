Zambot allows for peoples and organizations to build, test and deploy trading algorithms and strategies.

Zambot was designed for unlimited scalability and extensibility, allowing peoples and organizations to keep focus on their own trading algorithms and strategies. No mater whether you are an individual, a statup or a corporation, Zambot fits your needs. With Zambot, you start quickly and grow as fast as you need.


# Zambot Architecture
Zambot Architecture consists on four main modules:
* **Scheduler**  The _Scheduler_ prioritizes and controls task executions.
* **Controller**  The _Controller_ execute tasks, such as get ticker for a market, place and cancel orders, etc.
* **Tracker**  The _Tracker_ records signals by processing samples. It works with time windows in order to generate signals.
* **Advisor**  The _Advisor_ generates trading advices from track signals. It features a profile funcionality, so you can store values between advices.

## Business Logic
_Javascript_ is a very popuplar and easy to use language. Business Logics written in _Zambot JavaScript DSL_ is a DSL (Domain Specific Language) written on top of JavaScript language that features the creation of profiles that allows for storing behavior by creating variables such as counters and averages. This behaviour can be used by Algorithms and Trading Policies in order to generate _Signals_ and _Advices_. _Zambot Javascript DSL_ is powered by the _Nashorn JavaScript Engine_ and fully supports _ECMAScript 5.1_.

## Simulation
Zambot was designed for unlimited simulation by playingback tasks and signals.
